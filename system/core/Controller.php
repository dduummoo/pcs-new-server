<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2016, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2016, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Application Controller Class
 *
 * This class object is the super class that every library in
 * CodeIgniter will be assigned to.
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		EllisLab Dev Team
 * @link		https://codeigniter.com/user_guide/general/controllers.html
 */
class CI_Controller {

	/**
	 * Reference to the CI singleton
	 *
	 * @var	object
	 */
	private static $instance;
	public $need_login = false;//添加登录状态属性

	/**
	 * Class constructor
	 *
	 * @return	void
	 */
	public function __construct()
	{
		self::$instance =& $this;

		// Assign all the class objects that were instantiated by the
		// bootstrap file (CodeIgniter.php) to local class variables
		// so that CI can run as one big super object.
		foreach (is_loaded() as $var => $class)
		{
			$this->$var =& load_class($class);
		}

		$this->load =& load_class('Loader', 'core');
		$this->load->initialize();
//		$this->check_user_login();
		log_message('info', 'Controller Class Initialized');
	}


	/**
	 * Get the CI singleton
	 *
	 * @static
	 * @return	object
	 */
	public static function &get_instance()
	{
		return self::$instance;
	}

	public function check_login($user){
		if($user == "user"){
			$this->check_user_login();
		}else if($user=="suser"){
			$this->check_suser_login();
		}else if($user == "suser_or_user"){
			$this->check_suser_or_user_login();
		}else {
			echo "<script language='javascript' type='text/javascript'> alert('登录判断参数出错')</script>";
			$this->check_suser_or_user_login();
		}
}

	private function check_user_login(){//判断登录的方法
		if($this->need_login){
			if(!isset($_SESSION['uid'])){
			$url = "/pcs/index.php?C=User&F=login";
				echo "<script language='javascript' type='text/javascript'>";
				echo "alert('用户未登录或者登录已经过期。')";
				echo "</script>";
				echo "<script language='javascript' type='text/javascript'>";
				echo "window.location.href='$url'";
				echo "</script>";
				exit;
			}
		}
	}

	private function check_suser_login(){//判断登录的方法
		if($this->need_login){
			if(!isset($_SESSION['suid'])){

				$url = "/pcs/index.php?C=SuperUser&F=login";
				echo "<script language='javascript' type='text/javascript'>";
				echo "alert('管理员未登录或者登录已经过期。')";
				echo "</script>";
				echo "<script language='javascript' type='text/javascript'>";
				echo "window.location.href='$url'";
				echo "</script>";
				exit;
			}
		}
	}
	private function check_suser_or_user_login(){
		if($this->need_login){
			if((!isset($_SESSION['uid']))&&(!isset($_SESSION['suid']))){
				$url = "/pcs/index.php";
				echo "<script language='javascript' type='text/javascript'>";
				echo "alert('您未未登录或者登录已经过期。')";
				echo "</script>";
				echo "<script language='javascript' type='text/javascript'>";
				echo "window.location.href='$url'";
				echo "</script>";
				exit;
			}
		}
	}

}
