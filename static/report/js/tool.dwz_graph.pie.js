if(typeof(tool) == 'undefined'){
    tool = {};
}
if(typeof(tool["dwz_graph"]) == 'undefined'){
    tool["dwz_graph"] = {};
}
tool["dwz_graph"]["pie"] = {
    title : "",
    titleXpos : 390,
    titleYpos : 85,

    /* Pie Data */
    pieRadius : 130,//d
    pieXpos : 150,
    pieYpos : 180,
    pieData : [],
    pieLegend : [],

    pieLegendPos : "east",
    
    draw : function(id){
        var r = Raphael(id);

        r.text(this.titleXpos, this.titleYpos, this.title).attr({"font-size": 20});

        var pie = r.piechart(this.pieXpos, this.pieYpos, this.pieRadius, this.pieData, {legend: this.pieLegend, legendpos: this.pieLegendPos});
        pie.hover(function () {
            this.sector.stop();
            this.sector.scale(1.1, 1.1, this.cx, this.cy);
            if (this.label) {
                this.label[0].stop();
                this.label[0].attr({ r: 7.5 });
                this.label[1].attr({"font-weight": 800});
            }
        }, function () {
            this.sector.animate({ transform: 's1 1 ' + this.cx + ' ' + this.cy }, 500, "bounce");
            if (this.label) {
                this.label[0].animate({ r: 5 }, 500, "bounce");
                this.label[1].attr({"font-weight": 400});
            }
        });
    }



};