if(typeof(tool) == 'undefined'){
    tool = {};
}
if(typeof(tool["dwz_graph"]) == 'undefined'){
    tool["dwz_graph"] = {};
}
tool["dwz_graph"]["linechart"] = {
    //option
    // axis: "0 0 1 1", // Where to put the labels (trbl)
    // axisxstep: 10, // How many x interval labels to render (axisystep does the same for the y axis)
    // shade:false, // true, false
    // smooth:false, //曲线
    // symbol:"circle",
    xval : [],
    yval : [],

    draw : function(id){
        var options = {
            axis: "0 0 1 1", // Where to put the labels (trbl)
            axisxstep: 10, // How many x interval labels to render (axisystep does the same for the y axis)
            shade:false, // true, false
            smooth:false, //曲线
            symbol:"circle"
        };

        var r = Raphael(id);
        var lines = r.linechart(
            20, // X start in pixels
            10, // Y start in pixels
            600, // Width of chart in pixels
            400, // Height of chart in pixels
            this.xval, // Array of x coordinates equal in length to ycoords
            this.yval, // Array of y coordinates equal in length to xcoords
            options // opts object
        ).hoverColumn(function () {
            this.tags = r.set();

            for (var i = 0, ii = this.y.length; i < ii; i++) {
                this.tags.push(r.tag(this.x, this.y[i], this.values[i], 160, 10).insertBefore(this).attr([{ fill: "#fff" }, { fill: this.symbols[i].attr("fill") }]));
            }
        }, function () {
            this.tags && this.tags.remove();
        });
        lines.symbols.attr({ r: 6 });
    }



};