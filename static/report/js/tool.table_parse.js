/**
 * Created by cyy on 2016/9/14.
 */
if(typeof(tool) == 'undefined'){
    tool = {};
}
tool["table_parse"] = {

    make_table_data : function(arr_data,width,column,method){
        var html = "<table style='width: 100%;border: 1px solid #dddddd;'><tbody>";
        var len = arr_data.length;
        html += "<td width='"+width+"'>列名("+column+")</td>";
        html += "<td width='"+width+"'>统计结果("+method+")</td>";
        for(var i = 0;i < len;++i){
            html += "<tr>"
            for(var key in arr_data[i]){
                html += "<td width='"+width+"'>"+arr_data[i][key]+"</td>";
            }
            html += "</tr>";
        }
        return html + "</tbody></table>";
    }
};