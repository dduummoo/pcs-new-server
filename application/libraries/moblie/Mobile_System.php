<?php

/**
 * Created by PhpStorm.
 * User: bb
 * Date: 2016/8/18
 * Time: 15:32
 */
class Mobile_System
{
   public function system_config($post){
        $res = $this->checkLogin();
        if($res["rs"] == "ok"){
            $this->rs["status"] = "ok";
        }else {
            $this->rs['status'] = "need_login";
        }
        $this->rs["msg"] = null;
        $this->rs["data"] =  array(
            "software_pcs_name" =>"xxx派出所",   //使用单位的姓名
            "forget_password_tip"=>"忘记密码，请拨打xxxxxx", //点忘记密码弹出的提示框
            "backgroup_picture"=>"http://xxx.xx.xx/bg.jpg"  //登录页背景图片
        );
        $this->load->model("Group_Model","groupModel");
        $this->rs["data"]["level_groups"] = $this->groupModel->level_groups();
    }
}