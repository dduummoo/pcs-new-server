<?php
/**
 * Created by PhpStorm.
 * User: Realdata
 * Date: 2016/9/6
 * Time: 15:36
 */
Class My_time{
   
    public function get_start_and_end_time( $start, $interval){
       
        $current_time = time();
        $i = (int)((time()-$start)/$interval);
        $start_time = $start+$i*$interval;
        $end_time = $start+($i+1)*$interval;
       return array($start_time,$end_time);
    }
    
    //获取本月每周的开始时间和结束时间，格式为array(array(第一天开始时间，第一天结束时间),array(第二天开始时间，第二天结束时间)。。。。。)
    public function get_week_time(){
        $start = strtotime("last sunday next day", strtotime(date("Y-m-d",time()))); //取上周日的下一天，也就是取周一，这里不能使用monday直接取周一
        $end = strtotime("next monday", $start) - 1;  // 取下周一的前一秒，也就是本周日的最后一秒
//   echo sprintf("%s 当周开始时间戳和结束时间戳: <br/>\n", date('Y-m-d',time()));
//   echo sprintf("周一: %d -> %s <br/>\n", $start, strftime("%c", $start));
//   echo sprintf("周日: %d -> %s <br/><br/>\n\n", $end, strftime("%c", $end));
        $week_time = array();
        $i = 0;
        for(;$start<$end;$start=$start+24*60*60){
            $week_time[$i][0] = $start;
            $week_time[$i][1]=$start+24*60*60-1;
            $i++;
        }
        return $week_time;
    }
//获取本月每天的开始时间和结束时间，格式为array(array(第一天开始时间，第一天结束时间),array(第二天开始时间，第二天结束时间)。。。。。)
    public function  get_month_time($year,$month){
        $beginThismonth = strtotime($year."-".$month."-"."1 0:0:0");
        if($month+1<=12){
            $endThismonth = strtotime($year."-".($month+1)."-"."1 0:0:0")-1;
        }else{
            $endThismonth = strtotime(($year+1)."-1-1 12 0:0:0")-1;
        }
        print_r("aaa");
        $month_time = array();
        $i = 0;
        for(;$beginThismonth<$endThismonth;$beginThismonth=$beginThismonth+24*60*60){
            $month_time[$i][0] = $beginThismonth;
            $month_time[$i][1]=$beginThismonth+24*60*60-1;
            $i++;
            
        }
        return $month_time;
    }
    //获取今天的开始时间和结束时间，格式为array(array(第一天开始时间，第一天结束时间))
    function get_today_time(){
        $beginToday=mktime(0,0,0,date('m'),date('d'),date('Y'));
        $endToday=mktime(0,0,0,date('m'),date('d')+1,date('Y'))-1;
        return array(array($beginToday,$endToday));
    }

    //获取今年到现在每个月的开始和结束时间
    function get_every_month_time(){
        $time = time();
        //对时间戳进行格式化
        $month = date('m',$time);
        $year =  date('y',$time);
        $time = array();
        for($i=1;$i<$month+1;$i++){
             $begin_time = strtotime($year."-".$i."-"."1 0:0:0"); 
            if($i+1<=12){
             $end_time = strtotime($year."-".($i+1)."-"."1 0:0:0")-1;
            }else{
                $end_time = strtotime(($year+1)."-1-1 12 0:0:0")-1;
            }
            $time[$i-1] = array($begin_time,$end_time);
        }
        return $time;
    }
}