<?php
class Tablelist extends CI_Model{

    private $table;
    private $where;
    private $fields;
    private $orderby;

    public function __construct()
    {
        $this->load->database();
    }

    /**
     * 获取表数据 result_array
     * @param type $table 表
     * @param type $where sql 条件
     * @param type $fields sql select()
     */
    public function get_tablelist( $fields = '*',$table, $where = '1=1',$orderby = '') {
        $this->table = $table;
        $this->where = $where;
        $this->fields = $fields;
        $this->orderby = $orderby;
        $query = $this->db->query("select ".$fields." from ".$table." where ".$where );
        //print_r($query->result_array()[1]['group_id']);
        return $query->result_array();
    }

}

?>