<?php

/**
 * Created by PhpStorm.
 * User: cyy
 * Date: 2016/8/8
 * Time: 0:17
 */

defined('BASEPATH') OR exit('No direct script access allowed');


class Form_Parse_Tool
{


    /**
     * 负责把formdesigned中设计的东西，存储到mongo中,映射表如下：
     *
    [leipiplugins] => text
    [orgtype] => text
    ----------
    [leipiplugins] => select
    [value] => aa,bb,cc
    -------------
    [leipiplugins] => radios
    [value] => aa,bb,cc
    -------------
    [leipiplugins] => checkboxs
    [value] => aa,vv,cc
    -------------
    [type] => text
    [orgtype] => sys_datetime
    -------------
    [type] => text
    [orgtype] => sys_time
    -------------
    [leipiplugins] => text
    [orgtype] => float
    ---------------
    [leipiplugins] => text
    [orgtype] => int
    ----------------
    [leipiplugins] => text
    [orgtype] => idcard

     *
     * @param $data formdesign攻击传递的结构体中的data参数
     * @return array 返回适合mongo进行插入的数据结构
     */
    public function create_mongo_column_from_formdesign($data){
        $rs = array();
        foreach ($data as $value){
//            print_r($value);
            if((!array_key_exists("leipiplugins",$value))&&(!array_key_exists("type",$value))){
                $rs[$value["name"]] = array("type"=>"textarea");
                continue;
            }else  if(array_key_exists("leipiplugins",$value)) {
                $type = $value["leipiplugins"];
                if(($type === "macros" ||$type=="text")&&array_key_exists("orgtype",$value)){
                    $type = $value["orgtype"];
                }
            }
//            else{
////                return $value;
//                $type = $value['type'];
//                if($type=="text"&&array_key_exists("orgtype",$value)){
//                    $type = $value["orgtype"];
//                }
//            }

//            if($type == "text" || $type == "macros"){
//                $type = $value["orgtype"];
//            }

            $arr = array(
                "type" => $type
            );

            if($type == "select" || $type == "radios" || $type == "checkboxs"){
                $arr["items"] = explode(",",$value["value"]);
            }
            if($type == "checkboxs"){
                $name = explode(",",$value["name"])[0];
            }else{
                $name = $value["name"];
            }
            //复选框的时候，name是一列相同的，中间用逗号分隔，需要处理
            $rs[$name] = $arr;
        }


        return $rs;
    }


    /**
     *
     * 从formdesign 控件的data字段获取 整个报表的字段类型
     *
     * @param array $formdesign_data 从formdesign 控件的data字段获取 整个报表的字段类型，供其他部件使用
     * @return array  返回报表字段的类型数组，键是报表的各个字段，值是每个报表的类型
     */
    public function get_data_type($formdesign_data){
        $rs = array();
        foreach ($formdesign_data as $value){
            $type = $value["leipiplugins"];

            if($type == "text" || $type == "macros"){
                $type = $value["orgtype"];
            }
            $rs[explode(",",$value["name"])[0][0]] = $type;
        }
        return $rs;
    }

//    public function get_data_name($formdesign_data){
//        $rs = array();
//        print_r($formdesign_data);
//        foreach ($formdesign_data as $value){
//           array_push($rs,$value['name']);
//        }
//        return $rs;
//    }
    /**
     *
     * 对报表提交的结果进行整形，根据报表内容的type，把字符串变成对应的对应的类型，比如：int，float,time等。对参数本身直接进行操作
     *
     * @param array $report_data 准备进行整形的报表的提交的参数的数组，即form表单的POST内容，去掉了report_id
     * @param array $data_type  get_data_type()函数返回的 报表的字段的type的键值对数组
     */
    public function change_report_data_type(&$report_data,$data_type){
        foreach ($report_data as $key => $value){
            $type = $data_type[$key]['type'];
            if($type == null){
                continue;
            }

            if($type == "int"){
                $report_data[$key] = intval($value);
                continue;
            }

            if($type == "float"){
                $report_data[$key] =  floatval($value);
                continue;
            }
            if($type == "sys_datetime" || $type == "sys_date" || $type == "sys_time"){
                $report_data[$key] =  strtotime($value);
                continue;
            }

        }

    }
}