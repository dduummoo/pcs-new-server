<?php
/**
 * Created by PhpStorm.
 * User: cao
 * Date: 16-7-25
 * Time: 下午10:25
 *
 * 负责处理安全上的一些问题
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class System_security {

    /**
     * 对密码进行hash
     * @param string $username 用户的账号
     * @param string $password 用户的密码
     * @return string
     */
    public function hash_password($username,$password){
        return md5($username . "^_^" . $password);
    }
}