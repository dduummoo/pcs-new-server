<?php

/**
 * 对系统整体进行Hook，对系统进行控制和修改
 * Created by PhpStorm.
 * User: cyy
 * Date: 2016/7/20
 * Time: 21:53
 */
class Hook
{
    private $CI;
    function __construct(){
        $this->CI =& get_instance();
        $this->CI->load->library("session");
    }
    /**
     *  对系统的权限，进行判定，主要判定$_GET中传输的路由参数，如果没有运行权限的，一律返回 no_access
     *  如果对于没有登录的，一律返回 no_login
     *
     *  此处为了不影响性能，将使用缓存中的数据进行判定
     */
    public function acl(){
        // 根据配置文件里的设定，获取对应模块的请求参数

        $control = isset($_GET["C"])?$_GET["C"]:null;
        if($control == null){
            $control = isset($_GET['c']) ? $_GET['c']:null;
        }
        $func = isset($_GET["F"])?$_GET["F"]:null;
        if($func == null){
            $func = isset($_GET['f']) ? $_GET['f']:null;
        }
        if($control == null){
            return;
        }

        $this->CI->load->model("Acl_Rule_Model","aclRuleModel");
        $aclArray = $this->CI->aclRuleModel->find();
        $key = $control.".".$func;
//        print_r($aclArray);
//        print_r($key);
        if(array_key_exists($key,$aclArray )){
            $acl = $aclArray[$key];
            $type = $acl["type"];
            $suid = $this->CI->session->userdata("suid");
            $uid = $this->CI->session->userdata("uid");
            $ugroupid = $this->CI->session->userdata("ugroupid");
            if($type == "login"){
                //此项接口需要用户登录
                if($uid == "" && $suid ==""){
                    die("no_login");
                }
            }else if($type == "super"){
                //此项接口需要超级用户登录才能访问
                if($suid==""){
                    die("no_login");
                }
            }else if($type == "login_and_check"){
                //此类接口需要再判断分组是否在可执行的范围内
                if($suid==""&& $uid==""){
                    die("no_login");
                }
                if($ugroupid==""){
                    die("no_access");
                }
                $gid = $ugroupid;
                if(!in_array($gid,$acl['ugroup'])){
                    die("no_access");
                }
            }
        }
        return;
    }
}