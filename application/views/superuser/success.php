<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>派出所信息后台管理</title>
    <base href="<?php echo base_url(); ?>dwz/" />
    <link href="themes/default/style.css" rel="stylesheet" type="text/css" media="screen"/>
    <link href="themes/css/core.css" rel="stylesheet" type="text/css" media="screen"/>
    <link href="themes/css/print.css" rel="stylesheet" type="text/css" media="print"/>
    <link href="uploadify/css/uploadify.css" rel="stylesheet" type="text/css" media="screen"/>
    <!--[if IE]>
    <link href="themes/css/ieHack.css" rel="stylesheet" type="text/css" media="screen"/>
    <![endif]-->

    <!--[if lt IE 9]><script src="js/speedup.js" type="text/javascript"></script><script src="js/jquery-1.11.3.min.js" type="text/javascript"></script><![endif]-->
    <!--[if gte IE 9]><!--><script src="js/jquery-2.1.4.min.js" type="text/javascript"></script><!--<![endif]-->

    <script src="js/jquery.cookie.js" type="text/javascript"></script>
    <script src="js/jquery.validate.js" type="text/javascript"></script>
    <script src="js/jquery.bgiframe.js" type="text/javascript"></script>
    <script src="xheditor/xheditor-1.2.2.min.js" type="text/javascript"></script>
    <script src="xheditor/xheditor_lang/zh-cn.js" type="text/javascript"></script>
    <script src="uploadify/scripts/jquery.uploadify.js" type="text/javascript"></script>

    <!-- svg图表  supports Firefox 3.0+, Safari 3.0+, Chrome 5.0+, Opera 9.5+ and Internet Explorer 6.0+ -->
    <script type="text/javascript" src="chart/raphael.js"></script>
    <script type="text/javascript" src="chart/g.raphael.js"></script>
    <script type="text/javascript" src="chart/g.bar.js"></script>
    <script type="text/javascript" src="chart/g.line.js"></script>
    <script type="text/javascript" src="chart/g.pie.js"></script>
    <script type="text/javascript" src="chart/g.dot.js"></script>

    <script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=6PYkS1eDz5pMnyfO0jvBNE0F"></script>
    <script type="text/javascript" src="http://api.map.baidu.com/library/Heatmap/2.0/src/Heatmap_min.js"></script>

    <script src="js/dwz.core.js" type="text/javascript"></script>
    <script src="js/dwz.util.date.js" type="text/javascript"></script>
    <script src="js/dwz.validate.method.js" type="text/javascript"></script>
    <script src="js/dwz.barDrag.js" type="text/javascript"></script>
    <script src="js/dwz.drag.js" type="text/javascript"></script>
    <script src="js/dwz.tree.js" type="text/javascript"></script>
    <script src="js/dwz.accordion.js" type="text/javascript"></script>
    <script src="js/dwz.ui.js" type="text/javascript"></script>
    <script src="js/dwz.theme.js" type="text/javascript"></script>
    <script src="js/dwz.switchEnv.js" type="text/javascript"></script>
    <script src="js/dwz.alertMsg.js" type="text/javascript"></script>
    <script src="js/dwz.contextmenu.js" type="text/javascript"></script>
    <script src="js/dwz.navTab.js" type="text/javascript"></script>
    <script src="js/dwz.tab.js" type="text/javascript"></script>
    <script src="js/dwz.resize.js" type="text/javascript"></script>
    <script src="js/dwz.dialog.js" type="text/javascript"></script>
    <script src="js/dwz.dialogDrag.js" type="text/javascript"></script>
    <script src="js/dwz.sortDrag.js" type="text/javascript"></script>
    <script src="js/dwz.cssTable.js" type="text/javascript"></script>
    <script src="js/dwz.stable.js" type="text/javascript"></script>
    <script src="js/dwz.taskBar.js" type="text/javascript"></script>
    <script src="js/dwz.ajax.js" type="text/javascript"></script>
    <script src="js/dwz.pagination.js" type="text/javascript"></script>
    <script src="js/dwz.database.js" type="text/javascript"></script>
    <script src="js/dwz.datepicker.js" type="text/javascript"></script>
    <script src="js/dwz.effects.js" type="text/javascript"></script>
    <script src="js/dwz.panel.js" type="text/javascript"></script>
    <script src="js/dwz.checkbox.js" type="text/javascript"></script>
    <script src="js/dwz.history.js" type="text/javascript"></script>
    <script src="js/dwz.combox.js" type="text/javascript"></script>
    <script src="js/dwz.print.js" type="text/javascript"></script>

    <!-- 可以用dwz.min.js替换前面全部dwz.*.js (注意：替换时下面dwz.regional.zh.js还需要引入)
    <script src="bin/dwz.min.js" type="text/javascript"></script>
    -->
    <script src="js/dwz.regional.zh.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(function(){
            DWZ.init("dwz.frag.xml", {
                loginUrl:"login_dialog.html", loginTitle:"登录",	// 弹出登录对话框
//		loginUrl:"login.html",	// 跳到登录页面
                statusCode:{ok:200, error:300, timeout:301}, //【可选】
                pageInfo:{pageNum:"pageNum", numPerPage:"numPerPage", orderField:"orderField", orderDirection:"orderDirection"}, //【可选】
                keys: {statusCode:"statusCode", message:"message"}, //【可选】
                ui:{hideMode:'offsets'}, //【可选】hideMode:navTab组件切换的隐藏方式，支持的值有’display’，’offsets’负数偏移位置的值，默认值为’display’
                debug:false,	// 调试模式 【true|false】
                callback:function(){
                    initEnv();
                    $("#themeList").theme({themeBase:"themes"}); // themeBase 相对于index页面的主题base路径
                }
            });
        });
    </script>

</head>

<body>
<div id="layout">
<!--    <input type="text" value="--><?php //echo $info['suid']; ?><!--">-->
    <div id="header">
        <div class="headerNav">
            <a class="logo" href="javascript:">标志</a>
            <ul class="nav">
                <li style="color:#ddd">你好：管理员</li>
                <li><a href="../index.php?C=SuperUser&F=changepwd_view&suid=<?php echo $info['suid']; ?>" title="编辑" target="dialog" width ="900" class="edit" rel="superuserchangepwd">修改密码</a></li>
                <li><a href="../index.php?C=SuperUser&F=login">退出</a></li>
            </ul>
            <ul class="themeList" id="themeList">
                <li theme="default"><div class="selected">蓝色</div></li>
                <li theme="green"><div>绿色</div></li>
                <!--<li theme="red"><div>红色</div></li>-->
                <li theme="purple"><div>紫色</div></li>
                <li theme="silver"><div>银色</div></li>
                <li theme="azure"><div>天蓝</div></li>
            </ul>
        </div>

        <!-- navMenu -->

    </div>

    <div id="leftside">
        <div id="sidebar_s">
            <div class="collapse">
                <div class="toggleCollapse"><div></div></div>
            </div>
        </div>
        <div id="sidebar">
            <div class="toggleCollapse"><h2>主菜单</h2><div>收缩</div></div>

            <div class="accordion" fillSpace="sidebar">

                <div class="accordionHeader">
                    <h2><span>Folder</span>用户管理</h2>
                </div>
                <div class="accordionContent">
                    <ul class="tree">
                        <li><a href="../index.php?C=User&F=ulist" target="navTab" rel="showUserlist" title="用户列表">浏览用户</a></li>
                        <li><a href="../index.php?C=User&F=uadd_view" target="dialog" width ="900" rel="showUseradd">添加用户</a></li>
                    </ul>
                </div>

                <div class="accordionHeader">
                    <h2><span>Folder</span>分组管理</h2>
                </div>
                <div class="accordionContent">
                    <ul class="tree">
                        <li><a href="../index.php?C=Group&F=glist" target="navTab" rel="showUserGrouplist">浏览分组</a></li>
                        <li><a href="../index.php?C=Group&F=gadd_view" target="dialog" width ="900" rel="showGroupadd">添加分组</a></li>
                    </ul>
                </div>

                <div class="accordionHeader">
                    <h2><span>Folder</span>权限管理</h2>
                </div>
                <div class="accordionContent">
                    <ul class="tree">
                        <li><a href="../index.php?C=Ugroup_Permission&F=ugplist" target="navTab" rel="showUserGroupPermissionlist">浏览分组权限</a></li>
                        <li><a href="../index.php?C=Ugroup_Permission&F=rglist" target="navTab" rel="showReportGrantlist">报表分配管理</a></li>
                    </ul>
                </div>

                <div class="accordionHeader">
                    <h2><span>Folder</span>填报报表</h2>
                </div>

                <div class="accordionContent">
                    <ul class="tree">
                        <li><a href="../index.php?C=Form_design&F=create_form" target="navTab" rel="showUserGroupPermissionlist" external="true">创建填报报表</a></li>
                        <li><a href="../index.php?C=Form_design&F=report_list"  target="navTab" rel="xxx">填报报表列表
                            </a></li>
<!--                        <li><a href="../index.php?C=Form_design&F=show_report" target="navTab" rel="show_one_report" >查看填报报表</a></li>-->
                    </ul>
                </div>



                <div class="accordionHeader">
                    <h2><span>Folder</span>统计报表</h2>
                </div>

                <div class="accordionContent">
                    <ul class="tree">
                        <li><a href="../index.php?C=Form_design&F=create_statistics_form" target="navTab" rel="create_statistics_form" external="true">创建统计报表</a></li>
                        <li><a href="../index.php?C=Form_design&F=statistics_report_list" target="navTab" rel="statistics_list">统计报表列表</a></li>
<!--                        <li><a href="../index.php?C=Form_design&F=demo_show_one_statistics_report" target="navTab" rel="show_one_statistics_form" external="true">查看统计报表</a></li>-->
<!--                        <li><a href="../index.php?C=Form_design&F=show_graph_report_demo" target="navTab" rel="show_one_statistics_graph" external="true">查看图表报表</a></li>-->
                    </ul>
                </div>
<!--                <div class="accordionHeader">-->
<!--                    <h2><span>Folder</span>数据库还原与备份</h2>-->
<!--                </div>-->
<!--                <div class="accordionContent">-->
<!--                    <ul class="tree">-->
<!--                        <li><a href="../index.php?C=DbController&F=prepare_back_up_db" target="navTab" rel="back_up_db" external="true">备份Mysql</a></li>-->
<!--                        <li><a href="../index.php?C=Form_design&F=report_list"  target="navTab" rel="xxx">还原Mysql-->
<!--                            </a></li>-->
<!--                        <!--                        <li><a href="../index.php?C=Form_design&F=show_report" target="navTab" rel="show_one_report" >查看填报报表</a></li>-->-->
<!--                    </ul>-->
<!--                </div>-->

            </div>
        </div>
    </div>

    <div id="container">
        <div id="navTab" class="tabsPage">
            <div class="tabsPageHeader">
                <div class="tabsPageHeaderContent"><!-- 显示左右控制时添加 class="tabsPageHeaderMargin" -->
                    <ul class="navTab-tab">
                        <li tabid="main" class="main"><a href="javascript:;"><span><span class="home_icon">我的主页</span></span></a></li>
                    </ul>9
                </div>
                <div class="tabsLeft">left</div><!-- 禁用只需要添加一个样式 class="tabsLeft tabsLeftDisabled" -->
                <div class="tabsRight">right</div><!-- 禁用只需要添加一个样式 class="tabsRight tabsRightDisabled" -->
                <div class="tabsMore">more</div>
            </div>
            <ul class="tabsMoreList">
                <li><a href="javascript:;">我的主页</a></li>
            </ul>
            <div class="navTab-panel tabsPageContent layoutBox">
                <div class="page unitBox">
                    <div class="accountInfo">
                        <div style="font-size: 40px;">欢迎访问北京海淀区派出所管理系统</div>
                    </div>
                    <div style="width:230px;position: absolute;top:60px;right:0" layoutH="80">
                        xxx
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

<div id="footer">Copyright &copy; 2016 <a href="demo_page2.html" target="dialog">南京实数科技有限公司</a> 京ICP备xxxxxxxx号</div>

</body>
</html>
