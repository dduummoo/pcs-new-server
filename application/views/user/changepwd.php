<div class="pageContent">
    <form method="post" action="<?php echo site_url('C=User&F=changepwd') ?>" class="pageForm required-validate" onsubmit="return validateCallback(this,dialogAjaxDone);">
        <div class="pageFormContent nowrap" layoutH="56">
            <input type="hidden" name="id"  value="<?php echo $info['id']; ?>">
            <dl>
                <dt>旧 密 码：</dt>
                <dd><input name="password_f" type="text" size="30" /></dd>
            </dl>
            <dl>
                <dt>新 密 码：</dt>
                <dd><input name="password1" type="text" size="30" minlength="6" /><span class="info">字母、数字、下划线 6-20位</span></dd>
            </dl>
            <dl>
                <dt>再次输入新密码：</dt>
                <dd><input name="password2" type="text" size="30" minlength="6" /></dd>
            </dl>
        </div>
        <div class="formBar">
            <ul>
                <!--<li><a class="buttonActive" href="javascript:;"><span>保存</span></a></li>-->
                <li><div class="buttonActive"><div class="buttonContent"><button type="submit">确认</button></div></div></li>
                <li>
                    <div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
                </li>
            </ul>
        </div>
    </form>
</div>
