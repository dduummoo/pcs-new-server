<!--suppress ALL -->
<html>
<head >
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>派出所信息后台管理</title>
    <base href="<?php echo base_url(); ?>dwz/themes" />
    <link href="style/alogin.css" rel="stylesheet" type="text/css" />
    <script src="../static/js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="../static/js/localstorage.js" type="text/javascript"></script>
</head>
<body>
<?php echo form_open('C=User&F=login'); ?>
<div class="Main">
    <ul>
        <li class="top"></li>
        <li class="top2"></li>
        <li class="topA"></li>
        <li class="topB"><span>
                        <img src="themes/default/images/login/logo1.png" alt="" style="width: 150px;margin-left: 70px;">
                    </span></li>
        <li class="topC"></li>
        <li class="topD">
            <ul class="login">
                <li><span class="left">分  组：</span> <span style="left">
                                <select name="ugroupid" id="ugroupid" >
                                <option value="">请选择</option>
                                    <?php
                                    foreach ($ugrouplist as $group) {
                                        echo "<option value='{$group['id']}'>{$group['name']}</option>";
                                    }
                                    ?>
                    </select>
                            </span>
                </li>
                <li>
                    <span class="left">用户名：</span>
                            <span style="left">
                                <input id="username" type="text" name ="username" class="txt" />
                            </span>
                </li>
                <li><span class="left">密  码：</span> <span style="left">
                                <input id="password" type="password" name="password" class="txt" />
                            </span>
                </li>
                <li>
                   <span class="left">验证码：</span>
                    <input type="text" name="code" id="inputcode" class="txt"  style="width:130px"/>
                    <img id="verifycode" title="点击刷新"  onclick="refresh_code()"  alt="验证码" style="float: right;margin-top: 3px;margin-right:50px"  src="../index.php?C=User&F=get_image_and_str"/>
<!--                    <a href="javascript:refresh_code()">看不清？换一个</a>-->
                </li>
<!--                <li>-->
<!--                    <input id="save_btn" name = "rem" type="checkbox">记住密码-->
<!--                    <input id="auto_login_btn" name = "auto" type="checkbox" style="margin-left: 40px;">自动登录-->
<!--                </li>-->
            </ul>
            </ul>
        </li>
        <li class="topE"></li>
        <li class="middle_A"></li>
        <li class="middle_B">
        </li>
        <li class="middle_C">
                    <span class="btn">
                        <input id="commit_btn" type="image" alt="" src="themes/default/images/login/btnlogin.gif" />
                    </span>
        </li>
        <li class="middle_D"></li>
        <li class="bottom_A"></li>
        <li class="bottom_B">
            北京 海淀区派出所<br />
            后台管理
        </li>
    </ul>
</div>
</form>

<script >
        function refresh_code()
        {
            document.getElementById("verifycode").src = "../index.php?C=User&F=get_image_and_str";
        }
        function save_data(){
            store.set('username', $('#username').val());
            store.set('password', $('#password').val());
            store.set('group_id', $("#ugroupid").val())
        }
        function render_data(){
            $('#username').val(store.get('username'));
            $('#password').val(store.get('password'));
            $('#ugroupid').val(store.get('group_id'));
            if(store.get('is_save') == 1){
                $("#save_btn").prop("checked",true);
            }
            if(store.get('auto_login') == 1){
                $("#auto_login_btn").prop("checked",true);
            }
        }
        function bind_save_btn(){
            $('#save_btn').click(function(){
                if($(this).prop("checked")){
                    store.set('is_save', 1);
                }else{
                    store.set('is_save',0);
                }
            });
        }
        function bind_auto_login_btn(){
            $("#auto_login_btn").click(function(){
                if($(this).prop("checked")){
                    store.set('auto_login', 1);
                }else{
                    store.set('auto_login',0);
                }
            });
        }
        function bind_commit_btn(){
            $('#commit_btn').click(function(event){
                if($("#ugroupid").val() == ""){
                    alert("您尚未选择分组");
                    return false;
                }
                if($("#username").val() == ""){
                    alert("用户名不能为空");
                    return flase;
                }
                if($("#password").val() == ""){
                    alert("密码不能为空");
                    return false;
                }
//
                var code = $("#inputcode").val();
                if(code=="") {
                    alert("请输入验证码");
                    return false;
                }
//                var verifycode ="<?php //print_r($_SESSION['code']) ?>//";
//                alert(verifycode);
//                alert(code);
//               if(code.tolowercase!=verifycode.toLowerCase){
//                   alert("验证码错误");
//                   refresh_code();
//                   return false;
//               }
                if(store.get('is_save') == 1){
                    save_data();
                }

            });
        }
//        function init_code(){
//            $.ajax({
//                type: "GET",
//                dataType:"JSONP",
//                headers: {
//                    "Access-Control-Allow-Origin" : "*"},
//                url: 'http://localhost/pcs/index.php?C=User&F=get_image_and_str',
//                success: function(response) {
//                   console.log(response.img);
////                    $("#verifycode").src = response.obj.printimg();
//                }
//                });
//        }
        function init_page(){
            bind_save_btn();
            bind_commit_btn();
            bind_auto_login_btn();
//            init_code();
            if(store.get('is_save') == 1){
                render_data();
            }
            if(store.get('auto_login') == 1){
                $("#commit_btn").click();
            }
        }
        init_page();
</script>
</body>
</html>
