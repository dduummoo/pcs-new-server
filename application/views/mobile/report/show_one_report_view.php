
<!DOCTYPE HTML>
<html>
<head>

    <title>报表预览</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <link href="static/report/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <!--[if lte IE 6]>
    <link rel="stylesheet" type="text/css" href="static/report/css/bootstrap-ie6.css">
    <![endif]-->
    <!--[if lte IE 7]>
    <link rel="stylesheet" type="text/css" href="static/report/css/ie.css">
    <![endif]-->
    <link href="static/report/css/site.css" rel="stylesheet" type="text/css" />


    <script type="text/javascript" charset="utf-8" src="static/report/js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="static/report/js/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="static/report/js/ueditor.all.js"> </script>
    <script type="text/javascript" charset="utf-8" src="static/report/js/zh-cn.js"></script>


</head>
<body>




<div class="container">
    <div class="page-header">
        <h1>填写报表</h1>
        <div><a href="index.php?C=Mobile_api&F=api&model=view&action=show_all_report">返回列表</a></div>
    </div>
    <div>ID: <?php echo $id ?> &nbsp; 报表名称：<?php echo $name?></div>
    <hr/>
    <form name="one_report" method="post" action="index.php?C=Mobile_api&F=api&model=view&action=commit_report">
        <input type="hidden" name="report_id" value="<?php echo $report_id; ?>" />
        <?php  echo $content;?>
        <input type="hidden" name="GPS" id="GPS" value="GPS">
        <input type="hidden" name="IMEI" id="IMEI" value="IMET">
    </form>
    <div class>
        <button onclick="document.one_report.submit()"> 填报数据</button>
    </div>
</div><!--end container-->


</body>
<script>
    $(function(){
        $("input[type='checkbox']").each(function(){
            $(this).attr("name",$(this).attr("name")+"[]");
        });
    });
</script>
</html>