<!DOCTYPE HTML>
<html>
<head>

    <title>报表列表</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <link href="static/mobile/css/common.css" rel="stylesheet" type="text/css">
</head>
<body>

<form onsubmit="return navTabSearch(this);"
      action="<?php echo site_url('C=Mobile_api&F=api&model=view&action=show_all_report') ?>" method="post">

    <table align="center">
        <tr align="center">
            <td>
                <h4>报表名：<input id="searchContent" type="text" name="name" value=""/></h4>
            </td>
            <td>
                <h2>
                    <button type="submit">检索</button>
                </h2>
            </td>
        </tr>
    </table>
</form>

<div align="center">
    <table style="width: 100%;" align="center">
        <thead>
        <tr>
            <th>报表名称</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($reportlist as $row) { ?>
            <tr align="left">
                <td>
                    <a href="index.php?C=Mobile_api&F=api&model=view&action=do_one_report&id=<?php echo $row->id ?>"><span><h2><?php echo $row->name; ?>
                                <h2></span></a></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <div>
        <div>
            <span><h3>共 <?php echo $total ?> 条</h3></span>
        </div>
        <div id="page_turn_zone"><h3><?php echo $this->pagination->create_links(); ?> <h3></div>

    </div>
</div>

</body>
<script>
    document.getElementById("searchContent").value = '<?php echo $name?>';
</script>
</html>