
<!DOCTYPE HTML>
<html>
<head>
    <title>填写报表</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <link href="static/report/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <!--[if lte IE 6]>
    <link rel="stylesheet" type="text/css" href="static/report/css/bootstrap-ie6.css">
    <![endif]-->
    <!--[if lte IE 7]>
    <link rel="stylesheet" type="text/css" href="static/report/css/ie.css">
    <![endif]-->
    <link href="static/report/css/site.css" rel="stylesheet" type="text/css" />


    <script type="text/javascript" charset="utf-8" src="static/report/js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="static/report/js/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="static/report/js/ueditor.all.js"> </script>
    <script type="text/javascript" charset="utf-8" src="static/report/js/zh-cn.js"></script>
    <script type="text/javascript" charset="utf-8" src="dwz/js/dwz.ajax.js"></script>
    <script type="text/javascript" charset="utf-8" src="dwz/js/dwz.navTab.js"></script>



</head>
<body>




<div class="container">
    <div class="page-header">
        <h1>填写报表</h1>
        <div><a href="index.php?C=Mobile_api&F=api&model=view&action=show_all_report">返回列表</a></div>
    </div>
    <div>ID: <?php echo $id ?> &nbsp; 报表名称：<?php echo $name?></div>
    <hr/>
    <div id="report">
        <input type="hidden" name="report_id" value="<?php echo $report_id; ?>" />
        <?php  echo $content;?>
        <input type="hidden" name="GPS" id="GPS" value="GPS">
        <input type="hidden" name="IMEI" id="IMEI" value="IMET">
    </div>
    <div class>
        <button id="commit_report_data">填报数据</button>
    </div>
</div><!--end container-->


</body>
<script>
    //
    $(function () {
        $("#commit_report_data").click(function(){
            var data = {};
            var report_template=eval(<?php echo $report_type?>);
            var unvisblekey = new Array("time", "GPS", "IP","UA","IMEI","uid", "from","_id");
            data['report_template']=report_template;
            data['data']={};
            for(var key in report_template){
                if(in_array(key,unvisblekey)){
                    data['data'][key] = value;
                }else{
                    var element_type = report_template[key]['type'];
                    if(element_type == "text"||element_type == "int"||element_type == "float"||element_type == "email"||element_type == "idcard") {
                        var element = $("input[name="+key+"]")[0];
                        var key_word = key.substring(5,key.length);
                        var value = element.value;
                    }else if(element_type == "textarea"){
                        var element = $("textarea[name="+key+"]")[0];
                        var value = element.value;
                    }else if(element_type == "select"){
                        var element = $("select[name="+key+"]")[0];
                        var value = element.value;
                    }else if(element_type == "checkboxs"||element_type=="radios"){
                        var elements = $("input[name="+key+"]");
                        var value='';
                        for(var i=0; i<elements.length; i++){
                            if(elements[i].checked) value+=elements[i].value+',';  //如果选中，将value添加到变量s中
                        }
                        value =value.substring(0,value.length-1);
                    }
                    if(!judgeType(element_type,value)){
                        alert("填充值为"+value+"应该为"+element_type+"类型,类型不匹配或者格式不正确");
                        return;
                    }
                    data['data'][key] = value;
                }
            }
            if(!confirm("您是否确认上传数据？")){
                return;
            }
            var report_id=$("#report_id").val();
            data["report_id"] = report_id;
//            alert("暂时未与填报系统对接");
//            return;
            $.ajax({
                type: "POST",
                url: 'index.php?C=Mobile_api&F=api&model=view&action=commit_report',
                data: data,
                dataType:"json",
                success: function(response) {
//                    console.log(str_response);
                    if(response.rs == "success"){
                        alert("填报报表数据成功");
                        navTab.closeCurrentTab();
                    }else {
                        alert(response.msg);
                    }
                }
            });

            return false;
        });
    });

    function in_array(stringToSearch, arrayToSearch) {
        for (s = 0; s < arrayToSearch.length; s++) {
            thisEntry = arrayToSearch[s].toString();
            if (thisEntry == stringToSearch) {
                return true;
            }
        }
        return false;
    }
    function judgeType(type,value){
        if(type == "int") {
            if (parseInt(value) == value) {
                return true;
            } else {
                return false;
            }
        }
        if(type == "float"){
            if(isNaN(value)){
                return false;
            }else{
                return true;
            }
        }
        //验证邮箱；
        if(type == 'email'){
            var reg = /[\w!#$%&'*+/=?^_`{|}~-]+(?:\.[\w!#$%&'*+/=?^_`{|}~-]+)*@(?:[\w](?:[\w-]*[\w])?\.)+[\w](?:[\w-]*[\w])?/;
            if(reg.test(value)){
                return true;
            }else{
                return false;
            }
        }
        //验证身份证
        if(type=='idcard'){
            var reg= /^(\d{6})(\d{4})(\d{2})(\d{2})(\d{3})([0-9]|X)$/;
            if(reg.test(value)){
                return true;
            }else{
                return false;
            }
        }
        return  true;
    }

</script>
</html>