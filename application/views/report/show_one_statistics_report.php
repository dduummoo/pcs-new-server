
<!DOCTYPE HTML>
<html>
<head>

    <title>显示统计结果</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <link href="static/report/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <!--[if lte IE 6]>
    <link rel="stylesheet" type="text/css" href="static/report/css/bootstrap-ie6.css">
    <![endif]-->
    <!--[if lte IE 7]>
    <link rel="stylesheet" type="text/css" href="static/report/css/ie.css">
    <![endif]-->
    <link href="static/report/css/site.css" rel="stylesheet" type="text/css" />


    <script type="text/javascript" charset="utf-8" src="static/report/js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="static/report/js/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="static/report/js/ueditor.all.js"> </script>
    <script type="text/javascript" charset="utf-8" src="static/report/js/zh-cn.js"></script>
    <script type="text/javascript" src="dwz/chart/raphael.js"></script>
    <script type="text/javascript" src="dwz/chart/g.raphael.js"></script>
    <script type="text/javascript" src="dwz/chart/g.bar.js"></script>
    <script type="text/javascript" src="dwz/chart/g.line.js"></script>
    <script type="text/javascript" src="dwz/chart/g.pie.js"></script>
    <script type="text/javascript" src="dwz/chart/g.dot.js"></script>
    <script type="text/javascript" charset="utf-8" src="static/report/js/tool.dwz_graph.pie.js"></script>
    <script type="text/javascript" charset="utf-8" src="static/report/js/tool.dwz_graph.linechart.js"></script>
    <script type="text/javascript" charset="utf-8" src="static/report/js/tool.table_parse.js"></script>

</head>
<body>
<div class="container">
    <div class="page-header">
        <h1>显示统计结果 </h1>
    </div>
    <div id="statistic_report">
    <?php  echo $content;?>
    </div>
    <div id="chartHolder"></div>
    <div id="chartHolder3" style="width: 650px;height: 450px"></div>
    <button id="excel">生成excel</button>
    <script>
    $("input").each(function(){
        var query = $(this).attr("value");
        var dom = this;
        $.ajax({
            type:"GET",
            url:"index.php?C=Form_query&F=query&query="+encodeURIComponent(query),
            dataType:"json",
            success:function(data){
                console.log(data);
                if(data.type == "one"){
                    $(dom).prop("outerHTML",data.data+"");
                }else{
                    var width = $(dom).parent().parent().prev().children().attr("width");
                    $(dom).parent().css("padding","0px");
                    $(dom).prop("outerHTML",tool.table_parse.make_table_data(data.data.result,width,data.data.list,data.data.method));
//                     var d = "分组统计以"+data.data.list+"进行分组，分组的方式为"+data.data.method;
//            for(var i=0;i<data.data.result.length;i++){
//                var r = data.data.method.substr(5,data.data.method.length-5);
//                var key = r+"_"+data.data.column;
//                d=d+","+data.data.list+"="+data.data.result[i]._id+":"+data.data.result[i][key];
//            }
//                    $(dom).prop("outerHTML",d);
                }
            }
        });
    });
    $("textarea").css("display","none");
    array_id = new Array();
    array_num = new Array();
    query = $("textarea").val();
    //console.log(query);
    $.ajax({
        type:"GET",
        url:"index.php?C=Form_query&F=query&query="+encodeURIComponent(query),
        dataType:"json",
        success:function(data){
//            console.log(data);
            for(var i=0;i<data.data.result.length;i++)
            {
                for(var key in data.data.result[i]){
                    if(key == '_id')array_id.push("%%.%% – "+data.data.result[i][key]);
                    else array_num.push(data.data.result[i][key]);
                }
            }
            tool.dwz_graph.pie.title = "<?php echo $title?>";
            tool.dwz_graph.pie.pieData = array_num;
            tool.dwz_graph.pie.pieLegend = array_id;
            tool.dwz_graph.pie.draw("chartHolder");


        }
    });
        tool.dwz_graph.linechart.xval = [.5,1.5,2,2.5,3,3.5,4,4.5,5];
        tool.dwz_graph.linechart.yval = [7,11,9,16,3,19,12,12,15];
        tool.dwz_graph.linechart.draw("chartHolder3");
    $(function () {
        $("#excel").click(function(){
            var curTbl = document.getElementById("statistic_report");
            var oXL = new ActiveXObject("Excel.Application");
            var oWB = oXL.Workbooks.Add();
            var oSheet = oWB.ActiveSheet;
            var sel = document.body.createTextRange();
            sel.moveToElementText(curTbl);
            sel.select();
            sel.execCommand("Copy");
            oSheet.Paste();
            oXL.Visible = true;
        });
    });
        
    </script>
</body>
</html>