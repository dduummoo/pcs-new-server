<html><head>

    <title>填报报表</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <link href="static/report/css/bootstrap.css" rel="stylesheet" type="text/css">
    <!--[if lte IE 6]>
    <link rel="stylesheet" type="text/css" href="static/report/css/bootstrap-ie6.css">
    <![endif]-->
    <!--[if lte IE 7]>
    <link rel="stylesheet" type="text/css" href="static/report/css/ie.css">
    <![endif]-->
    <link href="static/report/css/site.css" rel="stylesheet" type="text/css">


    <script type="text/javascript" charset="utf-8" src="static/report/js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="static/report/js/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="static/report/js/ueditor.all.js"> </script>
    <script type="text/javascript" charset="utf-8" src="static/report/js/zh-cn.js"></script>


</head>
<body>




<div class="container">
    <div class="page-header">
        <h1>填报报表</h1>
    </div>

    <p style="text-align: center;"><span style="font-size: 24px;">每日例行街头巡逻报表</span></p><p><span style="font-size: 24px;"><br></span></p><p><span style="font-size: 24px;">事件标题：<input type="text" value="" name="data_biaoti" style="text-align: left; width: 150px;"></span></p><p><span style="font-size: 24px;">当事人：<input type="text" value="" name="data_dangshiren" style="text-align: left; width: 150px;"></span></p><p><span style="font-size: 24px;">发生时间：<input name="data_fashengshijian" type="text" value="2016-08-29 13:22:06" title="fashengshijian" leipiplugins="macros" orgtype="sys_datetime" orghide="0" orgwidth="150" style="width: 150px;"></span></p><p><span style="font-size: 24px;">详细经过：</span></p><p><span style="font-size: 24px;"><textarea name="data_xiangxijingguo" id="data_xiangxijingguo" value="" style="width:300px;height:80px;"></textarea></span></p><p><span style="font-size: 24px;">类型：<span leipiplugins="select"><select name="data_leixing" title="leixing" leipiplugins="select" size="1" orgwidth="150" style="width: 150px;"><option value="斗殴">斗殴</option><option value="赌博">赌博</option><option value="碰瓷">碰瓷</option><option value="诈骗">诈骗</option><option value="乞讨">乞讨</option></select>&nbsp;&nbsp;</span></span></p><p><span style="font-size: 24px;">处理结果：<input type="radio" name="data_chulijieguo" value="私下解决" checked="checked">私下解决&nbsp;<input type="radio" name="data_chulijieguo" value="等待进一步处理">等待进一步处理&nbsp;</span></p><p><span style="font-size: 24px;">定型：<input type="checkbox" name="data_dingxing" value="行政">行政&nbsp;<input type="checkbox" name="data_dingxing" value="民事" checked="checked">民事&nbsp;<input type="checkbox" name="data_dingxing" value="刑事">刑事&nbsp;</span></p>

    <button>提交报表</button>
</div><!--end container-->



</body></html>