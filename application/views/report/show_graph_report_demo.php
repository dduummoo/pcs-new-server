<html>
    <head>
        <meta charset="utf-8">
        <script src="http://cdn.hcharts.cn/jquery/jquery-1.8.3.min.js"></script>
        <script src="http://cdn.hcharts.cn/highcharts/highcharts.js"></script>
    </head>
    <body>
        <div id="graph1" style="min-width:400px;height:400px" ></div>
        <div id="graph2" style="min-width:400px;height:400px" ></div>
        <div id="graph3" style="min-width:400px;height:400px" ></div>

        <script>
            $(function(){
                $('#graph1').highcharts({
                    credits:{
                        enabled:false // 禁用版权信息
                    },
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false
                    },
                    title: {
                        text: '派出所分所盗窃情况比例图'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                        type: 'pie',
                        name: '盗窃情况比例',
                        data: [
                            ['大钟寺',   45.0],
                            ['中关村',       26.8],
                            {
                                name: '保福寺',
                                y: 12.8,
                                sliced: true,
                                selected: true
                            },
                            ['双榆树',    8.5],
                            ['某xx派出所',     6.2],
                            ['某bb派出所',   0.7]
                        ]
                    }]
                });


                //return;
                $('#graph2').highcharts({
                    credits:{
                        enabled:false // 禁用版权信息
                    },
                    title: {
                        text: '派出所分所盗窃情况统计图',
                        x: -20 //center
                    },
                    subtitle: {
                        text: '2016.08.22-2016.08.28',
                        x: -20
                    },
                    xAxis: {
                        categories: ['周一', '周二', '周三', '周四', '周五', '周六',
                            '周日']
                    },
                    yAxis: {
                        title: {
                            text: '案件发生数量'
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    },
                    tooltip: {
                        valueSuffix: '件'
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
                    series: [{
                        name: '双榆树',
                        data: [7, 6, 9, 14, 18, 21, 25]
                    }, {
                        name: '中关村',
                        data: [0, 0, 5, 11, 17, 22, 24]
                    }, {
                        name: '大钟寺',
                        data: [0, 0, 3, 8, 13, 17, 18]
                    }, {
                        name: '保福寺',
                        data: [3, 4, 5, 8, 11, 15, 17]
                    }]
                });
            });
        </script>
    </body>
</html>