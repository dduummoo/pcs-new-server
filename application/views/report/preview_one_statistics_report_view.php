
<!DOCTYPE HTML>
<html>
<head>

    <title>报表预览</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <link href="static/report/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <!--[if lte IE 6]>
    <link rel="stylesheet" type="text/css" href="static/report/css/bootstrap-ie6.css">
    <![endif]-->
    <!--[if lte IE 7]>
    <link rel="stylesheet" type="text/css" href="static/report/css/ie.css">
    <![endif]-->
    <link href="static/report/css/site.css" rel="stylesheet" type="text/css" />


    <script type="text/javascript" charset="utf-8" src="static/report/js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="static/report/js/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="static/report/js/ueditor.all.js"> </script>
    <script type="text/javascript" charset="utf-8" src="static/report/js/zh-cn.js"></script>
    <script type="text/javascript" charset="utf-8" src="static/report/js/tool.dwz_graph.pie.js"></script>
    <script type="text/javascript" charset="utf-8" src="static/report/js/tool.dwz_graph.linechart.js"></script>
    <script type="text/javascript" charset="utf-8" src="static/report/js/tool.table_parse.js"></script>

</head>
<body>
    <div class="container">
        <div class="page-header">
            <h1>预览统计报表 <small>如无问题请保存你的设计</small></h1>
        </div>

        <?php  echo $content;?>
    </div><!--end container-->

    <script>

        $("input").each(function(){
            var query = $(this).attr("value");
            var dom = this;
            $.ajax({
                type:"GET",
                url:"index.php?C=Form_query&F=query&query="+encodeURIComponent(query),
                dataType:"json",
                success:function(data){
                    if(data.type == "one"){
                        $(dom).prop("outerHTML",data.data+"");
                    }else{
                        var width = $(dom).parent().parent().prev().children().attr("width");
                        $(dom).parent().css("padding","0px");
                        $(dom).prop("outerHTML",tool.table_parse.make_table_data(data.data.result,width,data.data.list,data.data.method));
                    }
                }
            });
        });
    </script>
</body>
</html>