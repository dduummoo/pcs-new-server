
<!DOCTYPE HTML>
<html>
<head>

    <title>统计报表预览</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <link href="static/report/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <!--[if lte IE 6]>
    <link rel="stylesheet" type="text/css" href="static/report/css/bootstrap-ie6.css">
    <![endif]-->
    <!--[if lte IE 7]>
    <link rel="stylesheet" type="text/css" href="static/report/css/ie.css">
    <![endif]-->
    <link href="static/report/css/site.css" rel="stylesheet" type="text/css" />


    <script type="text/javascript" charset="utf-8" src="static/report/js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="static/report/js/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="static/report/js/ueditor.all.js"> </script>
    <script type="text/javascript" charset="utf-8" src="static/report/js/zh-cn.js"></script>
    <script type="text/javascript" charset="utf-8" src="static/report/js/tool.table_parse.js"></script>

</head>
<body>
<div class="container">
    <div class="page-header">
        <h1>预览统计报表 <small>如无问题请保存你的设计</small></h1>
    </div>


    <table class="table table-bordered" data-sort="sortDisabled"><tbody>
        <tr class="firstRow"><td width="132" valign="top" style="word-break: break-all;">名称</td><td width="132" valign="top" style="word-break: break-all;">数量</td>
<!--            <td width="132" valign="top"><br></td><td width="132" valign="top"><br></td>-->
<!--            <td width="132" valign="top"><br></td></tr>-->
        <tr><td valign="top" rowspan="3" colspan="2"><input type="text" value="result=66.uid.where().list_sum(group_by_data_thing_type)" name="data_ssss" style="text-align: left; width: 150px;" title="data_ssss"></td>
<!--            <td width="132" valign="top"><br></td><td width="132" valign="top"><br></td><td width="132" valign="top"><br></td></tr><tr><td width="132" valign="top"><br></td><td width="132" valign="top"><br></td><td width="132" valign="top"><br></td></tr><tr><td width="132" valign="top"><br></td><td width="132" valign="top"><br></td><td width="132" valign="top"><br></td></tr><tr><td width="132" valign="top"><br></td><td width="132" valign="top"><br></td><td width="132" valign="top"><br></td><td width="132" valign="top"><br></td><td width="132" valign="top"><br></td>-->
        </tr></tbody></table>


<script>

    $("input").each(function(){
        var query = $(this).attr("value");
        var dom = this;
        $.ajax({
            type:"GET",
            url:"index.php?C=Form_query&F=query&query="+encodeURIComponent(query),
            dataType:"json",
            success:function(data){
                /*
                暂时屏蔽这个请求
                if(data.type == "one"){
                    $(dom).prop("outerHTML",data.data+"");
                }
                */
                //我们假设请求返回的是一个list，需要显示成一列表格
                var o = {
                    data :[
                        {
                            "name":"cao",
                            "age":18
                        },
                        {
                            "name":"cyy",
                            "age":11
                        },
                        {
                            "name":"jm",
                            "age":20
                        },
                        {
                            "name":"jm",
                            "age":20
                        },
                        {
                            "name":"jm",
                            "age":20
                        },
                        {
                            "name":"jm",
                            "age":20
                        }
                    ]
                };
                var width = $(dom).parent().parent().prev().children().attr("width");
                $(dom).parent().css("padding","0px");
                $(dom).prop("outerHTML",tool.table_parse.make_table_data(o.data,width));
            }
        });
    });
</script>
</body>
</html>