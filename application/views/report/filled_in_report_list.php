
<form id="pagerForm" method="post" action="<?php echo site_url('C=Form_design&F=filled_report_find') ?>">
        <input type="hidden" name="pageNum" value="1" />
        <input type="hidden" name="numPerPage" value="<?php echo $info['page_size']?>" />
        <!--    <input type="hidden" name="status" value="${param.status}">-->
        <!--    <input type="hidden" name="username" value="--><?php //echo $info['uname']?><!--" />-->
        <input type="hidden" name="report_id" id="report_id1" value="<?php echo $info['reportid']?>" />
        <input type="hidden" name="orderField" value="${param.orderField}" />
    </form>
<!--<input type="text" value="--><?php //echo site_url('C=Form_design&F=filled_report_find') ?><!--"/>-->
    <div class="pageHeader">
        <form onsubmit="return navTabSearch(this)" action="<?php echo site_url('C=Form_design&F=filled_report_find') ?>" method="post">
            <div class="searchBar">
                <table class="searchContent">
                    <tr>
                        <td>
                            报 表 名：
                            <select name="reportid" >
                                <option value="0">个人报表</option>
                                <?php
                                foreach ($reportlist as $report) {
                                    echo "<option value='{$report['id']}'>{$report['name']}</option>";
                                }
                                ?>
                            </select>
                        </td>
                        <td>
                            <p>
                                <label>开始时间： </label>
                                <input type="text" name="date_start" class="date" dateFmt="yyyy-MM-dd HH:mm:ss" readonly="true"/>
                                <a class="inputDateButton" href="javascript:;">选择</a>
                            </p>
                            </td>
                        <td>
                            <p>
                                <label>结束时间： </label>
                                <input type="text" name="date_end" class="date" dateFmt="yyyy-MM-dd HH:mm:ss" readonly="true"/>
                                <a class="inputDateButton" href="javascript:;">选择</a>
                            </p>

                        </td>
                    </tr>

                </table>
                <div class="subBar">
                    <ul>
                        <li><div class="buttonActive"><div class="buttonContent"><button type="submit">检索</button></div></div></li>
                    </ul>
                </div>
            </div>
        </form>
    </div>
    <div class="pageContent">
        <div class="panelBar">
            <ul class="toolBar">
                <li><a class="edit" href="../index.php?C=Form_design&F=show_one_filled_report&mongo_id={mongo_id}&report_id=<?php echo $info['reportid']?>" target="navTab" width ="900" rel="showonefilledreport" external="true"><span>查看</span></a></li>
<!--                <li><a class="edit" href="#" onclick = "showOneFilledReport({mongo_id})" target="navTab" width ="900" rel="showonefilledreport"><span>查看</span></a></li>-->
                <li><a class="edit" href="../index.php?C=Form_design&F=prepare_update_one_report_data&mongo_id={mongo_id}&report_id=<?php echo $info['reportid']?>" target="navTab" width ="900" rel="prepareupdatereportdata" external="true"><span>修改</span></a></li>
            </ul>
        </div>
        <table class="table" width="100%" layoutH="138">
            <thead>
            <tr>
                <th width="20">序号</th>
                <th width="50">填充时间</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($datalist as $row) { ?>
                <tr target="mongo_id"  rel="<?php echo $row['_id']; ?>">
                    <td><?php echo $row['id']; ?></td>
                    <td ><?php echo $row['time']; ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="panelBar">
            <div class="pages">
                <span>显示</span>
                <select name="numPerPage" onchange="navTabPageBreak({numPerPage:this.value})">
                    <option value="20" <?php if($info['page_size']==20) echo 'selected = "selected"'?>>20</option>
                    <option value="50" <?php if($info['page_size']==50) echo 'selected = "selected"'?>>50</option>
                    <option value="100" <?php if($info['page_size']==100) echo 'selected = "selected"'?>>100</option>
                    <option value="200" <?php if($info['page_size']==200) echo 'selected = "selected"'?>>200</option>
                </select>
                <span>条，共<?php echo $info['total']?>条</span>
            </div>

            <div class="pagination" targetType="navTab" totalCount="<?php echo $info['total']?>" numPerPage="<?php echo $info['page_size']?>" pageNumShown="<?php echo ceil($info['total']/$info['page_size'])?>" currentPage="<?php echo $info['current']?>"</div>

    </div>
    </div>
