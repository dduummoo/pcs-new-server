<form id="pagerForm" method="post" action="<?php echo site_url('C=Form_design&F=report_list') ?>">
    <head>
        <script type="text/javascript">
            function dbltable(target,rel){
                if( target != ''){
//                    console.log(rel);
//            $.pdialog.open(url, dlgId, title，option);
//                navTab.openTab(tabid, url, { title:”New Tab”, fresh:false, data:{} });
                    navTab.openTab("showSuperreportedit", "../index.php?C=Form_design&F=prepare_update_one_report&id="+rel,{title:"修改",fresh:false,external:true});
//                $.pdialog.open("../index.php?C=User&F=uedit_view&uid="+rel,"showUseredit","修改",{ width: 900, height: 300});
                }
            }
        </script>
    </head>
    <form id="pagerForm" method="post" action="<?php echo site_url() ?>">
        <input type="hidden" name="status" value="${param.status}">
        <input type="hidden" name="keywords" value="${param.keywords}" />
        <input type="hidden" name="pageNum" value="1" />
        <input type="hidden" name="numPerPage" value="<?php echo $info['page_size']?>" />
        <input type="hidden" name="name" value="<?php echo $info['name']?>" />
        <input type="hidden" name="orderField" value="${param.orderField}" />
    </form>

<div class="pageHeader">
    <form onsubmit="return navTabSearch(this);" action="<?php echo site_url('C=Form_design&F=report_list') ?>" method="post">
        <div class="searchBar">

            <table class="searchContent">
                <tr>
                    <td>
                        报表名：<input type="text" name="name" />
                    </td>
                </tr>
            </table>
            <div class="subBar">
                <ul>
                    <li><div class="buttonActive"><div class="buttonContent"><button type="submit">检索</button></div></div></li>
                </ul>
            </div>
        </div>
    </form>
</div>
<div class="pageContent">
    <div class="panelBar">
        <ul class="toolBar">
<!--            <li><a class="edit" href="../index.php?C=Form_design&F=create_form" target="navTab" width ="900" rel="showUseredit" external="true"><span>添加</span></a></li>-->
<!--            <li><a class="edit" href="../index.php?C=Form_design&F=prepare_update_one_report&id={id}" target="navTab" width ="900" rel="showUseredit" external="true"><span>修改</span></a></li>-->
            <li><a class="edit" href="../index.php?C=Form_design&F=prepare_fill_in_one_report&id={report_id}" target="navTab" width ="900" rel="fill_in_one_report" external="true"><span>填表</span></a></li>
            <li><a class="edit" href="../index.php?C=Form_design&F=show_one_report&id={report_id}" target="navTab" width ="900" rel="showOneReport" external="true"><span>查看</span></a></li>
        </ul>
    </div>
    <table class="table" width="100%" layoutH="138">
        <thead>
        <tr>
            <th width="20">序号</th>
            <th width="50">报表名称</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($reportlist as $row) { ?>
            <tr target="report_id" rel="<?php echo $row->id; ?>">
                <td><?php echo $row->num; ?></td>
                <td><?php echo $row->name; ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <div class="panelBar">
        <div class="pages">
            <span>显示</span>
            <select class="combox" name="numPerPage" onchange="navTabPageBreak({numPerPage:this.value})">
                <option value="20">20</option>
                <option value="50">50</option>
                <option value="100">100</option>
                <option value="200">200</option>
            </select>
            <span>条，共<?php echo $info['total']?>条</span>
        </div>

        <div class="pagination" targetType="navTab" totalCount="<?php echo $info['total']?>" numPerPage="<?php echo $info['page_size']?>" pageNumShown="<?php echo ceil($info['total']/$info['page_size'])?>" currentPage="<?php echo $info['current']?>"></div>

    </div>
</div>
