<form id="pagerForm" method="post" action="<?php echo site_url('C=Ugroup_Permission&F=rgfind') ?>">
<head>
    <script type="text/javascript">
        function dbltable(target,rel){
            if( target != ''){
                console.log(rel);
//            $.pdialog.open(url, dlgId, title，option);
                $.pdialog.open("../index.php?C=Ugroup_Permission&F=rgedit_view&dbid="+rel,"showReportgrantedit","修改",{ width: 900, height: 300});
            }
        }
    </script>
</head>
<form id="pagerForm" method="post" action="<?php echo site_url() ?>">
    <input type="hidden" name="status" value="${param.status}">
    <input type="hidden" name="keywords" value="${param.keywords}" />
    <input type="hidden" name="pageNum" value="1" />
    <input type="hidden" name="numPerPage" value="<?php echo $info['page_size']?>" />
    <input type="hidden" name="orderField" value="${param.orderField}" />
</form>

<div class="pageHeader">
    <form onsubmit="return navTabSearch(this);" action="<?php echo site_url('C=Ugroup_Permission&F=rgfind') ?>" method="post">
        <div class="searchBar">

            <table class="searchContent">
                <tr>
                    <td>
                        报表名称：
                        <select name="report_id" class="required">
                            <option value="">所有报表</option>
                            <?php
                            foreach ($reportlist as $report) {
                                echo "<option value='{$report['id']}'>{$report['name']}</option>";
                            }
                            ?>
                        </select>
                    </td>
                    <td>
                        分组名称：
                        <select name="ugroupid" class="required">
                            <option value="">所有分组</option>
                            <?php
                            foreach ($ugrouplist as $group) {
                                echo "<option value='{$group['id']}'>{$group['name']}</option>";
                            }
                            ?>
                        </select>
                    </td>
                </tr>
            </table>
            <div class="subBar">
                <ul>
                    <li><div class="buttonActive"><div class="buttonContent"><button type="submit">检索</button></div></div></li>
                </ul>
            </div>
        </div>
    </form>
</div>

<div class="pageContent">
    <div class="panelBar">
        <ul class="toolBar">
            <li><a class="add" href="../index.php?C=Ugroup_Permission&F=rgadd_view" target="dialog" width ="900" height ="350" rel="showReportgrantadd"><span>添加</span></a></li>
            <li><a class="delete" href="../index.php?C=Ugroup_Permission&F=rgdel&dbid={dbid}" target="ajaxTodo" title="确定要删除吗?"><span>删除</span></a></li>
            <li><a class="edit" href="../index.php?C=Ugroup_Permission&F=rgedit_view&dbid={dbid}" target="dialog" width ="900" rel="showReportgrantedit"><span>修改</span></a></li>
        </ul>
    </div>
    <table class="table" width="100%" layoutH="138">
        <thead>
        <tr>
            <th width="10">序号</th>
            <th width="20">报表名称</th>
            <th width="20">分组名称</th>
            <th width="40">填写频率(小时)</th>
            <th width="45">敏感期是否开启</th>
            <th width="10">敏感期开始时间</th>
            <th width="10">敏感期结束时间</th>
            <th width="45">敏感期的填写频率(小时)</th>
            <th width="25">是否开启</th>
        </tr>
        </thead>
        <tbody>
        <?php if(count($rglist)>0){ foreach ($rglist as $row) { ?>
            <tr target="dbid" rel="<?php echo $row['dbid']; ?>">
                <td><?php echo $row['id']; ?></td>
                <td><?php echo $row['report_name']; ?></td>
                <td><?php echo $row['ugroup_name']; ?></td>
                <td><?php echo round($row['frequency']/3600,4); ?></td>
                <td><?php  if($row['is_special_enable'])echo '是'; else echo '否'?></td>
                <td><?php echo $row['special_time_start']; ?></td>
                <td><?php echo $row['special_time_end']; ?></td>
                <td><?php echo round($row['special_frequency']/3600,4); ?></td>
                <td><?php if($row['is_enable'])echo '是'; else echo '否'?></td>

            </tr>
        <?php }} ?>
        </tbody>
    </table>
    <div class="panelBar">
        <div class="pages">
            <span>显示</span>
            <select class="combox" name="numPerPage" onchange="navTabPageBreak({numPerPage:this.value})">
                <option value="20">20</option>
                <option value="50">50</option>
                <option value="100">100</option>
                <option value="200">200</option>
            </select>
            <span>条，共<?php echo $info['total']?>条</span>
        </div>

        <div class="pagination" targetType="navTab" totalCount="<?php echo $info['total']?>" numPerPage="<?php echo $info['page_size']?>" pageNumShown="<?php echo ceil($info['total']/$info['page_size'])?>" currentPage="<?php echo $info['current']?>"></div>

    </div>
