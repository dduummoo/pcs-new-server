<div class="pageContent">
    <form method="post" action="<?php echo site_url('C=Ugroup_Permission&F=rgadd') ?>" class="pageForm required-validate" onsubmit="return validateCallback(this,dialogAjaxDone);">
        <div class="pageFormContent nowrap" layoutH="56">
            <input type="hidden" name="suid"  value="<?php echo $this->session->suid; ?>">
            <dl>
                <dt>报表名称：</dt>
                <dd>
                    <select name="report_id" class="required">
                        <option value="">请选择</option>
                        <?php
                        foreach ($reportlist as $report) {
                            echo "<option value='{$report['id']}'>{$report['name']}</option>";
                        }
                        ?>
                    </select>
                </dd>
            </dl>
            <dl>
                <dt>分组名称：</dt>
                <dd>
                    <select name="ugroupid" class="required">
                        <option value="">请选择</option>
                        <?php
                        foreach ($ugrouplist as $group) {
                            echo "<option value='{$group['id']}'>{$group['name']}</option>";
                        }
                        ?>
                    </select>
                </dd>
            </dl>
            <dl>
                <dt>填写频率：</dt>
                <dd><input name="frequency" class="required" type="text" size="30" value="" alt="小时为单位" />
                    <span class="info">多少小时填写一次报表</span>
                </dd>
            </dl>

            <dl>
                <dt>敏感期是否开启：</dt><input name = "sp" value = "1" type="checkbox">
            </dl>

            <p>
                <label>敏感期开始时间：</label>
                <input type="text" name="date_start" class="date" dateFmt="yyyy-MM-dd HH:mm:ss" readonly="true"/>
                <a class="inputDateButton" href="javascript:;">选择</a>
            </p>
            <p>
                <label>敏感期结束时间： </label>
                <input type="text" name="date_end" class="date" dateFmt="yyyy-MM-dd HH:mm:ss" readonly="true"/>
                <a class="inputDateButton" href="javascript:;">选择</a>
            </p>

            <dl>
                <dt>敏感期的填写频率： </dt>
                <dd><input name="s_frequency" type="text" size="30" value="" alt="小时为单位" />
                    <span class="info">多少小时填写一次报表</span>
                </dd>
            </dl>

            <dl>
                <dt>是否开启：</dt>
                <dd><input name = "stop" value = "1" type="checkbox">
                </dd>
            </dl>



        </div>
        <div class="formBar">
            <ul>
                <!--<li><a class="buttonActive" href="javascript:;"><span>保存</span></a></li>-->
                <li><div class="buttonActive"><div class="buttonContent"><button type="submit">保存</button></div></div></li>
                <li>
                    <div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
                </li>
            </ul>
        </div>
    </form>
</div>
