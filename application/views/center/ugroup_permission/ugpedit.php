<div class="pageContent">
    <form method="post" action="<?php echo site_url('C=Ugroup_Permission&F=ugpedit') ?>" class="pageForm required-validate" onsubmit="return validateCallback(this,dialogAjaxDone);">
        <div class="pageFormContent nowrap" layoutH="56">
            <input type="hidden" name="id"  value="<?php echo $info['gid']; ?>">
            <input type="hidden" name="u1"  value="<?php echo $u[0]; ?>">
            <input type="hidden" name="u2"  value="<?php echo $u[1]; ?>">
            <input type="hidden" name="u3"  value="<?php echo $u[2]; ?>">
<!--            <input type="hidden" name="dbid0"  value="--><?php //echo $id[0]; ?><!--">-->
<!--            <input type="hidden" name="dbid1"  value="--><?php //echo $id[1]; ?><!--">-->
<!--            <input type="hidden" name="dbid2"  value="--><?php //echo $id[2]; ?><!--">-->
            <dl>
                <ul >
                    <h2 class="contentTitle"><?php echo $info['gname']; ?>分组权限管理</h2>
                </ul>
            </dl>
            <dl>
                <dd><label style="padding-left: 60px;height: 30px;width: 300px;"><input type="checkbox" name="c1" value="1" <?php if($u[0]!=0)echo 'checked=true';?>"/> 需要进行日常打开报告的权限</label></dd>
                <dd>
                    <label style="padding-left: 60px;height: 30px;width: 300px;"><input type="checkbox" name="c2" value="2" <?php if($u[1]!=0)echo 'checked=true';?>/> 领导层，需要日常进行监管</label>
                </dd>
                <dd>
                    <label style="padding-left: 60px;height: 30px;width: 300px;"><input type="checkbox" name="c3" value="3"  <?php if($u[2]!=0)echo 'checked=true';?>"/> 日常进行测试的用户</label>
                </dd>
            </dl>

        </div>
        <div class="formBar">
            <ul>
                    <!--<li><a class="buttonActive" href="javascript:;"><span>保存</span></a></li>-->
                <li><div class="buttonActive"><div class="buttonContent"><button type="submit">修改权限</button></div></div></li>
                <li>
                    <div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
                </li>
            </ul>
        </div>
    </form>
</div>
