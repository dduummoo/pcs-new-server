<div class="pageContent">
    <form method="post" action="<?php echo site_url('C=Ugroup_Permission&F=rgedit') ?>" class="pageForm required-validate" onsubmit="return validateCallback(this,dialogAjaxDone);">
        <div class="pageFormContent nowrap" layoutH="56">
            <input type="hidden" name="suid"  value="<?php echo $this->session->suid; ?>">
            <input type="hidden" name="dbid"  value="<?php echo $info['dbid']; ?>">
            <dl>
                <dt>报表名称：</dt>
                <dd>
                    <select name="report_id" class="required">
                        <?php
                        foreach ($reportlist as $report) {
                            if($info['reportid']==$report['id'])echo "<option value='{$report['id']}' selected='selected'>{$report['name']}</option>";
                            else echo "<option value='{$report['id']}'>{$report['name']}</option>";
                        }
                        ?>
                    </select>
                </dd>
            </dl>
            <dl>
                <dt>分组名称：</dt>
                <dd>
                    <select name="ugroupid" class="required">
                        <?php
                        foreach ($ugrouplist as $group) {
                            if($info['ugroupid']==$group['id']) echo "<option value='{$group['id']}' selected='selected'>{$group['name']}</option>";
                            else echo "<option value='{$group['id']}'>{$group['name']}</option>";
                        }
                        ?>
                    </select>
                </dd>
            </dl>
            <dl>
                <dt>填写频率：</dt>
                <dd><input name="frequency" class="required" type="text" size="30" value="<?php echo round($info['frequency']/3600,4)?>" alt="小时为单位" />
                    <span class="info">多少小时填写一次报表</span>
                </dd>
            </dl>

            <dl>
                <dt>敏感期是否开启：</dt><input name = "sp" value = "1" <?php  if ($info['sp']) echo 'checked="checked"' ?> type="checkbox">
            </dl>

            <p>
                <label>敏感期开始时间：</label>
                <input type="text" name="date_start" class="date" dateFmt="yyyy-MM-dd HH:mm:ss" value="<?php echo date('Y-m-d H:i:s',$info['date_start']);?>" />
                <a class="inputDateButton" href="javascript:;">选择</a>
            </p>
            <p>
                <label>敏感期结束时间：</label>
                <input type="text" name="date_end" class="date" dateFmt="yyyy-MM-dd HH:mm:ss" value="<?php echo date('Y-m-d H:i:s',$info['date_end']);?>"/>
                <a class="inputDateButton" href="javascript:;">选择</a>
            </p>

            <dl>
                <dt>敏感期的填写频率：</dt>
                <dd><input name="s_frequency" type="text" size="30" value="<?php echo round($info['s_frequency']/3600,4)?>" alt="小时为单位" />
                    <span class="info">多少小时填写一次报表</span>
                </dd>
            </dl>

            <dl>
                <dt>是否开启：</dt>
                <dd><input name = "stop" value = "1" <?php  if ($info['stop']) echo 'checked="checked"' ?> type="checkbox">
                </dd>
            </dl>



        </div>
        <div class="formBar">
            <ul>
                <!--<li><a class="buttonActive" href="javascript:;"><span>保存</span></a></li>-->
                <li><div class="buttonActive"><div class="buttonContent"><button type="submit">保存</button></div></div></li>
                <li>
                    <div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
                </li>
            </ul>
        </div>
    </form>
</div>
