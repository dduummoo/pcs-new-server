<form id="pagerForm" method="post" action="<?php echo site_url('C=User&F=ulist') ?>">
    <input type="hidden" name="status" value="${param.status}">
    <input type="hidden" name="keywords" value="${param.keywords}" />
    <input type="hidden" name="pageNum" value="1" />
    <input type="hidden" name="numPerPage" value="${model.numPerPage}" />
    <input type="hidden" name="orderField" value="${param.orderField}" />
</form>



<div class="pageContent">

    <table class="table" width="100%" layoutH="48">
        <thead>
        <tr>
            <th width="10">序号</th>
            <th width="20">分组名称</th>
            <th width="10">权限</th>
            <th width="10">监督分组</th>
        </tr>
        </thead>
        <tbody>
            <?php foreach ($ugrouplist as $row) { ?>
            <tr target="groupid" rel="<?php echo $row['gid']; ?>">
                <td><?php echo $row['id']; ?></td>
                <td><?php echo $row['name']; ?></td>
                <td>
                    <a title="编辑" target="dialog" href="../index.php?C=Ugroup_Permission&F=ugpedit_view&gid=<?php echo $row['gid']; ?>" width ="900" class="edit" rel="showpermissionedit">编辑</a>
                </td>
                <td>
                    <a title="编辑" target="dialog" href="../index.php?C=Ugroup_Permission&F=ugmgedit_view&gid=<?php echo $row['gid']; ?>" width ="900" height ="600" class="edit" rel="showmonitoredit">编辑</a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
<!--    <div class="panelBar">-->
<!--        <div class="pages">-->
<!--            <span>显示</span>-->
<!--            <select class="combox" name="numPerPage" onchange="navTabPageBreak({numPerPage:this.value})">-->
<!--                <option value="20">20</option>-->
<!--                <option value="50">50</option>-->
<!--                <option value="100">100</option>-->
<!--                <option value="200">200</option>-->
<!--            </select>-->
<!--            <span>条，共${totalCount}条</span>-->
<!--        </div>-->
<!---->
<!--        <div class="pagination" targetType="navTab" totalCount="200" numPerPage="20" pageNumShown="10" currentPage="1"></div>-->
<!---->
<!--    </div>-->
</div>
