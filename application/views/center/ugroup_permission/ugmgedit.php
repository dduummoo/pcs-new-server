<div class="pageContent">
    <form method="post" action="<?php echo site_url('C=Ugroup_Permission&F=ugmgedit_add') ?>" class="pageForm required-validate" onsubmit="return validateCallback(this,dialogAjaxDone);">
        <div class="pageFormContent nowrap"  style="height: 533px; overflow: auto;" layoutH="56">
            <dl>
                <ul >
                    <h2 class="contentTitle"><?php echo $info['gname']; ?>分组权限管理</h2>
                    <input type="hidden" name="mid"  value="<?php echo $info['mid']; ?>">
                </ul>
            </dl>
            <dl>
                <dt>新增：</dt>
                <dd style="width: 200px;">
                    <select name="mongid" >
<!--                        <option value="0">根分组</option>-->
                        <?php
                        foreach ($ugrouplist as $group) {
                            echo "<option value='{$group['gid']}'>{$group['name']}</option>";
                        }
                        ?>
                    </select>
                </dd>
                <div class="buttonActive"><div class="buttonContent"><button type="submit">新增一个监管分组</button></div></div>
            </dl>
            <dl>
                <table class="table" width="100%" layoutH="48">
                    <thead>
                    <tr>
                        <th width="10">已监管分组id</th>
                        <th width="20">已监管分组名称</th>
                        <th width="10">操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($monlist as $row) { ?>
                        <tr target="groupid" rel="<?php echo $row['dbid']; ?>">
                            <td><?php echo $row['id']; ?></td>
                            <td><?php echo $row['name']; ?></td>
                            <td>
                                <a title="不再监督" class="delete" target="ajaxTodo" href="../index.php?C=Ugroup_Permission&F=ugmgedit_delete&dbid=<?php echo $row['dbid']; ?>" width ="900" class="edit" rel="check_no_monitor" title="确定不再监督吗?">不再监督</a>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </dl>

    </form>
</div>
