<div class="pageContent">
    <form method="post" action="<?php echo site_url('C=User&F=uedit') ?>" class="pageForm required-validate" onsubmit="return validateCallback(this,dialogAjaxDone);">
        <div class="pageFormContent nowrap" layoutH="56">
            <input type="hidden" name="id"  value="<?php echo $info['id']; ?>">
            <dl>
                <dt>用 户 名：</dt>
                <dd><input name="username" type="text" size="30" value="<?php echo $info['name']; ?>"  class="required" /></dd>
            </dl>
            <dl>
                <dt>密    码：</dt>
                <dd><input name="password" type="text" size="30" placeholder="空表示不修改" minlength="6"/><span class="info">字母、数字、下划线 6-20位</span></dd>
            </dl>
            <dl>
                <dt>分    组：</dt>
                <dd>
                    <select name="ugroupid" >
                        <?php
                        foreach ($ugrouplist as $group) {
                            if($group['id'] == $info['groupid']) echo "<option value='{$group['id']}' selected='selected' >{$group['name']}</option>";
                            else echo "<option value='{$group['id']}'>{$group['name']}</option>";
                        }
                        ?>
                    </select>
                </dd>
            </dl>

        </div>
        <div class="formBar">
            <ul>
                    <!--<li><a class="buttonActive" href="javascript:;"><span>保存</span></a></li>-->
                <li><div class="buttonActive"><div class="buttonContent"><button type="submit">保存</button></div></div></li>
                <li>
                    <div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
                </li>
            </ul>
        </div>
    </form>
</div>
