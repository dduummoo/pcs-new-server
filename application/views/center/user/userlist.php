<head>
    <script type="text/javascript">
        function dbltable(target,rel){
            if( target != ''){
                console.log(rel);
//            $.pdialog.open(url, dlgId, title，option);
                $.pdialog.open("../index.php?C=User&F=uedit_view&uid="+rel,"showUseredit","修改",{ width: 900, height: 300});
            }
        }
    </script>
</head>
<form id="pagerForm" method="post" action="<?php echo site_url('C=User&F=ufind') ?>">
    <input type="hidden" name="pageNum" value="1" />
    <input type="hidden" name="numPerPage" value="<?php echo $info['page_size']?>" />
    <input type="hidden" name="status" value="${param.status}">
    <input type="hidden" name="username" value="<?php echo $info['uname']?>" />
    <input type="hidden" name="ugroupid" value="<?php echo $info['ugroupid']?>" />
    <input type="hidden" name="orderField" value="${param.orderField}" />
</form>

<div class="pageHeader">
    <form onsubmit="return navTabSearch(this);" action="<?php echo site_url('C=User&F=ufind') ?>" method="post">
        <div class="searchBar">
            <table class="searchContent">
                <tr>
                    <td>
                        用户名：<input type="text" name="username" />
                    </td>
                    <td>
                        分    组：
                            <select name="ugroupid" >
                                <option value="0">全部分组</option>
                                <?php
                                    foreach ($ugrouplist as $group) {
                                        echo "<option value='{$group['id']}'>{$group['name']}</option>";
                                    }
                                ?>
                            </select>
                    </td>

                </tr>

            </table>
            <div class="subBar">
                <ul>
                    <li><div class="buttonActive"><div class="buttonContent"><button type="submit">检索</button></div></div></li>
                </ul>
            </div>
        </div>
    </form>
</div>
<div class="pageContent">
    <div class="panelBar">
        <ul class="toolBar">
            <li><a class="add" href="../index.php?C=User&F=uadd_view" target="dialog" width ="900" rel="showUseradd"><span>添加</span></a></li>
            <li><a class="delete" href="../index.php?C=User&F=udel&uid={userid}" target="ajaxTodo" title="确定要删除吗?"><span>删除</span></a></li>
            <li><a class="edit" href="../index.php?C=User&F=uedit_view&uid={userid}" target="dialog" width ="900" rel="showUseredit" id = "111"><span>修改</span></a></li>
        </ul>
    </div>
    <table class="table" width="100%" layoutH="138">
        <thead>
        <tr>
            <th width="20">序号</th>
            <th width="50">用户名</th>
            <th width="60">分组</th>
        </tr>
        </thead>
        <tbody>
            <?php
            if(count($datalist)>0) {
            foreach ($datalist as $row) { ?>
            <tr target="userid" rel="<?php echo $row->uid; ?>">
                <td><?php echo $row->id; ?></td>
                <td ><?php echo $row->username; ?></td>
                <td><?php echo $row->group_name; ?></td>
            </tr>
            <?php }} ?>
        </tbody>
    </table>
    <div class="panelBar">
        <div class="pages">
            <span>显示</span>
            <select name="numPerPage" onchange="navTabPageBreak({numPerPage:this.value})">
                <option value="20" <?php if($info['page_size']==20) echo 'selected = "selected"'?>>20</option>
                <option value="50" <?php if($info['page_size']==50) echo 'selected = "selected"'?>>50</option>
                <option value="100" <?php if($info['page_size']==100) echo 'selected = "selected"'?>>100</option>
                <option value="200" <?php if($info['page_size']==200) echo 'selected = "selected"'?>>200</option>
            </select>
            <span>条，共<?php echo $info['total']?>条</span>
        </div>

        <div class="pagination" targetType="navTab" totalCount="<?php echo $info['total']?>" numPerPage="<?php echo $info['page_size']?>" pageNumShown="<?php echo ceil($info['total']/$info['page_size'])?>" currentPage="<?php echo $info['current']?>"</div>

</div>
</div>
