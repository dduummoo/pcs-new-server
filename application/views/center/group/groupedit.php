<div class="pageContent">
    <form method="post" action="<?php echo site_url('C=Group&F=gedit') ?>" class="pageForm required-validate" onsubmit="return validateCallback(this,dialogAjaxDone);">
        <div class="pageFormContent nowrap" layoutH="56">
            <input type="hidden" name="id"  value="<?php echo $info['gid']; ?>">
            <dl>
                <dt>分 组 名：</dt>
                <dd><input name="groupname" type="text" size="30" value="<?php echo $info['gname']; ?>"  class="required" alt="<?php echo $info['gname']; ?>" /></dd>
            </dl>
            <dl>
                <dt>上级分组：</dt>
                <dd>
                    <select name="fgroupid" >
                        <?php
                        foreach ($ugrouplist as $group) {
                                if($group['id'] == $info['fgid']) echo "<option value='{$group['id']}' selected='selected' >{$group['name']}</option>";
                                else echo "<option value='{$group['id']}'>{$group['name']}</option>";
                        }
                        ?>
                    </select>
                </dd>
            </dl>

        </div>
        <div class="formBar">
            <ul>
                    <!--<li><a class="buttonActive" href="javascript:;"><span>保存</span></a></li>-->
                <li><div class="buttonActive"><div class="buttonContent"><button type="submit">保存</button></div></div></li>
                <li>
                    <div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
                </li>
            </ul>
        </div>
    </form>
</div>
