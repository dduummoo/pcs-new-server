<?php

/**
 * 生成的模板model
 * Created by PhpStorm.
 * User: cyy
 * Date: 2016/7/20
 * Time: 23:30
 */
class Report_Column_Type_Model extends CI_Model
{
    //映射数据库的表名
    public static $DB = "report_column_type";

    //映射数据库的列
    public static $COL_ID = 'id';
    public static $COL_REPORT_ID = 'report_id';
    public static $COL_COLUMN_NAME = 'column_name';
    public static $COL_COLUMN_TYPE = 'column_type';


    //表示类型信息，按需要进行扩展
    public static $TYPE_ERROR = "error"; 	// 操作失败
    public static $TYPE_SUCCESS = "success"; 	// 操作失败

    //表示函数返回信息，按需要进行扩展
    public static $MSG_NOT_EXIST = "not_exist"; // 不存在
    public static $MSG_NOT_EMPTY = "not_empty"; // 当前不空

    /**
     * 请用标准化注释方法，先写函数体，再写参数，再写返回值，完毕之后，
     * 在函数上方键入《/**》然后回车，自动生成注释，再添加参数的类型，返回内容的格式，函数解释等信息
     */
    public function demo(){

    }

    function __construct(){
        parent::__construct();
        $this->load->database();
    }

    /**
     * @param int $report_id 报表的id
     * @param string $column_name 列名
     * @param string $column_type 列对应的类型
     * @return array   array('rs'=>'result); result=success/error
     */
    public function add($report_id,$column_name,$column_type){
        $data = array(
            self::$COL_REPORT_ID=>$report_id,
            self::$COL_COLUMN_NAME=>$column_name,
            self::$COL_COLUMN_TYPE=>$column_type
        );
        $this->db->insert(self::$DB,$data);
        if($this->db->affected_rows()>0){
            log_message('info',$this->db->last_query());
            return array('rs'=>'success');
        }

        return array('rs'=>'error');
    }
//    public function delete($id){
//        $this->db->where(self::$COL_ID,$id);
//        $q = $this->db->delete(self::$DB);
//        if($q){
//            log_message('info', $this->db->last_query());
//            return array('rs'=>'success');
//        }
//        return array('rs'=>'error');
//    }

    /**
     * 根据报表的id和列名获取对应的类型type
     * 
     * @param int $report_id  报表Id
     * @param string $column_name 列名
     * @return  bool bool 判断是否可以修改
     */
    public function is_exist($report_id,$column_name,$type){
        $array = array('report_id'=>$report_id,'column_name' => $column_name);
        $this->db->where($array);
        $report_colimn_type = $this->db->get(self::$DB)->first_row();
        return $report_colimn_type->type===$type;
    }
}