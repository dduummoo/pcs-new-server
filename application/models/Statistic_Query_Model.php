<?php
/**
 * Created by PhpStorm.
 * User: Realdata
 * Date: 2016/8/11
 * Time: 11:57
 */
class Statistic_Query_Model extends  CI_Model{
//    private static $monitor_id='monitor_id';
    private $start_time;    //存储查询的开始时间
    private $end_time;      //存储查询的结束时间
    private $tmp_var;  //存储临时变量
    private $where_arr ; //存储where条件
    private $array_uid;//根据uid获取所有呗监控的人的id
    private $report_id;     //mongo里面对应的报表的id
    private $column;        //单个报表里对应的column
    private static $COL_Time='time';//Mongo报表数据的时间
    private static $PREFIX_DB='report_data_';//存储数据的mongo中的collection的前缀
    private static $UN_VISIBLE_ITEMS;
    private  static $WHERE_SEPARATE_CHAR=",";//where条件中多个条件按什么隔开
    function __construct(){
        $this->load->library("Calc");
        $this->load->library('session');
        $this->start_time = 0;
        $this->end_time = time();
        if(isset($_SESSION['uid'])){
            $id = $_SESSION['uid'];
        }else if(isset($_SESSION['suid'])){
            $id = 0;
        }else{
//            echo "您尚未登录或者登录已经过期";
//            return;
            $id = 0;
        }
        $this->set_in_uid($id);
        $this->load->library("mongo_db");
        self::$UN_VISIBLE_ITEMS=array('time','GPS','IP','UA','IMEI','uid','from');
    }


    /**
     * 根据获得的监察者的id获取所有能监察到的用户的id，最后面搜索的时候做in操作
     *
     * @param  int  $uid 监察者的id
     */
    private function set_in_uid($uid){
        $this->load->model('User_Model','user');
        if($uid === 0){
            $group_id=0;
        }else {
            $result = $this->user->findId($uid);
            if ($result['rs'] == 'error') {
                return;
            }
            $group_id = $result['data']->group_id;
        }
        $this->load->model('Monitor_Grant_Model','monitor');
        $this->array_uid = $this->monitor->get_groups_and_users_by_group_id($group_id);
        for($i=0;$i<count($this->array_uid);$i++){
            $this->array_uid[$i] = intval( $this->array_uid[$i]);
        }
    }

    /**
     *
     * 负责设定查询时间区间的开始时间,接受2016-08-08 13:26:30 ，2016-08-08，13:26:30这种形式
     *
     * @param $start_time_str
     */
    private function set_start_time($start_time_str){
        $this->start_time = strtotime($start_time_str);
    }

    /**
     *
     * 负责设定查询时间区间的结束时间,接受2016-08-08 13:26:30 ，2016-08-08，13:26:30这种形式
     *
     * @param string $end_time_str
     */
    private function set_end_time($end_time_str){
        $this->end_time = strtotime($end_time_str);
    }

//    function link_to_mongo(){
//        $connection = new Mongo(mongodb://192.168.1.5:27017); //链接到 192.168.1.5:27017//27017端口是默认的。
//        $connection = new Mongo( "example.com" ); //链接到远程主机(默认端口)
//        $connection = new Mongo( "example.com:65432" ); //链接到远程主机的自定义的端口
//        print_r($connection->listDBs());//能打印出数据库数组，看看有几个数据库。
//    }


    /**
     * @return array array
     * 格式
     * Array
     *(
     *[result] => Array
     *(
     *[0] => Array
     *(
     *[_id] => 1111
     *[avg_uid] => 13
     *)
     *
     *[1] => Array
     *(
     *[_id] => 
     *[avg_uid] => 13
     *)
     *
     *)
     *
     *[ok] => 1
     *)
     */
    private function avg(){
        
        //TODO
        //根据report_id column where_arr 进行查找返回
//        return $this->sum()/$this->count();
        $match = $this->where();
        if($match == null){
            return null;
        }
        $r = $this->mongo_db->aggregate(self::$PREFIX_DB.$this->report_id,array(
            $match,
            array(
                '$group'=>array(

                    "_id"=>"null",'avg_'.$this->column=>array(
                        '$avg'=>"$".$this->column)))));
        if($r['result']==null){
            return 0;
        }
        return  $r['result'][0]['avg_'.$this->column];
    }

    /**
     * @return mixed
     */
    private function sum(){
        //TODO
        //根据report_id column where_arr 进行查找返回
        $match = $this->where();
        if($match == null){
            return null;
        }
        $r = $this->mongo_db->aggregate(self::$PREFIX_DB.$this->report_id,array(
            $match,
            array(
                '$group'=>array(
                    "_id"=>"null",'total_'.$this->column=>array(
                        '$sum'=>"$".$this->column)))));
        if($r['result']==null){
            return 0;
        }
        return  $r['result'][0]['total_'.$this->column];
    }

    /**
     * 查找符合条件的条目数量
     *
     * @return array $r返回数组
     */
    private function count(){
        //TODO
        //根据report_id column where_arr 进行查找返回
//        $q = $this->do_where_arr();//返回执行查询条件后的$q
//        if($q == null){
//            return 0;
//        }
//        return $q->count(self::$PREFIX_DB.$this->report_id);
        $match = $this->where();
        if($match == null){
            return null;
        }
        $r = $this->mongo_db->aggregate(self::$PREFIX_DB.$this->report_id,array(
            $match,
            array(
                '$group'=>array(
                    "_id"=>"null",'count_'.$this->column=>
                        array(
                            '$sum'=>1)))));
        if($r['result']==null){
            return 0;
        }
               return $r['result'][0]['count_'.$this->column];
    }


    private function do_where_arr(){
        $q = $this->mongo_db;
        $report_template = $this->get_report_template_by_report_id($this->report_id);
        foreach ($this->where_arr  as $key=>$value) {
            foreach ($value as $key_1=>$value_1){
                if($value_1[0]=='=='){
                    $value = $value_1[1];
                    if($report_template[$key]['type'] == 'int'){
                        $value = (int)$value_1[1];
                    }else if($report_template[$key]['type'] == 'float'){
                        $value = (float)$value_1[1];
                    }
                    $q = $q->where($key,$value);
                }else if($value_1[0]=='<='){
                    $q = $q->where_lte($key,$value_1[1]);
                }else if($value_1[0]=='>='){
                    $q = $q->where_gte($key,$value_1[1]);
                }
            }
        }
        return $q;
    }

    /**
     * 根据传递过来的字符串搜索对应的记录
     *
     * @return array array('data'=>d,'key'=>k) d为搜索出来的记录，k为搜索出来的记录中所有的键值
     */
    private function find_data_and_keys(){
        $q = $this->do_where_arr();
        $data = $q->get(self::$PREFIX_DB.$this->report_id);
        $keys = $this->get_keys($data);
        $data_and_keys = array('data'=>$data,'keys'=>$keys);
        return $data_and_keys;
    }

    /**
     * @param string $column 分组的列名
     * @return array $r表示分组后的数据
     */
    private function find_by_group($column,$column_value){
       return $this->mongo_db->where(array($column=>$column_value))->get(self::$PREFIX_DB.$this->report_id);
    }
    /**
     * @param array $data 传递的记录数组
     * @return array array 返回所有的键值
     */
    private function get_keys($data){
        $keys = array();
        for($i =0;$i<count($data);$i++){
            foreach($data[$i] as $key=>$value){
                if(!in_array($key,$keys)){
                    array_push($keys,$key);
                }
            }
        }
        return $keys;
    }

    /**
     * @param string $column 列名
     * @return array $r表示以$column分组所获得的汇总的集合
     * Array
     *(
     *[result] => Array
     *(
     *[0] => Array
     *(
     *[_id] => 1111
     *[total_uid] => 39
     *)
     *
     *[1] => Array
     *(
     *[_id] => 
     *[total_uid] => 52
     *)
     *
     *)
     *
     *[ok] => 1
     *)
     */
    private function list_sum($column){
        $match = $this->where();
        if($match == null){
            return null;
        }
        $r = $this->mongo_db->aggregate(self::$PREFIX_DB.$this->report_id,array(
          $match,
            array(
                '$group'=>array(
                "_id"=>"$".$column,'sum_'.$this->column=>array(
                    '$sum'=>"$".$this->column)))));
        if($r == null){
            return null;
        }
        $r['list'] = $column;
        $r['column']= $this->column;
        $r['method'] = 'list_sum';
        return  $r;
    }

    /**
     * @param string $column 按什么来分组，列名
     * @return array array返回按分组的数量
     */
    private function list_count($column){
        $match = $this->where();
        if($match == null){
            return null;
        }
        $r = $this->mongo_db->aggregate(self::$PREFIX_DB.$this->report_id,array(
            $match,
            array(
                '$group'=>array(
                    "_id"=>"$".$column,'count_'.$this->column=>array(
                        '$sum'=>1)))));
        $r['list'] = $column;
        $r['column']= $this->column;
        $r['method'] = 'list_count';
        return  $r;
    }

    /**
     * @param string  $column 按什么来分组，列名
     * @return  array array返回按分组的平均数
     */
    private function list_avg($column){
        //TODO
        //根据report_id column where_arr 进行查找返回
//        return $this->sum()/$this->count();
        $match = $this->where();

        if($match == null){
            return null;
        }
        $r = $this->mongo_db->aggregate(self::$PREFIX_DB.$this->report_id,array(
            $match,
            array(
                '$group'=>array(
                    "_id"=>"$".$column,'avg_'.$this->column=>array(
                        '$avg'=>"$".$this->column)))));
        $r['list'] = $column;
        $r['column']= $this->column;
        $r['method'] = 'list_avg';
        return  $r;
    }
    /**
     *  根据where_arr将所有的条件都执行一下返回聚合函数需要的match条件
     *
     * @return array array表示聚合函数中match的条件
     */
    private function where()
    {
//        return $this->mongo_db->where($where_arr);
//        array('$match' => array('data_name'=>array('$gte'=>"李四",'$lte'=>'李四'))),
        $match = array();
        $match['$match'] = array();
        if(!array_key_exists('time',$this->where_arr)){
            $match['$match']['time']['$gte'] = $this->start_time;
            $match['$match']['time']['$lte'] = $this->end_time;
        }
        foreach ($this->where_arr as $key => $value) {
                foreach ($value as $key_1 => $value_1) {
                    $report_template = $this->get_report_template_by_report_id($this->report_id);
                    $value = $value_1[1];
                    if(in_array($key,array_keys($report_template))){
                        if($report_template[$key]['type'] == 'int'){
                            $value = (int)$value_1[1];
                        }else if($report_template[$key]['type'] == 'float'){
                            $value = (float)$value_1[1];
                        }
                    }

                    if ($value_1[0] == '==') {
                        $match['$match'][$key] = $value;
                    } else if ($value_1[0] == '<=') {
                        $match['$match'][$key]['$lte'] = $value;
                    } else if ($value_1[0] == '>=') {
                        $match['$match'][$key]['$gte'] = $value;
                    }
                }
        }
        if(isset($match['$match']['uid'])){
            if(!in_array($match['$match']['uid'],$this->array_uid)){
                return null;
            }
        } else{
            $match['$match']['uid'] = array('$in'=>$this->array_uid);
        }
        return $match;
    }

    //.,都是有特殊的含义,$str中不能随意出现.,
    /*
        预先处理 start_time,end_time
        $str = xxx==aaa;time>=2012-06-07;time<=2015-08-10
        单句
        id.column.where($str).count();
        单句
        id.column.where($str).avg();
        单句
        id.column.where($str).sum();

        组合句子
        total = id.column.where().sum();
        num = 3 + id.column.where(xxx==aaa:time>=start_time:time<=end_time).count();
        result = total / num; //最后一个必须是result，变量为result的值将会显示出来
    */


    /*
     * 首先会替换start_time标签，end_time标签
     * @param string $line 需要计算的表达式
     */
    public function query($str){
        $this->tmp_var = array();
//        $str = str_replace("start_time",$this->start_time,$str);
//        $str = str_replace("end_time",$this->end_time,$str);
        //首先将时间处理成时间戳
        //首先把一个连续的表达式
        $array_query = explode(";",$str);
        for($k = 0;$k<count($array_query);$k++){//遍历多个表达式...
            $arr = explode(".",$array_query[$k]);//分割一个表达式

            if(array_key_exists(2,$arr)&&strpos($arr[2],"where")===0){
                $where_arr = explode(",",substr($arr[2],6,strlen($arr[2])-7));//分割条件
                $count = count($where_arr);
                for($j=0;$j<$count;$j++) {
                    if (strpos($where_arr[$j], "time") === 0) {
                        if(stripos($where_arr[$j],">=")){
                            $num = stripos($where_arr[$j],">=");
                        }else if(stripos($where_arr[$j],"<=")){
                            $num = stripos($where_arr[$j],"<=");
                        }
                        $time = substr($where_arr[$j],$num+2,strlen($where_arr[$j])-6);
                        if(!is_numeric($time)){
                            $time = strtotime($time);
                        }
                        $where_arr[$j] = substr($where_arr[$j],0,$num+2).$time;
                    }
                }
                //首先需要得到where套件，
                $arr[2] = "where(".implode(",", $where_arr).")";
                $array[$k] =  implode(".",$arr);
            }
        }//到这里已经将条件中的这种2015-10-10 12:20:21全部转成时间戳
//        $str_arr = explode(";",$str);
//        print_r($array_query);
        foreach ($array as $line){
            if($line!=null) {
                $this->calc_one_line($line);
            }
        }
        return isset($this->tmp_var["result"]) ? $this->tmp_var["result"] : 0;
    }


    /**
     *
     * 我们假定处理的语句val = id.column.where().sum() * 3
     * @param string $line 需要计算的 表达式
     * 计算一个完整的长句子的值
     */
    public function calc_one_line($line){
        if(preg_match("/([^=\s]+)\s*?=\s*?(.+)/",$line,$matches) == 0){
            return;
        }
        $key = $matches[1];   // val
//        print_r($key);
        $calculate = $matches[2];  // id.column.where().sum() * 3
//        print_r($calculate);
        //如果一行没有表达式，没有加减乘除，那么要么就是一个数字，或者一个变量，那么就是一个子表达式
        if(preg_match("/(.+)([\+\-\*\/])(.+)/",$calculate,$calc_line) == 0){
            $index = strpos($line,'=');
            $this->tmp_var[$key]=$this->calc_one_sub_line($calculate);
            return;
        };
        $left = $calc_line[1];    // id.column.where().sum()
        $op = $calc_line[2];    //  *
        $right = $calc_line[3]; //  3
        if($op == "+"){
            $this->tmp_var[$key] = $this->calc_one_sub_line($left) +
                $this->calc_one_sub_line($right);
        }elseif ($op == "-"){
            $this->tmp_var[$key] = $this->calc_one_sub_line($left) -
                $this->calc_one_sub_line($right);
        }elseif($op == "*"){
            $this->tmp_var[$key] = $this->calc_one_sub_line($left) *
                $this->calc_one_sub_line($right);
        }elseif($op == "/"){
            $this->tmp_var[$key] = sprintf("%.2f",$this->calc_one_sub_line($left) /
                $this->calc_one_sub_line($right));
        }
    }

    /**
     *
     * 输出的字符串是  id.column.where(xxx==aaa:time>=start_time:time<=end_time).count() 这种形式的
     *  report_id.column.where().list_sum(group_by_cloumn)
     *  report_id.column.where().list_scount(group_by_cloumn)
     *  report_id.column.where().list_acg(group_by_cloumn)
     * @param string $sub_line 需要计算的 表达式
     * @return int|string 输出个数或者是统计的总数
     */
    public function calc_one_sub_line($sub_line){
        //如果子表达式的一侧是一个数字，那么直接返回
        if(is_numeric($sub_line)){
            return $sub_line + 0;
        }

        //如果子表达式一侧是一个变量，那么就在临时数组查询变量值
        if(isset($this->tmp_var[$sub_line])){
            return $this->tmp_var[$sub_line];
        }
        //既不是一个数字，也不是一个变量，那么只能是一个表达式，开始计算
        //每查询一个子表达式，都要清空where条件，防止where条件污染其他表达式
        $this->where_arr = array();
        $sub_line_arr = explode(".",$sub_line);
        if(sizeof($sub_line_arr) != 4){//说明可能就是计算前两次结果的表达式
           return 0;
        }
        $report_id = intval($sub_line_arr[0]);//报表的Id'
        $column = $sub_line_arr[1];//uid
        $where = substr($sub_line_arr[2],6,strlen($sub_line_arr[2]) - 7);//为where括号里面的值
        $op = $sub_line_arr[3];//为计算方式,sum()或者list_sum()
        $this->report_id = $report_id;
       if(in_array($column,self::$UN_VISIBLE_ITEMS)){
        $this->column = $column;
       }else{
           $this->column = "data_".$column;
       }
        //TODO
        //这里使用对应mongo里面的report_id和column，
        //处理where
        $this->parse_where($where);
        if($op == "sum()"){
            return $this->sum();
        }
        if($op == "avg()"){
            return $this->avg();
        }
        if($op == "count()"){
            return $this->count();
        }
        if($op == "find()"){
            return $this->find_data_and_keys();
        }
        if(strpos($op,'list_sum')!==false){
            $column =substr($op,18,strlen($op)-19);
            return $this->list_sum($column);
        }
        if(strpos($op,'list_avg')!==false){
            $column =substr($op,18,strlen($op)-19);
            return $this->list_avg($column);
        }
        if(strpos($op,'list_count')!==false){
            $column =substr($op,20,strlen($op)-21);
            return $this->list_count($column);
        }
    }

    /**
     *
     * 处理where条件，这里传入的格式是 name==曹原野:time>=0，time<=1400000,id==3
     * 这种格式，:表示的意思是 AND 的意思
     *
     * @param string  $where 'name==曹原野:time>=0:time<=1400000:id==3'或者'name==曹原野，time>=2015-10-5 13:34:10，time<=2015-10-6 13:34:10:id==3'
     */
    public function parse_where($where){
        if($where == ""){
            return ;
        }
        $where_arr = explode(self::$WHERE_SEPARATE_CHAR,$where);
        $this->where_arr = array();
        foreach ($where_arr as $value){
            if(preg_match("/(.*)(==|<=|>=)(.*)/",$value,$matches) == 0){
                continue;
            }
            $key = $matches[1];   //where条件的键
        
            $op = $matches[2];  // ==   <=  >=  这几种操作
        
            if($key == "time"){
                $value = strtotime($matches[3]);//判断如果是time>=或者time<=这种则将时间转换成时间戳
            }else {
                $value = $this->calc->calculate($matches[3]);
            }
            if($value == null){
                $value = $matches[3];
            }
            //where条件的值,支持四则运算表达式
            //TODO
            //处理where_arr数组
            //$this->where_arr
            if(in_array($key,self::$UN_VISIBLE_ITEMS)) {//如果是不可见数组，关键字不包含data_
                if(!array_key_exists($key,$this->where_arr)){
                    $this->where_arr[$key] = array();
                }
                array_push($this->where_arr[$key],array($op,$value));
            }else {
                if(!array_key_exists('data_'.$key,$this->where_arr)) {//如果不属于不可见数组，关键字包含data_
                    $this->where_arr['data_' . $key] = array();
                }
                array_push($this->where_arr['data_' . $key], array($op, $value));
            }
        }

    }

    /**
     * 根据报表id获取对应的报表模板对象
     * 
     * @param int $report_id 报表id
     * @return object $report_template 报表模板
     */
    private function get_report_template_by_report_id($report_id){
        $this->load->model("Report_Template_Model","report_template");
        $report_template = $this->report_template->get_one_report_template_by_report_id($report_id);
        return $report_template;
    }
}