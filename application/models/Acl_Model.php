<?php
/**
 * 负责处理权限控制规则的增删查改
 * Created by PhpStorm.
 * User: cyy
 * Date: 2016/7/20
 * Time: 23:30
 */
class Acl_Model extends CI_Model
{
    //映射数据库的表名
    public static $DB = "acl";

    //映射数据库的列
    public static $COL_ID = "id";
    public static $COL_CONTROL = "control";
    public static $COL_FUNC = "func";
    public static $COL_TYPE = "type";


    //下面3个字段表示 acl权限控制的类型
    public static $TYPE_SUPER_USER = "super_user"; //如果是超级管理员，不拦截
    public static $TYPE_LOGIN = "login"; //如果是login，只判断普通用户是否登录
    public static $TYPE_LOGIN_AND_CHECK = "login_and_check"; //如果是login_and_check，判断登录而且分组有权限

    //返回信息
    public static $MSG_FUNC_EXIST = "func_exist"; //同一个控制器下有相同的函数名了。
    public static $MSG_NOT_EMPTY = "not_empty"; // 当前不空
    public static $MSG_DB_ERROR = "db_error"; //数据库失败。

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
     * 增加一个acl控制规则，注意，同一个control下不准有两个相同名字的func
     *
     * @param string $control 控制器的模块名
     * @param string $func 控制器的执行函数名
     * @param string $type 我们准备进行限制的类型
     * @return array  返回增加的结果，array("rs":"aa","msg":"bb") ,aa==success/error bb当发生func存在时，返回MSG_FUNC_EXIST
     */
    public function add($control,$func,$type){
        $this->db->where(array(self::$COL_CONTROL=>$control));
        $this->db->where(array(self::$COL_FUNC=>$func));
        $q = $this->db->get(self::$DB);
        if($q->num_rows() > 0) {
            //此control下已经存在func。
            return array('rs'=>'error', 'msg' => $this::$MSG_FUNC_EXIST);
        }
        $acl = array(
            self::$COL_CONTROL=>$control,
            self::$COL_FUNC=>$func,
            self::$COL_TYPE=>$type
        );
        $this->db->insert(self::$DB,$acl);
        if($this->db->affected_rows()>0){
            return array('rs'=>'success', 'msg' => null);
        }
        log_message('error',$this->db->last_query().'添加控制条目到acl失败');
        return array('rs'=>'error', 'msg' => $this::$MSG_DB_ERROR);
    }

    /**
     * 删除一条存在的规则，注意：本条规则必须在没人使用的情况下，才能删除
     * @param int $id 准备删除的规则的id
     * @return array 返回删除的结果，array("rs":"aa","msg":"bb") ,aa==success/error bb当发生准备删除的规则仍有人呢使用时，返回MSG_NOT_EMPTY
     */
    public function delete($id){
        $this->load->model("Acl_Rule_Model","aclRuleModel");
        $aclRuleModel = $this->aclRuleModel;
        $re = $aclRuleModel->findByAclId($id);
        if($re['data'] != null){
            return array("rs"=>"error", "msg"=>self::$MSG_NOT_EMPTY);
        }
        $this->db->where(self::$COL_ID,$id);
        $q = $this->db->delete(self::$DB);
        if($q){
            return array('rs'=>'success', 'msg'=>null);
        }
        log_message('info',$this->db->last_query().'从acl删除出错');
        return array('rs'=>'error', msg=>self::$MSG_DB_ERROR);
    }

    /**
     *
     * 搜索acl 数据表
     *
     * @param array $dict 查询数据库的键值对，id = 1;func="" 这种形式
     * @return array 返回数据库的结果集，不需要分页
     */
    public function find($dict){

        $this->db->where($dict);
        $q = $this->db->get(self::$DB);
        if($q->num_rows()>0){
            return array("data"=>$q->result());
        }
        return array("data"=>null);
    }
    /**
     *
     * 得到所有数据
     *
     * @param array $dict 查询数据库的键值对，id = 1;func="" 这种形式
     * @return array 返回数据库的结果集，不需要分页
     */
    public function findAll(){
        $q = $this->db->get(self::$DB);
        if($q){
            return $q->result();
        }
        return null;
    }

    /**
     * 修改某个规则的信息
     *
     * @param int $id 需要修改的规则的id
     * @param array $update_key 需要修改的规则的数据库的列名
     * @param array $update_value 修改的规则的数据库的值
     * @return array 返回更新的结果，array("rs":"msg") ,msg==success/error
     */
    public function update($id,$update_key,$update_value){
        $this->db->where(self::$COL_ID,$id);
        $arr=array_combine($update_key,$update_value);
        $this->db->update(self::$DB,$arr);//表名字 传入数组
        if($this->db->affected_rows()>0){
            return array('rs'=>'success');
        }
        log_message('error', $this->db->last_query().'更新acl出错.');
        return array('rs'=>'error');
    }
}