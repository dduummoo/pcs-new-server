<?php
/**
 * Created by PhpStorm.
 * User: Realdata
 * Date: 2016/8/5
 * Time: 13:09
 */

    class Monitor_Grant_Model extends CI_Model
    {
        //映射数据库的表名
        public static $DB = "monitor_grant";

        //映射数据库的列
        public static $COL_ID = 'id';
        public static $COL_MONITOR_UGROUP_ID = 'monitor_ugroup_id';
        public static $COL_MONITED_UGROUP_ID = 'monited_ugroup_id';
        private static $children = 'children';
        private static $groups_and_users = 'groups_and_users';
        //表示类型信息，按需要进行扩展
        public static $TYPE_ERROR = "error";    // 操作失败
        public static $TYPE_SUCCESS = "success";    // 操作失败

        //表示函数返回信息，按需要进行扩展
        public static $MSG_NOT_EXIST = "not_exist"; // 不存在
        public static $MSG_NOT_EMPTY = "not_empty"; // 当前不空

        function __construct()
        {
            parent::__construct();
            $this->load->database();
        }


        /**
         * 添加一个monitor_grant对象
         *
         * @param int $monitor_ugroup_id 查看统计模块的所在的分组
         * @param int $monited_ugroup_id 被查看的分组id
         * @return array array('rs'=>result) result='success'/'error'返回添加的结果
         */
        public function add($monitor_ugroup_id, $monited_ugroup_id)
        {
            $data = array(
                self::$COL_MONITOR_UGROUP_ID => $monitor_ugroup_id,
                self::$COL_MONITED_UGROUP_ID => $monited_ugroup_id
            );
            if($this->judge_exists($monitor_ugroup_id,$monited_ugroup_id)){
                $this->db->insert(self::$DB, $data);
                if ($this->db->affected_rows() > 0) {
                    log_message('info', $this->db->last_query());
                    return array('rs' => 'success');
                }
            }
            return array('rs' => 'error');
        }

        public function judge_exists($monitor_id,$monited_id){
            $this->db->where(self::$COL_MONITOR_UGROUP_ID,$monitor_id);
            $this->db->where(self::$COL_MONITED_UGROUP_ID,$monited_id);
            $result = $this->db->get(self::$DB);
            if($result->num_rows()>0){
                return false;
            }
            return true;
    }
        /**
         * 删除一条monitor_grant对象
         *
         * @param int $id monitor_grant_id
         * @return array array 返回删除的结果，array('rs'=>result),result='error'/'success'
         */
        public function delete($id)
        {
            $this->db->where(self::$COL_ID, $id);
            $q = $this->db->delete(self::$DB);
            if ($q) {
                log_message('info', $this->db->last_query());
                return array('rs' => 'success');
            }
            return array('rs' => 'error');
        }

        /**
         * 根据id查找表记录
         *
         * @param int $id id
         * @return array  array('rs'=>'result','data'=>)搜索到的记录如果个数不为0result='success',data为查找到的对象
         */
        public function find_by_id($id)
        {
            $this->db->where(self::$COL_ID, $id);
            $q = $this->db->get(self::$DB);
            if ($q->num_rows() > 0) {
                return array('rs' => 'success', 'data' => $q->first_row());
            }
            return array('rs' => 'error', 'data' => null);
        }

        /**
         * 查询所有的记录
         *
         * @return array  array('rs'=>'result','data'=>)搜索到的记录如果个数不为0result='success',data为查找到的对象
         */
        public function find()
        {
            $q = $this->db->get(self::$DB);
            if ($q->num_rows() > 0) {
                return array('rs' => 'success', 'data' => $q->result());
            }
            return array('rs' => 'error', 'data' => null);
        }

        /**
         * 根据键值对和id更新表记录
         *
         * @param int $id 需要更新的记录的id
         * @param array $update_array 更新的键集合
         * @param array $update_array 更新的值集合
         * @return array array('rs'=>'result') result= success/error
         */
        public function update($id, $update_array, $update_array)
        {
            $this->db->where(self::$COL_ID, $id);
            $arr = array_combine($update_array, $update_array);
            $this->db->update(self::$DB, $arr);
            if ($this->db->affected_rows() > 0) {
                log_message('info', $this->db->last_query());
                return array('rs' => 'success');
            }
            return array('rs' => 'error');
        }

        /**
         * @param array $arr 所有的用户分组由缓存中获取
         * @param int $id   用户分组的id，就是监察者的用户所在的分组id
         * @return array array 返回的是某一个用户分组能监听到的所有的用户的id,是一个多维数组
         */
        private function arr_forId($arr, $id, &$rs)
        {
            if (!is_array($arr)) {
                return null;
            }
            for ($i = 0; $i < count($arr); $i++) {
                if ($arr[$i]['id'] == $id) {
                    if(array_key_exists("children",$arr[$i])) {
                        $rs = $arr[$i]['children'];
                    }else{
                        $rs = array();
                    }
                    break;
                } else if ($arr[$i]['id'] != $id && array_key_exists('children', $arr[$i]) && is_array($arr[$i]['children'])) {
                    $this->arr_forId($arr[$i]['children'], $id, $rs);
                }
            }
            return $rs;
        }

        /**
         * 根据监察者所在的分组可以获得所有监察的组的人员的id(本组和被监察者组的所有人员的id)
         *
         * @param int $group_id 用户分组的id
         * @return array  array返回的数组
         * 数组类型
         * Array(
         *[0] => 14
         *[1] => 24
         *[2] => 25
         *[3] => 31
         *)
         */
        public function get_groups_and_users_by_group_id($group_id)
        {
            $this->load->driver('cache', array('adapter' => 'redis'));
            $data = $this->cache->get(self::$groups_and_users);
            $result = array();
            if ($data == null) {
                $this->refresh_cashe();
            }
            $data = $this->cache->get(self::$groups_and_users);
            if($group_id === 0){
                $this->array_many_to_one($data, $result);
            }else {
                $this->arr_forId($data, $group_id, $rs);
                $this->array_many_to_one($rs, $result);
            }
            return $result;
        }

        /**
         * @param array $arr 将多维的数组转换为一维的数组
         * 类型是这样的 array(
         *          id:1,
         *          fatherid:12,
         *          users:array(1,2,3,4),
         *          children:array(
         *                   id:2,
         *                   fatherid:4,
         *                   users:(5,6,7,8)
         *                   children:....
         * )
         * )
         * @param array $rs 最后面返回的数组
         * @return array array 返回的数组
         * 数组类型
         * Array(
         *[0] => 14
         *[1] => 24
         *[2] => 25
         *[3] => 31
         *)
         */
        private function array_many_to_one($arr,&$rs)
        {
            foreach ($arr as $key => $value) {
                if ($key === 'users') {
                    $rs = array_merge($rs, $value);
                } else if ($key === 'children' || is_array($value)) {
                    $this->array_many_to_one($value, $rs);
                }
            }
            return $rs;
        }

        /**
         * @return array 获取所有的用户分组和对应的用户
         */
        public function get_groups_and_users()
        {
            $this->load->model('User_Model', 'user');
            $this->load->model('Group_Model', 'group');
            $users = $this->user->find_all()['data'];

            $groups = $this->group->get_group_array();;
            return $this->list_to_tree($groups, 0, $users);
        }

        /**
         * @param array $list 从数据库中找到的一维数组
         * @param int $root
         * @param array $users 所有的用户
         * @return array array|null 返回所有的用户分组和用户
         */
        private function list_to_tree($list, $root = 0, $users)
        {
            if ($list == null) {
                return null;
            }
            //创建Tree
            $tree = array();

            if (is_array($list)) {
                //创建基于主键的数组引用
                $refer = array();
                foreach ($list as $key => $data) {
                    $refer[$data['id']] = &$list[$key];
                    foreach ($refer[$data['id']] as $key1 => $data1) {
                        if ($key1 != 'id' && $key1 != 'father_id') {
                            unset($refer[$data['id']][$key1]);
                        }
                    }
                    $refer[$data['id']]['users'] = array();
                    for ($i = 0; $i < count($users); $i++) {
                        if ($users[$i]->group_id == $refer[$data['id']]['id']) {
                            array_push($refer[$data['id']]['users'], $users[$i]->id);
                        }
                    }
                }
                foreach ($list as $key => $data) {
                    //判断是否存在parent
                    $parantId = $data['father_id'];
                    if ($root == $parantId) {
                        $tree[] = &$list[$key];
                    } else {
                        if (isset($refer[$parantId])) {
                            $parent = &$refer[$parantId];
                            $parent['children'][] = &$list[$key];
                        }
                    }
                }
            }

            return $tree;
        }

        /**
         * 用于缓存所有的用户分组信息和每个组所有的人的结果
         */
        private function refresh_cashe(){
            $this->load->driver('cache',array('adapter' => 'redis'));
            $data = $this->get_groups_and_users();
            $this->cache->save(self::$groups_and_users,$data);//将所有的分组数据保存进redis
        }

        /**
         * 根据监察者id获取对应的记录
         *
         * @param $group_id  监察者id
         * @return array array 返回搜索的监察结果集
         */
        public function find_by_monitor_group_id($group_id){
           $q = $this->db->where(self::$COL_MONITOR_UGROUP_ID, $group_id)->get(self::$DB);
            return $q->result();
        }
    }