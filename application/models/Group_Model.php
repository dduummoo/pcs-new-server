<?php

/**
 * Created by PhpStorm.
 * User: cyy
 * Date: 2016/7/20
 * Time: 14:15
 *
 * 负责对用户组进行操作。负责处理用户（普通分组）的基本分组
 *
 * 用户组有一定的层级关系，father_id为0表示是顶级分组，不为0，则表示他是这个id的分组的下级分组
 *
 * 此模块要求在redis里进行缓存，所以进行增删改的时候，需要刷新缓存
 *
 * redis 中缓存的不是数据库的直接查询结果，而是一棵树形的结果。

  redis_save_data = array{
        [0] : array{
            "name":"高新区",
           "id":1,
           "children":array{
                [0] : array{
                   "name":"xx街道",
                   "id":2,
                   "children":array()
                    },
               [1] : array{
                   "name":"yy街道",
                   "id":2,
                   "children":array(...继续递归)
               }
            }
        },
        [1] : array{
            ...
        }
  }


 */
class Group_Model extends CI_Model
{

    private static $MSG_EXIST_LOOP = "上下存在环路"; //上下级存在环路的警告语
    private static $MSG_EIXST_NAME = "该组下面有相同的组名"; //同一个组下面不准有两个名字相同的组名，但是不同组的下面允许有相同的组名
    private static $MSG_CHILDREN_NOT_EMPTY = "下级分组不为空"; //下级分组不为空，不准删除
    private static $MSG_USER_NOT_EMPTY = "分组下面的用户不为空"; //分组下面的用户不为空，不准删除
    private static $MSG_FATHERID_NOT_EXIST = "fatherid_not_exist"; //父ID不存在
    private static $tree_group_key='user_groups';
    private static $path = 'path';
    private static $group_to_level_array='level_groups';
    private static $group_id='id';
    private static $group_name='name';
    private static $group_fatherid='father_id';
    private static $group_db='ugroup';
    private static $children='children';
    private static $repeat_space='&nbsp;&nbsp;&nbsp;&nbsp;';
    private static $all_paths=null;
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }


    /**
     * 增加一个用户组，注意：需要能判断插入是否形成了一个环路。如果检测存在环路，就不插入，并返回相关错误信息。检测同一个组下面有无相同的组名
     *
     * @param string $name 用户组的名称
     * @param int $father_id 用户组，默认是0，表示是顶级用户组
     * @return array 返回增加的结果，array("rs":"aa","msg":"bb") ,aa==success/error bb等于error的具体原因，
     *                  $MSG_FATHERID_NOT_EXIST表示父亲id不存在，$MSG_EIXST_NAME表示同一分组下有相同名字
     */
    public function add($name, $father_id = 0){
        $result = $this->existFatherid($father_id);//判断父亲id是否存在如果不存在就直接返回不执行添加操作
        if($result['rs']=='error'){
            return array('rs'=>'error','msg'=>self::$MSG_FATHERID_NOT_EXIST);
        }
        $arr = $this->checkName($name, $father_id);
        if($arr['rs']=='error'){
            return array('rs'=>'error','msg'=>self::$MSG_EIXST_NAME);
        }
        $ugroup=array(
            self::$group_name=>$name,
            self::$group_fatherid=>$father_id
        );
        $this->db->insert(self::$group_db,$ugroup);
        if($this->db->affected_rows()>0){
            log_message('info',$this->db->last_query());
            $this->refresh_cache();
            return array('rs'=>'success','msg'=>null);
        }
        return array('rs'=>'error','msg'=>null);
    }

    /**
     * 判断该id所在的数组中father_id对应的组是否存在，如果存在就产生回路了
     * @param $father_id  某个用户分组的father_id
     * @return array array('rs'=>'msg'); msg=success/error
     */
    private function existFatherid($father_id){
        if($father_id==0){
            return array('rs'=>'success');
        }
        $arr = $this->find();
        if($arr == null) {//用户组为空添加用户组的父亲id只能为0
            return array('rs' => 'error');
        }

        return $this->findId($arr,$father_id);
    }
    /**
     * 去redis中找有没有这个id(这个函数我自己调用你们别调用)
     *
     * @param array $arr redis中存储的所有的分组结果
     * @param int $id 分组的id
     * @return  array array('rs'=>) 返回结果rs=success/error
     */

    public function findId($arr,$id){
        for($i=0;$i<count($arr);$i++) {
            if($arr[$i][self::$group_id] == $id) {
              return array('rs'=>'success');
            }else if (array_key_exists(self::$children,$arr[$i]) && is_array ($arr[$i][self::$children])){
                $result =  $this->findId($arr[$i][self::$children],$id);
                if($result['rs']=='success'){
                    return array('rs'=>'success');
                }
            }
        }
            return array('rs' => 'error');
    }

    /**
     * 去mysql数据库中查找对应id的用户分组
     * @param  int $id 用户分组的id
     * @return array array('rs'=>,'data'=>) rs为搜索结果，data为搜索到的数据
     */
    public function find_by_id($id){
      $this->db->where(array(self::$group_id=>$id));
        $q = $this->db->get(self::$group_db);

        if($q->num_rows()>0){
//            log_message('info', $this->db->last_query);
            return array('rs'=>'success','data'=>$q->first_row());
        }
        return array('rs'=>'error','data'=>null);
    }


    /**
     * 获得用户分组的层级数组
     * @return  arrayarray 返回关于模拟用户分组的层级数组
     * Array
     *(
     *[0] => Array
     *(
     *[id] => 26
     *[name] => 海淀区
     *[father_id] => 0
     *)
     *[1] => Array
     *(
     *[id] => 29
     *[name] =>   大钟寺派出所
     *[father_id] => 26
     *)
     * 主要是看name前面空格的个数，用于下拉列表框
     */
    public function level_groups(){
        $this->load->driver('cache',array('adapter' => 'redis'));
        $data = $this->cache->get(self::$group_to_level_array);
        if($data!=null){
            return $data;
        }
        $this->refresh_cache();
        $data = $this->cache->get(self::$tree_group_key);
        return $this->go($data);
    }

    private function walk(&$root,&$rs,$level){
        if($root == null){
            return;
        }
        if(array_key_exists(self::$children,$root)) {
            foreach ($root[self::$children] as $key => $node) {
                array_push($rs, array(
                    self::$group_id => $node[self::$group_id],
                    self::$group_name => str_repeat(self::$repeat_space, $level).$node[self::$group_name],
                    self::$group_fatherid => $node[self::$group_fatherid]
                ));
                $this->walk($root[self::$children][$key], $rs, $level + 1);
            }
        }
    }

     private function go($data){
        $header = array(
            self::$children => $data
        );
        $this->rs = array();
        $this->walk($header,$this->rs,0);
        return $this->rs;
    }


     /** 根据用户组id找到对应的路径
      * @param int $_id ugroup的id
      * @return array array  返回一个从上到下的层次路径集合。分组是含有上下级的关系，
      * 返回比如["海淀区总局","中关村派出所","巡警"] 代表属于 海淀区总局的中关村派出所的巡警
     */
    public function get_path_for_id($_id){

        if(self::$all_paths!=null){
            if(!array_key_exists($_id,self::$all_paths)){
                return null;
            }
            return self::$all_paths[$_id];
        }
        $this->load->driver('cache',array('adapter' => 'redis'));
        if($this->cache->get(self::$path)==null){
            $this->refresh_cache();
        }
        self::$all_paths = $this->cache->get(self::$path);
        if(!array_key_exists($_id,self::$all_paths)){
            return null;
        }
        return self::$all_paths[$_id];
    }

    /**
     * 返回redis中缓存的路径集合
     *
     * @return array array 返回所有的路径
     */
    public function  get_path(){
        $this->load->driver('cache',array('adapter' => 'redis'));
        $data = $this->cache->get(self::$path);
    
        if($data != null){
           return $data;
        }
        $this->refresh_cache();
       return $this->cache->get(self::$path);
    }

    /**
     * 将用户分组转换成路径数组
     *
     * @param array $groups 所有的用户分组
     * @param array $last 上一个循环中上一级的路径集合
     * @return array array 将树形的用户分组转换成路径集合
     */
    private function tree_for_path($groups,$last=null){
        static $paths = array();
        for($i=0;$i<count($groups);$i++) {
             if($last!=null){
                $group=$last;
             }
            $group[]=array(self::$group_id=>$groups[$i][self::$group_id],self::$group_name=>$groups[$i][self::$group_name]);
           $paths[$groups[$i]['id']]=$group;
             if (array_key_exists(self::$children,$groups[$i]) && is_array ($groups[$i][self::$children])){
                 $tmp = $group;
                 $group =null;
                $this->tree_for_path($groups[$i][self::$children],$tmp);
            }else{
                 $group =null;
             }
        }
        return $paths;
    }


    /**
     * 将对象数组转换成普通数组
     *
     * @param array $obj 对象数组
     * @return array array 普通的数组
     */
    private function objarray_to_array($obj) {
        $ret = array();
        foreach ($obj as $key => $value) {
            if (gettype($value) == "array" || gettype($value) == "object"){
                $ret[$key] =  $this->objarray_to_array($value);
            }else{
                $ret[$key] = $value;
            }
        }
        return $ret;
    }
    /**
     * 判断是否组成环路
     * @param int $id 用户组id
     * @param int $father_id 用户组所在的父亲结点的Id
     * @return array array   array('rs'=>rs)有回路rs=error,没有rs=success
     */

    public function checkLoop($id, $father_id){
        if($id == $father_id){
            return array('rs'=>'error');
        }
        $arr= $this->find();//获取缓存中的数据
        $rs = array();
        $this->arr_forId($arr,$id,$rs);
       return  $this->checkFatherId($rs,$father_id);
    }
    

    /**
     * 判断子节点中有没有节点的id为father_id,没有的话返回success，用于验证回路
     *
     * @param array $arr 从缓存中获取所有分组找到$id对应的分组
     * @param int $father_id 某一个分组的父亲结点的id
     * @return array array('rs'=>rs) arr中如果找到了$father_id说明产生的回路，rs='error',如果没找到就返回rs='success'
     */
    public function checkFatherId($arr,$father_id){
        if($arr == null) {
            return array('rs' => 'success');
        }
        for($i=0;$i<count($arr);$i++) {
            if($arr[$i][self::$group_id] == $father_id) {
                return array('rs'=>'error');
            }else if (array_key_exists(self::$children,$arr[$i]) && is_array ($arr[$i][self::$children])){
                 $result = $this->checkFatherId($arr[$i][self::$children],$father_id);
                if($result['rs']=='error'){
                    return  array('rs'=>'error');
                }
            }
        }
            return array('rs' => 'success');
    }

    /**
     * 递归查找某一个ID对应的数组
     * 
     * @param array $arr redis中所有用户分组信息
     * @param int $id 某一个用户分组的id
     * @return array array
     */
       private function arr_forId($arr,$id,&$rs)
    {
        if (!is_array ($arr)){
              return  null;
        }
       for($i=0;$i<count($arr);$i++) {
                 if($arr[$i][self::$group_id] == $id)
                 {//如果找到了id为$id,并且具有children节点并且children是数组就返回children节点
                     if(array_key_exists(self::$children,$arr[$i])) {
                         $rs= $arr[$i][self::$children];
                     }
                 }
                 else  if ($arr[$i]['id']!=$id &&array_key_exists(self::$children,$arr[$i])&&is_array ($arr[$i][self::$children])){
                    $this->arr_forId($arr[$i][self::$children],$id,$rs);
                 }
         }
        return $rs;

    }
    //判断该数组下所有的子节点的id是否等于该节点的父节点father_id
    //public function judge

    //判断同一组下面是否有相同的名字
    //@return array array 返回结果，array("msg":"aa") ,aa==success/error
    private function checkName($name, $father_id){
        $this->db->where(array(self::$group_name=>$name));
        $this->db->where(array(self::$group_fatherid=>$father_id));
        $q = $this->db->get(self::$group_db);
        if($q->num_rows()>0){
            return array('rs'=>'error',"father_id"=>$father_id);
        }
        return array('rs'=>'success');

    }


    /**
     * 将数据库中的ugroup中的数据取出来（一条一条的数据，非树形）
     * @return array  （一条一条的数据，非树形）
     */
    public function findAll(){
        $q = $this->db->get(self::$group_db);
        if($q){
            return $q->result();
        }
        return null;
    }
    //将数据库中取出来的组转换成一颗树
    /** self::$group_id,self::$group_fatherid,self::$children);
     * @param $list  普通数组作为数据源传入
     * @return array  一颗树
     * $tree=[
                {
                    "id": "1",
                    "name": "三",
                    "father_id": "0",
                     "children": [{
                            "id": "2",
                             "name": "aaaa",
                            "father_id": "1",
                             "children": [
                                   {
                                        "id": "12",
                                        "name": "a",
                                        "father_id": "2"
                                     }
                                ]
        },
        {
             "id": "4",
             "name": "aaaa",
             "father_id": "1",
             "children": [
                {
                     "id": "6",
                     "name": "a",
                     "father_id": "4",
                     "children": [
                        {
                             "id": "3",
                             "name": "aaaa",
                             "father_id": "6",
                            "children": [
                                .......
     */
    public function list_to_tree($list,$root = 0) {
        if($list == null){
            return null;
        }
        //创建Tree
        $tree = array();

        if (is_array($list)) {
            //创建基于主键的数组引用
            $refer = array();

            foreach ($list as $key => $data) {
                $refer[$data[self::$group_id]] = &$list[$key];
            }

            foreach ($list as $key => $data) {
                //判断是否存在parent
                $parantId = $data[self::$group_fatherid];

                if ($root == $parantId) {
                    $tree[] = &$list[$key];
                } else {
                    if (isset($refer[$parantId])) {
                        $parent = &$refer[$parantId];
                        $parent[self::$children][] = &$list[$key];
                    }
                }
            }
        }

      return $tree;
    }
    /**
     * 删除一个用户组
     *
     * 删除用户组必须满足两个条件，1、当前分组下面没有其他的下级分组。2、当前分组下没有普通用户 。
     *
     * @param int $id 准备删除的分组的id
     * @return array array 返回删除的结果，array("rs":"aa","msg":"bb") ,aa==success/error bb等于error的具体原因，
     *                      $MSG_CHILDREN_NOT_EMPTY表示分组下的子分组不为空不能删除，$MSG_USER_NOT_EMPTY该用户分组
     *                      下的用户不为空不能删除
     */

    public function delete($id){
        $result = $this->checkGroupHaveChildrenByID($id);
        if($result['rs'] == 'error'){
            return array('rs'=>'error','msg'=>self::$MSG_CHILDREN_NOT_EMPTY);
        }
        $this->load->model('User_Model','user');
        $result = $this->user->find(null,$id);//success说明还有，error说明该group_id下的user已经没有了
        if($result['rs'] == 'success'){
            return array('rs'=>'error','msg'=>self::$MSG_USER_NOT_EMPTY);
        }
        $this->db->where(self::$group_id,$id);
        $q = $this->db->delete(self::$group_db);
        if($q>0){
            log_message('info',$this->db->last_query());
            $this->refresh_cache();
            return array('rs'=>'success');
        }
        return array('rs'=>'error','msg'=>null);
    }
    //根据id判断是否还有下级分组
    /**
     * @return array array 返回删除的结果，array("rs":"aa" ,aa==success/error
     */

        private function checkGroupHaveChildrenByID($id){
            $this->db->where(self::$group_fatherid,$id);
            $q = $this->db->get(self::$group_db);
            if($q->num_rows()>0){
                return array('rs'=>'error');
            }
            return array('rs'=>'success');
}
    /**
     * 直接返回redis的缓存结果（树形结构)
     *
     * @return array 返回所有的分组的结果
     */
    public function find(){
        $this->load->driver('cache',array('adapter' => 'redis'));
        if($this->cache->get(self::$tree_group_key)==null){
            $this->refresh_cache();
        }
        return $this->cache->get(self::$tree_group_key);
    }
    

    /**
     * 修改某个分组的信息
     * 注意：需要能判断修改是否形成了一个环路。如果检测存在环路，就不插入，并返回相关错误信息。检测同一个组下面有无相同的组名
     * @param int $id 需要修改的分组的id
     * @param array $update_key 需要修改的分组的数据库的列名
     * @param array $update_value 修改的分组的数据库的值
     * @return array 返回更新的结果，array("rs":"msg") ,msg==success/error,$MSG_EXIST_LOOP表示产生了环路,
     *                  $MSG_FATHERID_NOT_EXIST表示父亲id不存在，$MSG_EIXST_NAME表示同以分组下有重名
     */
    public function update($id,$update_key,$update_value)
    {
        $arr = array_combine($update_key, $update_value);
        if (in_array(self::$group_fatherid, $update_key)) {//如果更新的键中有fathser_id这一列
            $result = $this->existFatherid($arr[self::$group_fatherid]);//判断父亲id是否存在如果不存在就直接返回不执行添加操作
            if ($result['rs'] == 'error') {
                return array('rs' => 'error', 'msg' => self::$MSG_FATHERID_NOT_EXIST);
            }
            $result = $this->checkLoop($id, $arr[self::$group_fatherid]);
            if ($result['rs'] == 'error') {
                return array('rs' => 'error', 'msg' => self::$MSG_EXIST_LOOP);
            }
        }
            $father_id =  $arr['father_id'];
            $result = $this->checkName($arr[self::$group_name],$father_id);
            if($result['rs']=='error'){
                return array('rs'=>'error','msg'=>self::$MSG_EIXST_NAME,"father_id"=>$father_id);
            }

        $this->db->where(self::$group_id,$id);
        $this->db->update(self::$group_db,$arr);
        $this->refresh_cache();
        return array('rs'=>'success','data'=>null);
    }

    public function get_group_array(){
        return $this->objarray_to_array($this->findAll());
    }
    /**
     * 刷新redis 中的分组缓存缓存，刷新的时候，需要把 分组的结构变成一棵树
     */
    private function refresh_cache(){
        $this->load->driver('cache',array('adapter' => 'redis'));
        $data = $this->list_to_tree($this->objarray_to_array($this->findAll()));//所有的基础分组数据
        $this->cache->save(self::$tree_group_key,$data);//将所有的分组数据保存进redis
        //$this->cache->save(self::$path,$this->get_path());这代码需要放在 $this->cache->save(self::$key,$data)代码下面
        //否则陷入死循环
        $this->cache->save(self::$path,$this->tree_for_path($data));//路径数组保存进redis
        $this->cache->save(self::$group_to_level_array,$this->go($data));//层级数组保存进redis
    }

    /**
     * @param $id   用户分组的id
     * @return array|null array 获取到的是一个分组以及所有他的子分组的id，比如26然后返回的Array ( [0] => 26 [1] => 29 [2] => 31 [3] => 32 [4] => 46 )
     */
    public function find_self_and_children_by_id($id){
        $group = $this->find_by_id($id)['data'];
        $rs = array($id);
        if($group == null){
          return $rs;
        }
       $arr= $this->find();//获取缓存中的数据
         $this->arr_forId($arr,$id,$children);
        if($children != null){
            $this->get_childrens_by_groups($children,$rs);
        }
        return $rs;
    }
//    public function find_children_users_id_by_group_id($group_id){
//        $arr= $this->find();//获取缓存中的数据
//        $children = $this->arr_forId($arr,$group_id);
//        print_r($arr);
//        return $children;
//    }
    public function get_childrens_by_groups($arr,&$rs){
        for($i =0;$i<count($arr);$i++){
            array_push($rs,$arr[$i]['id']);
            if(array_key_exists('children',$arr[$i])&&$arr[$i]['children'] != null){
                $this->get_childrens_by_groups($arr[$i]['children'],$rs);
            }
        }
    }
}