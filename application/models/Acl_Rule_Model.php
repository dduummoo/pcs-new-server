<?php
/**
 * 负责对acl权限里分配到具体的权限分组上。在acl的type为login_and_check时有效，则判定用户所在的分组是否有权限运行
 * Created by PhpStorm.
 * User: cyy
 * Date: 2016/7/20
 * Time: 23:31
 */
class Acl_Rule_Model extends CI_Model
{
    //映射数据库的表名
    public static $DB = "acl_rules";

    //映射数据库的列
    public static $COL_ID = "id";
    public static $COL_ACL_ID = "acl_id";
    public static $COL_UGROUP_ID = "ugroup_id";

    //返回信息
    public static $MSG_EXIST = "exist"; // 已经存在
    public static $MSG_NOT_NEED = "not_need"; //不需要这么做
    public static $MSG_NO_SUCH_ACL = "no such acl";
    public static $MSG_NO_SUCH_ACL_RULE = "no such acl rule";
    public static $MSG_DB_ERROR = "db_error"; //数据库失败。

	public static $key = "acl_rules";
	
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
     *
     * 准备对某条规则 分配到某个分组,注意，如果acl_id ugroupd_id 同时相同的记录 存在时，返回EXIST错误
     * 如果acl_id对应的type不是 login_and_check 的时候，返回 NOT_NEED 错误
     * @param int $acl_id acl表的中的id
     * @param int $ugroup_id  ugroup表中的id
     * @return array 返回增加的结果，array("rs":"aa","msg":"bb") ,aa==success/error bb当发生 acl_id ugroupd_id存在时，返回MSG_EXIST
     */
    public function add($acl_id,$ugroup_id){
        //检查acl的type
        $this->load->model("Acl_Model","aclModel");
        $aclModel = $this->aclModel;
        $aclArray = $aclModel->find(array($aclModel::$COL_ID => $acl_id));
        if($aclArray['data'] == null){
            return array("rs"=>"error","msg"=>self::$MSG_NO_SUCH_ACL);
        }
        $acl = $aclArray['data'][0];
        $key = $aclModel::$COL_TYPE;
        if($acl->$key != $aclModel::$TYPE_LOGIN_AND_CHECK){
            return array("rs"=>"error","msg"=>self::$MSG_NOT_NEED);
        }
        //检查是否有相同的acl和group_id 记录
        $this->db->where(array(self::$COL_ACL_ID=>$acl_id));
        $this->db->where(array(self::$COL_UGROUP_ID=>$ugroup_id));
        $q = $this->db->get(self::$DB);

        if($q->num_rows() > 0) {
            //已经存在规则
            return array('rs'=>'error', 'msg' => $this::$MSG_EXIST);
        }
        $aclRules = array(
            self::$COL_ACL_ID=>$acl_id,
            self::$COL_UGROUP_ID=>$ugroup_id
        );
        $this->db->insert(self::$DB,$aclRules);
        if($this->db->affected_rows()>0){
            $this->refresh_redis();
            return array('rs'=>'success', 'msg' => null);
        }
        log_message('error',$this->db->last_query().'添加控制规则到acl_rules失败');
        return array('rs'=>'error', 'msg' => $this::$MSG_DB_ERROR);
    }


    /**
     * 删除种类类型的acl控制信息，根据type的不同，删除不同的种类
     * @param string $type  $COLID $COL_ACL_ID $COL_UGROUP_ID 中的一个
     * @param int $id 如果type为acl_id 就删除acl_id == $id 的，以此类推
     * @return array 返回删除的结果，array("rs":"aa","msg":"bb") ,aa==success/error
     */
    public function delete($type,$id){
        $this->db->where($type,$id);
        $q = $this->db->delete(self::$DB);
        if($q){
            $this->refresh_redis();
            return array('rs'=>'success');
        }
        log_message('info',$this->db->last_query().'从acl_rules删除规则出错');
        return array('rs'=>'error');
    }

    /**
     * 返回 acl_id 为$acl_id 的数据
     *
     * 返回 array(data=>) null / 或者查到的记录
     */
    public function findByAclId($acl_id){
        $this->db->where(self::$COL_ACL_ID, $acl_id);
        $q = $this->db->get(self::$DB);
        if($q->num_rows()>0){
            return array("data"=>$q->result());
        }
        return array("data"=>null);
    }
    /**
     * 直接返回数据库所有的 acl的控制信息，并结合acl_rules的先关的ugroup_id结果
     * 此步需要直接返回 在 redis里的缓存
     *
     *
     * @return array 直接返回缓存的数据
     */
    public function find(){
        $this->load->driver('cache',array('adapter' => 'redis'));
        if($this->cache->get(self::$key)==null){
            $this->refresh_redis();
        }
        return $this->cache->get(self::$key);
    }
    /**
     * 修改某个控制信息
     * 如果acl_id ugroupd_id 同时相同的记录 存在时，返回EXIST错误
     * 如果acl_id对应的type不是 login_and_check 的时候，返回 NOT_NEED 错误
     * @param int $id 需要修改的控制信息的id
     * @param array $update_key 需要修改的控制信息的数据库的列名
     * @param array $update_value 修改的控制信息的数据库的值
     * @return array 返回更新的结果，array("rs":"msg") ,msg==success/error
     */
    public function update($id,$update_key,$update_value){
        $this->db->where(self::$COL_ID,$id);
        $q = $this->db->get(self::$DB);
        if($q->num_rows()<= 0){
            return array("rs"=>"error", 'msg'=>self::$MSG_NO_SUCH_ACL_RULE);
        }
        $key_gid = self::$COL_UGROUP_ID;
        $gid = $q->result()[0] -> $key_gid;
        $key_aid = self::$COL_ACL_ID;
        $acl_id = $q->result()[0] -> $key_aid;
        $arr=array_combine($update_key,$update_value);
        if(array_key_exists(self::$COL_ACL_ID, $arr)){
            //需要检查acl_id是否合法
            $this->load->model("Acl_Model","aclModel");
            $aclModel = $this->aclModel;
            $acl_id = $arr[self::$COL_ACL_ID];
            $aclArray = $aclModel->find(array($aclModel::$COL_ID => $acl_id));
            if($aclArray['data'] == null){
                return array("rs"=>"error","msg"=>self::$MSG_NO_SUCH_ACL);
            }
            $acl = $aclArray['data'][0];
            $key = $aclModel::$COL_TYPE;
            if($acl->$key != $aclModel::$TYPE_LOGIN_AND_CHECK){
                return array("rs"=>"error","msg"=>self::$MSG_NOT_NEED);
            }
        }
        if(array_key_exists(self::$COL_UGROUP_ID, $arr)){
            $gid = $arr[self::$COL_UGROUP_ID];
        }
        if(  $gid != $q->result()[0] -> $key_gid ||  $acl_id != $q->result()[0] -> $key_aid ){
            //检查更改后的acl_id 和 group_id是否存在
            $this->db->where(array(self::$COL_ACL_ID=>$acl_id));
            $this->db->where(array(self::$COL_UGROUP_ID=>$gid));
            $q = $this->db->get(self::$DB);
            if($q->num_rows() > 0) {
                //已经存在规则
                return array('rs'=>'error', 'msg' => $this::$MSG_EXIST);
            }
        }
        if(  $gid == $q->result()[0] -> $key_gid ||  $acl_id == $q->result()[0] -> $key_aid ){
            //没有更改
            return array('rs'=>'success', 'msg' => null);
        }
        $this->db->where(self::$COL_ID,$id);
        $this->db->update(self::$DB,$arr);//表名字 传入数组
        if($this->db->affected_rows()>0){
            $this->refresh_redis();
            return array('rs'=>'success', 'msg' => null);
        }
        log_message('error', $this->db->last_query().'更新acl_rules控制规则出错.');
        return array('rs'=>'error', 'msg' => self::$MSG_DB_ERROR);
    }

	  //将对象数组转换成普通数组
    public function objarray_to_array($obj) {
        $ret = array();
        if($obj == null){
            return $ret;
        }
        foreach ($obj as $key => $value) {
            if (gettype($value) == "array" || gettype($value) == "object"){
                $ret[$key] =  $this->objarray_to_array($value);
            }else{
                $ret[$key] = $value;
            }
        }
        return $ret;
    }
	
	//将aclRules 拼成缓存的格式
    private function getAllRules(){
        $this->load->model("Acl_Model","aclModel");
        $aclModel = $this->aclModel;
		//取出所有acl
        $aclArray = $aclModel->findAll();
		$aclArray = $this->objarray_to_array($aclArray);
		$aclData = array();
        for($i=0;$i<count($aclArray);$i++){
            $acl_id = $aclArray[$i][$aclModel::$COL_ID];
			$arr_rule = array();
			$arr_rule[$aclModel::$COL_TYPE] = $aclArray[$i][$aclModel::$COL_TYPE];
			if($aclArray[$i][$aclModel::$COL_TYPE] == $aclModel::$TYPE_LOGIN_AND_CHECK){
				$rules =  $this->objarray_to_array($this->findByAclId($acl_id)['data']);
				$arr_rule['ugroup'] = array();
				for($j=0;$j<count($rules);$j++){
					array_push($arr_rule['ugroup'], $rules[$j][self::$COL_UGROUP_ID]);
				}
			}
			$aclData[$aclArray[$i][$aclModel::$COL_CONTROL]."." . $aclArray[$i][$aclModel::$COL_FUNC]] = $arr_rule;
        }
		return $aclData;
    }
	
    /**
     * 在增删改之后，更新redis缓存的数据，参照Acl_Model里面type的各种字段解释
     * 在redis里缓存的数据，缓存的是某种特定格式的数据，如下所示

    redis_save_data = array(
        "Welcome.hello" : array(
            type : "login"			//此方法仅需要用户登录就可以执行
        ),

        "Welcome.test_insert" : array(
            type : "login_and_check",//此方法不仅需要登录，而且需要ugroup在授权范围内
            ugroup : array(
                1,  //id == 1 的分组可以执行
                2	//id == 2 的分组可以执行
            )
        )，

      "SupserUser.login" : array(
            type : "supser_user"  //此方法是超级管理员才可以执行
        )
    )

     *
     * 执行的时候，流程如下，通过$_GET{"C"] 获取请求的控制器的类名  $_GET["F"]获取请求的控制器的方法名，
     * 然后请求 本模块的find() ，获取redis缓存的 上面的这个结构。通过拼装的 类方法字符串，可以找到权限的说明。
     *
     * 上面结构 每个方法下的type，确定下一步的动作，是拒绝还是放行。但是具体的判断，在 hooks/Hook.php 进行判断，这里不进行操作，仅仅是提供获取的api
     */
    public function refresh_redis()
    {
        $this->load->driver('cache', array('adapter' => 'redis'));
        $data = $this->getAllRules();
        $this->cache->save(self::$key, $data);
    }
	
	
}