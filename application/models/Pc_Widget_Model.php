<?php

/**
 *
 * 负责把 mongodb 里面存储的 表格的模板数据，渲染成html表单
 *
 * 适合pc端使用这种场景
 *
 * Created by PhpStorm.
 * User: cyy
 * Date: 2016/7/31
 * Time: 18:28
 */
class Pc_Widget_Model extends CI_Model
{
    //某模板的解析结果进行缓存
    public static $REDIS_SAVE_KEY = "_ci_pc_widget";

    public static $TYPE_STRING = "string";
    public static $TYPE_NUMBER = "number";
    public static $TYPE_DATE = "date";
    public static $TYPE_DATE_TIME = "date_time";
    public static $TYPE_CHECKBOX = "checkbox";
    public static $TYPE_RADIO = "radio";
    public static $TYPE_SELECT = "select";
    public static $TYPE_TEXTAREA = "textarea";

    private $js_css_include; //存储css和js的头部的信息
    private $widget_html; //存储模板生成的html代码
    private $init_widget_js; //存储初始化的js代码



    public function get_js_css_include(){
        $this->js_css_include = '	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link href="./css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="./css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
	<link href="./css/magicsuggest-min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="./css/default.css">


	<!-- 百度编辑器 -->
    <link href="umeditor/themes/default/css/umeditor.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="umeditor/third-party/jquery.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="umeditor/umeditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="umeditor/umeditor.min.js"></script>
    <script type="text/javascript" src="umeditor/lang/zh-cn/zh-cn.js"></script>
    
    <!-- 百度编辑器 -->
    
    <script type="text/javascript" src="./js/bootstrap.min.js"></script>
	<script type="text/javascript" src="./js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
    <script type="text/javascript" src="./js/bootstrap-datetimepicker.zh-CN.js" charset="UTF-8"></script>
    <script src="./js/magicsuggest-min.js"></script>
    <script src="./js/bootstrap-wysiwyg.js"></script>';


        return $this->js_css_include;
    }

    public function get_widget_html(){
        return $this->widget_html;
    }
    public function get_init_widget_js(){
        return $this->init_widget_js;
    }


    /**
     * 根据一个模板，返回模板对应的html  源代码
     * @param array $report_template 从 mongo数据库读出来的单条模板
     * @return string 返回 从模板生成的报表的html文件
     */
    public function init($report_template){
        $this->widget_html = "";
        $this->init_widget_js = "";
        $this->js_css_include = "";

        foreach ($report_template as $key => $value){
            if($key != "id"){
                $this->get_widget($key,$value);
            }
        }
    }


    /**
     * 给定mongodb 的报表模板里的单条规则，生成对应的html
     *
     * @param array $item mongodb里存储的报表模板里的单条信息，一般有如下结构array("type":"","items":[])
     * @return string 返回对应的html字符串
     */
    private function get_widget($key,$item){
        $type = $item["type"];

        if($type == self::$TYPE_STRING){
            $this->get_string_widget($key,$item);
        }

        if($type == self::$TYPE_NUMBER){
            $this->get_number_widget($key,$item);
        }

        if($type == self::$TYPE_DATE){
            $this->get_date_widget($key,$item);
        }

        if($type == self::$TYPE_DATE_TIME){
            $this->get_datetime_widget($key,$item);
        }

        if($type == self::$TYPE_CHECKBOX){
            $this->get_checkbox_widget($key,$item);
        }

        if($type == self::$TYPE_RADIO){
            $this->get_redio_widget($key,$item);
        }

        if($type == self::$TYPE_SELECT){
            $this->get_select_widget($key,$item);
        }

        if($type == self::$TYPE_TEXTAREA){
            $this->get_textarea_widget($key,$item);
        }
    }


    private function get_string_widget($key,$item){
        $this->widget_html.=$item['reminder'].'：<input class="form-control my_widget" column="'.$key.'" type="text" /><br />'."\n";
    }
    private function get_number_widget($key,$item){
        $this->widget_html.=$item['reminder'].'：<input class="form-control my_widget" name=""  type="text" column="'.$key.'" onkeyup="this.value=this.value.replace(/[^0-9\.]/gi,\'\')" /><br />'."\n";
    }
    private function get_date_widget($key,$item){
        $tmpl = '	<div class="form-group">
		<label for="dtp_input1" class="col-md-2 control-label">%s</label>
		<div class="input-group date form_datetime col-md-5" data-date="" data-date-format="yyyy MM dd - HH:ii p" data-link-field="dtp_input1">
			<input class="form-control"  size="16" type="text" value="" readonly>
			<span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
			<span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
		</div>
		<input type="hidden" id="dtp_input1" class="my_widget" column="%s" value="" /><br/>
	</div>';
        //初始化插件的日期html部分
        $this->widget_html.=sprintf($tmpl,$item['reminder'],$key);


        //初始化插件的日期部分
        $this->init_widget_js .= "	<script>$('.form_date').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0
    });</script>\n";
    }
    private function get_datetime_widget($key,$item){
        $tmpl = '	<div class="form-group">
		<label for="dtp_input2" class="col-md-2 control-label">%s</label>
		<div class="input-group date form_date col-md-5" data-date="" data-date-format="yyyy MM dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
			<input class="form-control" size="16" type="text" value="" readonly>
			<span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
			<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
		</div>
		<input type="hidden" id="dtp_input2" class="my_widget" column="%s" value="" /><br/>
	</div>';

        //初始化内容部分
        $this->widget_html.=sprintf($tmpl,$item['reminder'],$key)."\n";

        //初始化日期时间插件的初始化部分
        $this->init_widget_js .= "    <script>$('.form_datetime').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		forceParse: 0,
        showMeridian: 1
    });</script>\n";
    }

    private function get_checkbox_widget($key,$item){
        $this->widget_html .=  $key."：";

        $tmpl = "<label class=\"checkbox \"><input type=\"checkbox\" column='%s' name='checkbox_%s' class='my_widget'  value=\"%s\"> %s</label>";


        foreach ($item["items"] as $value){
            $this->widget_html.= sprintf($tmpl,$key,$key,$value,$value);
        }

        $this->widget_html.="<br />\n";
    }

    private function get_redio_widget($key,$item){
        $this->widget_html .=  $key."：";

        $tmpl = "<label class=\"radio\"><input type=\"radio\" name=\"radio_%s\" class='my_widget' column='%s' value=\"%s\" >%s</label>";


        foreach ($item["items"] as $value){
            $this->widget_html.= sprintf($tmpl,$key,$key,$value,$value);
        }

        $this->widget_html.="<br />\n";
    }
    private function get_textarea_widget($key,$item){
        $this->widget_html .= $item['reminder']."：<br /><script type=\"text/plain\" id=\"myEditor\" style=\"width:1000px;height:240px;\">
    		<p>这里我可以写一些输入提示</p>
		</script>";
    }
    
    private function get_select_widget($key,$item){
        
    }




}