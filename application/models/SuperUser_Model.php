<?php

/**
 * Created by PhpStorm.
 * User: cyy
 * Date: 2016/7/20
 * Time: 21:37
 *
 * 系统只允许有一个系统管理员账号密码,默认数据库super_user表的id为1的用户就是超级管理员
 */
class SuperUser_Model extends CI_Model
{
    private static $super_db='super_user';
    private static $super_id='id';
    private static $super_name='username';
    private static $super_password='password';
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }
    /**
     * 一个超级用户进行登录o
     * @param string $username 用户名
     * @param string $password 经过hash的用户的密码，hash由前端负责hash，这里只管查询
     * @return array 返回数据库中命中的那一列的数组形式array("rs":"msg","data":obj),msg==success/error
     */
    public function login($username, $password){
        $this->db->where(array(self::$super_name=>$username));
        $this->db->where(array(self::$super_password=>$password));
        $q = $this->db->get(self::$super_db);
        if($q->num_rows() > 0) {
            return array("rs" => "success", "data" => $q->first_row());
        }
        return array("rs" => "error","data"=>null);
    }
  
    
    /**
     * 修改超级管理员的信息
     * @param array $key_arr 准备修改的数据库的列的数组
     * @param  int $suid 管理员id
     * @param array $value_arr 准备修改的数据库的值的数组
     * @return array 返回修改结果 array("rs":"msg"),msg==success/error
     */
    public function update($suid,$key_arr,$value_arr){
        $super = $this->find();
        if($super['rs']=='success'){
            $arr=array_combine($key_arr,$value_arr);
            $this->db->where(self::$super_id,$suid)->update(self::$super_db,$arr);//表名字 传入数组
            if($this->db->affected_rows()>0){
                log_message('info', $this->db->last_query());
                return array('rs'=>'success');
            }
        }
        return array("rs" => "success");
    }

    /**查找超级管理员是否存在
     * @return array  array('rs'=>result) result='error'/'success'
     */
    public  function find(){
        $q = $this->db->get(self::$super_db);
        if($q->num_rows() > 0&& $q->first_row()->id == 1) {
            return array("rs" => "success");
        }
         return array("rs" => "error");
    }
    public function findId($suid){
        $q = $this->db->where(array(self::$super_id=>$_SESSION['suid']))->get(self::$super_db);
       return $q->first_row();
    }
}