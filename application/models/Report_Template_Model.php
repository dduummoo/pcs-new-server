<?php
/**
 * Created by PhpStorm.
 * User: cyy
 * Date: 2016/7/31
 * Time: 15:23
 */


class Report_Template_Model extends CI_Model{
    public static $DB = "pcs";
    public static $COLLECTION  = "report_template";
    public static $CAN_NOT_HANDLE_UN_VISIBLE_ITEM='can not handle unvisible item';
    public static $CAN_NOT_FIND_REPORT_TEMPLATE='can not find report_template by id';
    public static $ILLEGAL_TYPE = 'illegal type';
    public static $TYPE_HAS_BEEN_ANOTHER_TYPE = 'can not use this type here';
    public static $UN_VISIBLE_ITEMS;
    public static $all_types;
    //type
    public static $TYPE_STRING = "text";
    public static $TYPE_INT = "int";
    public static $TYPE_FLOAT = "float";
    public static $TYPE_IDCARD = "idcard";

    public static $TYPE_TIME = "sys_time";
    public static $TYPE_DATE = "sys_date";
    public static $TYPE_DATE_TIME = "sys_datetime";

    public static $TYPE_CHECKBOX = "checkboxs";
    public static $TYPE_RADIO = "radios";
    public static $TYPE_SELECT = "select";
    public static $TYPE_TEXTAREA = "textarea";

    public function __construct(){
        self::$UN_VISIBLE_ITEMS['time']['type']=self::$TYPE_INT;//报表填写的时间
        self::$UN_VISIBLE_ITEMS['GPS']['type']=self::$TYPE_STRING;//移动端的时候，有用
        self::$UN_VISIBLE_ITEMS['IP']['type']=self::$TYPE_STRING;//均有用
        self::$UN_VISIBLE_ITEMS['UA']['type']=self::$TYPE_STRING;//User-Agent ，pc端的时候有用
        self::$UN_VISIBLE_ITEMS['IMEI']['type']=self::$TYPE_STRING;//手机串号，移动端有用，pc端为空
        self::$UN_VISIBLE_ITEMS['uid']['type']=self::$TYPE_INT;//填表人的id
        self::$UN_VISIBLE_ITEMS['from']['type']=self::$TYPE_INT ; //pc和mobile 二选一,1pc 2手机
        self::$all_types = array(self::$TYPE_INT,self::$TYPE_FLOAT,self::$TYPE_STRING,self::$TYPE_IDCARD,self::$TYPE_DATE,self::$TYPE_TIME,self::$TYPE_DATE_TIME,
            self::$TYPE_CHECKBOX,self:: $TYPE_RADIO,self::$TYPE_SELECT,self::$TYPE_TEXTAREA
            );
        $this->load->library("mongo_db");
    }

    /**
     * 初始化一个报表模板，然后，把不可见元素都插入进去(相当于添加一个report模板)
     *
     * @return string $rs->{'$id'} 返回插入到mongodb数据库中的报表模板的id
     */
    public function init($insert_data){
        $new_data = array_merge(self::$UN_VISIBLE_ITEMS,$insert_data);
        $rs = $this->mongo_db->insert(self::$COLLECTION,$new_data);
        if($rs==FALSE){
            return null;
        }
      return  $rs->{'$id'};
    }

    /**
     * 修改多个控件
     *
     * @param $_id  报表模板的id
     * @param $update_data  报表模板修改过后的更新数据
     * @return mixed
     */
    
    

    /**
     * 添加一列，然后呢，添加的时候，判断存在不存在，别冲突了。
     *
     * 不可见的元素是不能被修改的
     *
     * 添加的列的类型必须是上面self::$all_types中给定的
     *
     * @param int $_id  report_template模板的id
     * @param string $column_name 需要添加的列名
     * @param string $type 类型
     * @param string $reminder 需要提示的信息，默认为空
     * @param array $items 如果是单选复选这种需要传递的选项
     * @return array array('rs'=>,'msg'=>) rs为运行的结果，msg为错误的信息
     */
    public function add_one_column($_id,$column_name,$type,$reminder=null,$items = []){
//        //exist ???
//        //直接 $obj = get_one_report_template($_id)
        $report_template = $this->get_one_report_template($_id);
        $result = $this->change_is_ok($_id,$column_name,$type);
        if($result['rs'] == 'error'){
            return $result;
        }
        $report_template[$column_name]['type'] = $type;
        $report_template[$column_name]['reminder'] = $reminder;
        $report_template[$column_name]['items'] = $items;
        $this->mongo_db->where(array('_id'=>new MongoID($_id)))->replace(self::$COLLECTION,$report_template);
        return array('rs'=>'success');
    }


    /**
     * 删除一个item，不准删除不可见的元素
     *
     * @param int $_id repot_template的id
     * @param string $column_name 列名
     * @return  array   return array('rs'=>'','msg'=>null); rs为运行结果，msg为提示消息
     */
    public function delete_one_column($_id,$column_name){
        if(array_key_exists($column_name,self::$UN_VISIBLE_ITEMS)){
            return array('rs'=>'error','msg'=>self::$CAN_NOT_HANDLE_UN_VISIBLE_ITEM);
        }
        $arr = $this->get_one_report_template($_id);
        unset($arr[$column_name]);
        $this->mongo_db->where(array('_id'=>new MongoID($_id)))->replace(self::$COLLECTION,$arr);
        return array('rs'=>'success','msg'=>null);
    }

    /**
     * 更新一个报表模块
     *
     * @param  int $_id 报表模板的id
     * @param string $column_name 报表模板的列名
     * @param string  $type 报表模板的列的类型
     * @param string  $reminder 表模板的列的提示
     * @param  array $items 表模板的列如果是单选复选需要提供对应的选项集合
     * @return array   array('rs'=>'','msg'=>),rs:error/success,msg为消息
     */
    public function update_one_column($_id,$column_name,$type,$reminder,$items = []){
        return $this->add_one_column($_id,$column_name,$type,$reminder,$items);
    }

    /**
     * 去mysql中的report_column_type表中查询是否可以修改
     * @param $_id
     * @param $column_name
     * @param $type
     */
    private function change_is_ok($_id,$column_name,$type){
        $report_template = $this->get_one_report_template($_id);
        if(!$report_template){
            log_message('error','找不到report_template对象');
            return array('rs'=>'error','msg'=>self::$CAN_NOT_FIND_REPORT_TEMPLATE);
        };//判断修改的合理性
        if(array_key_exists($column_name,self::$UN_VISIBLE_ITEMS)) {
            return array('rs' => 'error', 'msg' => self::$CAN_NOT_HANDLE_UN_VISIBLE_ITEM);
        }
        if(!in_array($type,self::$all_types)){
            return array('rs'=>'error','msg'=>self::$ILLEGAL_TYPE);
        }

//        $this->load->model('Report_Model','report');
//        $this->load->model('Report_Column_Type_Model','report_column_type');
//        if(!$flag){
//            return array('rs'=>'error','msg'=>self::$TYPE_HAS_BEEN_ANOTHER_TYPE);
//        }
        return array('rs'=>'success','msg'=>null);
    }

    /**
     * 根据id获取一个报表模板
     *
     * @param int $_id 报表模板的id
     * @return  array  $report_template 返回一个模板实例，可能为空
     */
    public function get_one_report_template($_id){
        $report_template =  $this->mongo_db->where(array("_id"=>new MongoID($_id)))->find_one(self::$COLLECTION);
        return $report_template;
    }
    
    public function get_one_report_template_by_report_id($report_id){
        $this->load->model("Report_Model","report_model");
        $result = $this->report_model->findId($report_id);
        if($result !=null) {
            $_id = $result['data']->_id;
            $report_template =  $this->mongo_db->where(array("_id"=>new MongoID($_id)))->find_one(self::$COLLECTION);
            return $report_template;
        }
      return null;
    }
    /** 
     * 传递一个字符串以,隔开（这个字符串代表的是更改后的报表模板的各个可见模块的名字的顺序集合），
     *
     * 获取编辑过后的report_template对象
     *
     * @param string $keys_str为前端传过来的参数，格式为 'name,age';
     * @param array $data data为传过来的整个的report_template对象
     * @return array array返回的是修改后的整个模板对象
     */
    public function readEditReportTemplate($keys_str,$data){
        $newdata = array_slice($data,0,count(self::$UN_VISIBLE_ITEMS));
        $keys = explode(',',$keys_str); 
        for($i=0;$i<count($keys);$i++){
          $newdata[$keys[$i]]=$data[$keys[$i]];
        }
       return $this->mongo_db->where(array("_id"=>$data['_id']))->replace(self::$COLLECTION,$newdata);
    }

    /**
     * 获取某个模板中的可见元素
     *
     * @param $id 报表id
     * @return  array array(里面是一维数组),返回
     */
    public function get_visible_by_id($id){
        $this->load->model('Report_Model','report_model');
        $report = $this->report_model->findId($id)['data'];
        $report_template = $this->get_one_report_template($report->_id);
        $unvisible_size = count(self::$UN_VISIBLE_ITEMS)+1;
        $size = count($report_template);
        $report_template = array_slice($report_template,$unvisible_size,$size);
        $report_template['id'] = $report->id;
        return $report_template;
    }

    public function update_all_column($mongo_id,$update_report_template){
      return  $this->mongo_db->where(array('_id'=>new MongoID($mongo_id)))->replace(self::$COLLECTION,$update_report_template);
    }
}