<?php
/**
 * Created by PhpStorm.
 * User: Realdata
 * Date: 2016/7/28
 * Time: 12:51
 */
class Statistics_Report_Model extends  CI_Model
{
    public static $MSG_EXIST_NAME = "name_has_existed"; //统计方式的名字已经存在
    //映射数据库的表名
    public static $DB = "statistics_report";
    static $PAGE_SIZE=20;
    //映射数据库的列
    public static $COL_ID = 'id';//统计方式的id
    public static $COL_REPORT_ID = 'report_id';//报表的id
    public static $COL_CREATE_TIME = 'create_time';//创建的时间
    public static $COL_UPDATE_TIME = 'update_time';//最后更新时间
    public static $COL_NAME = 'name';//统计方式的名字
    public static $COL_CREATOR_ID = 'creator_id';//创建统计方式的人的id
    public static $COL_UPDATOR_ID = 'updator_id';//最后更新统计方式的人的id
    public static $COL_FORMDESIGN_TMPL = 'formdesign_tmpl';//报表统计方式的模板
    public static $COL_FORMDESIGN_PARSE = 'formdesign_parse';//formdesign的参数
    public static $COL_FORMDESIGN_DATA = 'formdesign_data';//formdesign的参数
    public static  $CREATOR_ID_NOT_EXIST='creator id not exist';//创建人不存在
    public static  $UPDATOR_ID_NOT_EXIST='updator id not exist';//更新者的id不存在
    public static $UPDATE_ARRAY_IS_NULL='update array is null';//更新的条件为空
    public static $MISSING_UPDATOR_ID='misssing the updator id';//更新者的id为空
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    public function change_page_size($page_size){
        self::$PAGE_SIZE = $page_size;
        return array("data"=>self::$PAGE_SIZE);
    }
    /**
     *
     * 添加一个统计方式
     *
     * @param int $report_id 表示此统计方式用于那个报表的计算
     * @param string $name  统计方式的名称
     * @param int $creator_id 创建统计方式的人的id
     * @param int $updator_id  最后一次更新人的id
     * @param string $tmpl 统计方式的模板
     * @param string $formdesign_tmpl  统计方式的前端代码
     * @param string $formdesign_parse formdesign的参数
     * @param string $formdesign_data  formdesign的参数
     * @return array 返回添加的结果，array("rs":"msg" ,msg==success/error
     */

    public function add($report_id,$name,$creator_id,$updator_id,$formdesign_tmpl,$formdesign_parse,$formdesign_data){
        $result = $this->checkName($name);
        if($result['rs'] == 'error'){
            return $result;
        }
        $data = array(
            self::$COL_REPORT_ID=>$report_id,
            self::$COL_CREATE_TIME=>time(),
            self::$COL_UPDATE_TIME=>time(),
            self::$COL_NAME=>$name,
            self::$COL_CREATOR_ID=>$creator_id,
            self::$COL_UPDATOR_ID=>$updator_id,
            self::$COL_FORMDESIGN_TMPL=>$formdesign_tmpl,
            self::$COL_FORMDESIGN_PARSE=>$formdesign_parse,
            self::$COL_FORMDESIGN_DATA=>$formdesign_data
        );

        $this->db->insert(self::$DB,$data);
        if($this->db->affected_rows()>0){
            log_message('info',$this->db->last_query());
            return array('rs'=>'success');
        }

        return array('rs'=>'error');
    }

    /**
     * 验证统计方式的名是否重名
     *
     * @param string $name 统计方式的名字
     * @return array array("rs":"result"，'msg'=>'data') ,result==success/error,msg=MSG_NAME_EXIST_TEST表示报表重名
     *                  $MSG_NAME_EXIST_TEST表示报表名包含test_是不允许的
     */
    private function checkName($name){
        $this->db->where(self::$COL_NAME,$name);
        $q = $this->db->get(self::$DB);
        if($q->num_rows()>0){
            log_message('info',$this->db->last_query());
            return array('rs'=>'error','msg'=>"name has exists");
        }
        return array('rs'=>'success','msg'=>null);
    }
    /**
     * @param int $id 统计方式的Id
     * @return array array('rs'=>'msg') msg=error/success
     */
    public function delete($id){
        $this->db->where(self::$COL_ID,$id);
        $q = $this->db->delete(self::$DB);
        if($q){
            log_message('info', $this->db->last_query());
            return array('rs'=>'success');
        }
        return array('rs'=>'error');
    }


    /**
    * 查找所有的统计方式
     *
    * @return array array('rs'=>'result','data'=>'data') result=success/error,data为查找出的数据
    */
    public function find(){
        $q = $this->db->get(self::$DB);
        return array('rs'=>'success','data'=>$q->result());
    }




    /**
     * 修改某个统计方式的信息
     * 
     * @param int $id 需要修改的统计方式的id
     * @param array $update_key 需要修改的统计方式的数据库的列名集合
     * @param array $update_value 修改的统计方式的数据库的值集合
     * @return array array("rs":"result",'msg'=>message)返回更新的结果 ,result==success/error,$UPDATE_ARRAY_IS_NULL更新的条件数组为空
     *                  $MISSING_UPDATOR_ID更新的数组中缺少updatorid键值对
     */
    public function update($id,$update_key,$update_value){
        $arr=array_combine($update_key,$update_value);
        if($arr == null){
            return array('rs'=>'error','msg'=>self::$UPDATE_ARRAY_IS_NULL);
        }
        if(!array_key_exists ( self::$COL_UPDATOR_ID, $arr)){
            return array('rs'=>'error','msg'=>self::$MISSING_UPDATOR_ID);
        }
        if(array_key_exists(self::$COL_NAME,$arr)){
            $this->db->where(self::$COL_NAME,$arr[self::$COL_NAME]);
            $q = $this->db->get(self::$DB);
            if($q->num_rows()>0){
                foreach($q->result() as $row){
                    if($row->id !=$id){
                        log_message('info',$this->db->last_query());
                        return array('rs'=>'error','msg'=>"name has exists");
                    }
                }
            }
        }
        $arr[self::$COL_UPDATE_TIME] = time();
        $this->db->where(self::$COL_ID,$id)->update(self::$DB,$arr);
        if($this->db->affected_rows()>0){
            log_message('info',$this->db->last_query());
            return array('rs'=>'success');
        }
        return array('rs'=>'error','msg'=>null);
    }


    /** 
     * 根据id查找统计方式是否存在,如果存在返回此对象
     * 
     * @paramc int $rid 统计方式的id
     * @return int array 返回查找的结果 array("rs":result,'data'=>data) ,result=='success'/'error'
     */
    public function findId($id){
        $this->db->where(self::$COL_ID,$id);
        $q = $this->db->get(self::$DB);
        if($q->num_rows()>0){
            return array('rs'=>'success','data'=>$q->first_row());
        }
        return array('rs'=>'error','data'=>null);
    }

    public function find_by_name($name,$pageNum){
        if($name!=null) {
           $total =  $this->db->like(self::$COL_NAME, $name)->count_all_results(self::$DB);
            $reports =  $this->db->like(self::$COL_NAME, $name)->limit(self::$PAGE_SIZE,$pageNum*self::$PAGE_SIZE)->get(self::$DB)->result();
        }else{
            $total = $this->db->count_all_results(self::$DB);
            $reports =  $this->db->limit(self::$PAGE_SIZE,$pageNum*self::$PAGE_SIZE)->get(self::$DB)->result();
        }
            return array("reports"=>$reports,"total"=>$total,"page_size"=>self::$PAGE_SIZE,"current"=>$pageNum);

    }
    /**
     *根据报表的id查找到所有的此报表统计方式
     *
     * @param int $report_id 报表的id
    * @return array   array('rs'=>'success','data'=>data); rs表示搜索结果，‘data’为查找到的所有对应此报表id的统计方式
     */
    public function get_statistics_report_by_report_id($report_id){
        $this->db->where(self::$COL_REPORT_ID,$report_id);
        $q = $this->db->get(self::$DB);
        if($q->num_rows()>0){
            return array('rs'=>'success','id'=>$q->result());
        }
        return array('rs'=>'error','id'=>null);
    }

    /**
     * 用户可以查看所有本组的人的统计方式,管理员查看所有的
     * 
     *  @param array $user_id 本组用户的id
     * @return array array所有本组的人员的Id
     */
    public function get_statistics_report_by_usersid_and_statistics_name($usersid=null,$name =null,$pageNum){
        if($usersid !== null) {
            $this->db->where_in(self::$COL_CREATOR_ID, $usersid);
        }else{
            return null;
        }
        if($name !== null){
            $this->db->like(self::$COL_NAME,$name);
        }
        $total = $this->db->count_all_results(self::$DB,false);
        $reports= $this->db->limit(self::$PAGE_SIZE,$pageNum*self::$PAGE_SIZE)->get()->result();
        return array("page_size"=>self::$PAGE_SIZE,"current"=>$pageNum,"total"=>$total,"reports"=>$reports);
    }
}