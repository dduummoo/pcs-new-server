<?php
/**
 * Created by PhpStorm.
 * User: BB
 * Date: 2016/8/2
 * Time: 23:10
 */
class Permission_Model extends CI_Model
{
    //映射数据库的表名
    public static $DB = "permission";

    //映射数据库的列
    public static $COL_ID = 'id';
    public static $COL_NAME = 'name';
    public static $COL_TXT = 'txt';


    //表示类型信息，按需要进行扩展
    public static $TYPE_ERROR = "error"; 	// 操作失败
    public static $TYPE_SUCCESS = "success"; 	// 操作失败

    //表示函数返回信息，按需要进行扩展
    public static $MSG_NOT_EXIST = "not_exist"; // 不存在
    public static $MSG_ALREADY_EXIST = "already_exist"; // 已经存在
    public static $MSG_NOT_EMPTY = "not_empty"; // 当前不空
    public static $MSG_DB_ERROR= "db_error"; // 数据库错误



    /**
     * Permission_Model constructor.
     */
    function __construct(){
        parent::__construct();
        $this->load->database();
    }

    /**
     * 给permission 表添加一条记录
     * @param string  $name 业务逻辑名称： 目前有 report montor test, 不能和已有名称重复
     * @param string $txt 对业务逻辑的备注
     * @return array  array("rs" => , "msg"=>)成功 'rs'=>'success', msg => null; 失败返回 array('rs'=>'error', msg=> 失败原因);
     */
    public function add($name,$txt){
        $this->db->where(array(self::$COL_NAME =>$name));
        $q = $this->db->get(self::$DB);
        if($q->num_rows() > 0) {
            //已经存在该逻辑名称
            return array('rs'=>'error', 'msg' => $this::$MSG_ALREADY_EXIST);
        }
        $data = array(
            self::$COL_NAME=>$name,
            self::$COL_TXT=>$txt
        );
        $this->db->insert(self::$DB,$data);
        if($this->db->affected_rows()>0){
            log_message('info',$this->db->last_query());
            return array('rs'=>'success', 'msg' => null);
        }
        log_message('error',$this->db->last_query().'添加到permission失败');
        return array('rs'=>'error', 'msg' => $this::$MSG_DB_ERROR);
    }

    /**
     * 删除id 为 $id的记录, 首先查询 ugroup_permission 表里有没有人用
     * @param int $id
     * @return array  array("rs" => , "msg"=>)成功 'rs'=>'success', msg => null; 失败返回 array('rs'=>'error', msg=> 失败原因);
     */
    public function delete($id){
        //检查是否有人应用这个逻辑权限
        $this->load->model("Ugroup_Permission_Model","uPermissionModel");
        $uPermissionModel = $this->uPermissionModel;
        $re = $uPermissionModel->findByPermissionId($id);
        if($re != null){
            return array("rs"=>"error", "msg"=>self::$MSG_NOT_EMPTY);
        }
        $this->db->where(self::$COL_ID,$id);
        $q = $this->db->delete(self::$DB);
        if($q){
            log_message('info', $this->db->last_query());
            return array('rs'=>'success', 'msg' => null);
        }
        log_message('error',$this->db->last_query().'数据库从permission删除失败');
        return array('rs'=>'error', 'msg' => $this::$MSG_DB_ERROR);
    }


    /**
     * 按id查找
     * @param  int $id
     *
     * @return  返回数据库结果
     **/
    public function find($id){
        $this->db->where(array(self::$COL_ID =>$id));
        $q = $this->db->get(self::$DB);
        if($q->num_rows()>0){
            return $q->result()[0];
        }
        return null;
    }

    /**
     * 修改
     *
     * @param int $id 需要修改的permission的id
     * @param array $update_key 需要修改的数据库的列名
     * @param array $update_value 修改的数据库的值
     * @return array 返回更新的结果，array("rs"=>,, 'msg'=>) ,rs==success/error  msg出错信息或null
     */
    public function update($id,$update_key,$update_value){
        $arr=array_combine($update_key,$update_value);
        $name = null;
        if(array_key_exists(self::$COL_NAME, $arr)){
            $name = $arr[self::$COL_NAME];
        }
        $this->db->where(array(self::$COL_NAME =>$name));
        $q = $this->db->get(self::$DB);
        if($q->num_rows() > 0) {
            //已经存在该逻辑名称
            return array('rs'=>'error', 'msg' => $this::$MSG_ALREADY_EXIST);
        }
        $this->db->where(self::$COL_ID,$id);
        $this->db->update(self::$DB,$arr);
        if($this->db->affected_rows()>0){
            log_message('info', $this->db->last_query());
            return array('rs'=>'success', 'msg'=>null);
        }
        log_message('error',$this->db->last_query().'数据库从permission更新失败');
        return array('rs'=>'error', 'msg' => $this::$MSG_DB_ERROR);
    }
    
}