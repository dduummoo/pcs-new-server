<?php
/**
 * Created by PhpStorm.
 * User: Realdata
 * Date: 2016/8/8
 * Time: 13:06
 */
class Report_Record_Model extends CI_Model
{
    private static $PREFIX_DB='report_data_';//保存的collection的名字的前缀
    private static  $PAGE_SIZE = 20;
    public function __construct()
    {
        parent::__construct();
        $this->load->library("mongo_db");
    }
   
    public function change_page_size($page_size){
        self::$PAGE_SIZE = $page_size;
        return array("data"=>self::$PAGE_SIZE);
    }
    public function get_one_report_record($_id,$report_id){
        $report_record =  $this->mongo_db->where(array("_id"=>new MongoID($_id)))->find_one(self::$PREFIX_DB.$report_id);
        return $report_record;
    }

    /**
     * 填写数据后保存到mongo数据库中
     * 
     * @param int $id  报表的id
     * @param array $unvisibledata 填写的不可见的元素的数据
     * @param array $visibledata 填写的可见元素的数据
     */
    public function add($id,$visibledata,$unvisibledata){
        $data = array_merge($unvisibledata,$visibledata);
        return $this->mongo_db->insert(self::$PREFIX_DB.$id,$data);
    }

    /**
     * @param array $record 一条记录
     * @return array $unvisble 所有不可见元素的键值对
     */
    public function get_unvisble_data($record){
        $unvisble=array();
      $unvisble_key = array('time', 'GPS', 'IP', 'UA','IMEI','uid','from');
        for($i =0;$i<count($unvisble_key);$i++){
            $unvisble = array_merge($unvisble,array($unvisble_key[$i]=>$record[$unvisble_key[$i]]));
        }
        return $unvisble;
    }

    /**
     * 根据用户的Id和报表的id获取对应的报表记录
     */
    public function get_records_by_userid_and_report_id($userid,$report_id){
        $report_records =  $this->mongo_db->where(array("uid"=>$userid))->get(self::$PREFIX_DB.$report_id);
        return $report_records;
    }

    public function get_record_by_mongo_id_and_report_id($mongo_id,$report_id){
        $report_records =  $this->mongo_db->where(array("_id"=>new MongoID($mongo_id)))->get(self::$PREFIX_DB.$report_id);
        return $report_records[0];
    }
//    /**
//     * 根据用户的Id和报表的id获取对应的报表记录
//     */
//    public function get_records_by_userid_and_start_time_and_report_id($userid,$start_time,$report_id,$pagenum){
//        if($start_time !==null){
//               $records = $this->mongo_db->where(array("uid" => $userid))->where_gt("time",$start_time)->get(self::$PREFIX_DB . $report_id);
//        }else{
//                $records = $this->mongo_db->where(array("uid" => $userid))->get(self::$PREFIX_DB.$report_id);
//        }
//        $report_records = array();
//        for($i=$pagenum*self::$PAGE_SIZE;$i<($pagenum+1)*self::$PAGE_SIZE-1;$i++){
//            array_push($report_records,$records[$i]);
//        }
//        return array("report_records"=>$report_records,"page_num"=>$pagenum,"page_size"=>self::$PAGE_SIZE);
//    }
    /**
     * 根据用户的Id和报表的id获取对应的报表记录
     */
    public function get_records_by_userid_and_start_time_and_end_time_and_report_id($userid,$start_time,$end_time,$report_id,$pagenum){
        $q = $this->mongo_db->where(array("uid" => (int)$userid));
        if($start_time !==null){
                $q = $q->where_gt("time", $start_time);
            
        }
        if($end_time !==null){
            $q = $q->where_lt("time", $end_time);
        }
        $records = $q->get(self::$PREFIX_DB . $report_id);
         rsort($records);
        $current_page_records = array();
        for($i =$pagenum*self::$PAGE_SIZE;$i<count($records)&&$i<($pagenum+1)*self::$PAGE_SIZE;$i++){
            array_push($current_page_records,$records[$i]);
        }
        return array("report_records"=>$current_page_records,"page_num"=>$pagenum,"page_size"=>self::$PAGE_SIZE,"total"=>count($records));
    }
    /**
     * 根据用户的Id和报表的id获取该用户最后一次填充表格的时间
     */
    public function get_last_record_time_by_userid_and_report_id($userid,$report_id){
        $report_records =  $this->mongo_db->where(array("uid"=>(int)$userid))->order_by(array('time'=>'DESC'))->get(self::$PREFIX_DB.$report_id);
        if(count($report_records)===0){
            return null;
        }
        return $report_records[0]['time'];
    }

    /**
     * @param string $_id mongo_id
     * @param string $report_id 报表Id
     * @param  array $visible 传递过来的可见元素的键值对
     * @return bool
     */
    public function update_report_data($_id,$report_id,$visible){
        $record = $this->get_one_report_record($_id,$report_id);
        $unvisble = $this->get_unvisble_data($record);
        $new_data = array_merge($unvisble,$visible);
        $flag = $this->mongo_db->where(array("_id"=>new MongoID($_id)))->replace(self::$PREFIX_DB.$report_id,$new_data);
        return $flag;
    }
}