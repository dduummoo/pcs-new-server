<?php
/**
 * Created by PhpStorm.
 * User: Realdata
 * Date: 2016/7/28
 * Time: 16:44
 */
class Report_Grant_Model extends  CI_Model
{
    //映射数据库的表名
    public static $DB = "report_grant";
   
    //映射数据库的列
    public static $COL_ID = 'id';
    public static $COL_REPORT_ID = 'report_id';
    public static $COL_UGROUP_ID = 'ugroup_id';
    public static $COL_CREATE_TIME = 'create_time';
    public static $COL_UPDATE_TIME = 'update_time';
    public static $COL_CREATOR_ID = 'creator_id';
    public static $COL_UPDATOR_ID = 'updator_id';
    public static $COL_FREQUENCY = 'frequency';
    public static $COL_IS_SPECIAL_ENABLE = 'is_special_enable';
    public static $COL_SPECIAL_TIME_START = 'special_time_start';
    public static $COL_SPECIAL_TIME_END = 'special_time_end';
    public static $COL_SPECIAL_FREQUENCY = 'special_frequency';
    public static $COL_IS_ENABLE = 'is_enable';
    public static  $CREATOR_ID_NOT_EXIST='creator id not exist';
    public static  $UPDATOR_ID_NOT_EXIST='updator id not exist';
    public static  $REPORT_ID_NOT_EXIST='report id not exist';
    public static  $UGROUP_ID_NOT_EXIST='ugroup id not exist';
    public static  $SPECIAL_TIME_START_MUST_LARGE_THEN_CURRENT_TIME='special_time_start must large than current time';
    public static  $SPECIAL_TIME_END_MUST_LARGE_THEN_SPECIAL_TIME_START='special_time_end must large than special_time_start';
    public static  $REPORT_GRANT_HAS_EEXIST='this group has allocated this report';
    //表示类型信息，按需要进行扩展
    public static $TYPE_ERROR = "error"; 	// 操作失败
    public static $TYPE_SUCCESS = "success"; 	// 操作失败

    //表示函数返回信息，按需要进行扩展
    public static $MSG_NOT_EXIST = "not_exist"; // 不存在
    public static $MSG_NOT_EMPTY = "not_empty"; // 当前不空
    public static $PAGE_SIZE = 20;
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    public function change_page_size($page_size){
        self::$PAGE_SIZE = $page_size;
        return array("data"=>self::$PAGE_SIZE);
    }
  
    /** 添加一个报表权限
     * @param int $report_id 报表的id
     * @param int $ugroup_id 分组的id
     * @param int $create_time 创建的时间
     * @param int $update_time 最后更新的时间
     * @param int $creator_id 创建人员的id
     * @param int $updator_id 最后更新的人的id
     * @param int $frequency  填写的频率
     * @param int $is_special_enable 敏感时期是否开启
     * @param int $special_time_start 敏感事件开始时间
     * @param int $special_time_end 敏感时间结束时间
     * @param int $special_frequency 敏感期的填写频率
     * @param int $is_enable 是否暂停
     */
    public function add($report_id,$ugroup_id,$creator_id,$updator_id,$frequency,
                                             $special_time_start,$special_time_end,$special_frequency, $is_special_enable=1,$is_enable=1){
//        $result = $this->checkReport_id($report_id);
//        if($result['rs']=='error'){
//            return $result;
//        }
//        $result = $this->checkReportGrantByGroupIdAndReportId($ugroup_id,$report_id);
//        if($result['rs']=='error'){
//            return $result;
//        }
//        $result = $this->checkUgroup_id($ugroup_id);
//        if($result['rs']=='error'){
//            return $result;
//        }
//        $result = $this->checkCreator_id($creator_id);
//        echo $result['rs'];
//        if($result['rs']=='error'){
//            return $result;
//        }
//        $result = $this->checkSpecial_time_start($special_time_start);
//        if($result['rs']=='error'){
//            return $result;
//        }
//        $result = $this->checkSpecial_time_end($special_time_start,$special_time_end);
//        if($result['rs']=='error'){
//            return $result;
//        }
        $report_grant = array(
        self::$COL_REPORT_ID =>$report_id,
        self::$COL_UGROUP_ID=> $ugroup_id,
        self::$COL_CREATE_TIME =>time(),
        self::$COL_UPDATE_TIME =>time(),
        self::$COL_CREATOR_ID =>$creator_id,
        self::$COL_UPDATOR_ID=> $updator_id,
        self::$COL_FREQUENCY =>$frequency,
        self::$COL_IS_SPECIAL_ENABLE=>$is_special_enable,
        self::$COL_SPECIAL_TIME_START =>$special_time_start,
        self::$COL_SPECIAL_TIME_END =>$special_time_end,
        self::$COL_SPECIAL_FREQUENCY=>$special_frequency,
        self::$COL_IS_ENABLE =>$is_enable
        );
         $this->db->insert(self::$DB,$report_grant);
        if($this->db->affected_rows()>0){
            log_message('info',$this->db->last_query());
            return array('rs'=>'success');
        }
       
        return array('rs'=>'error');
    }
    /**
     * 删除一个报表分组
     * @param int $id 报表分组的id
     * @return array array 返回删除的结果，array("rs":"msg") ,msg==success/error
     */
    public function delete($id){
        $this->db->where(self::$COL_ID,$id);
        $q = $this->db->delete(self::$DB);
        if($q){
            log_message('info',$this->db->last_query());
            return array('rs'=>'success');
        }
      
        return array('rs'=>'error');
    }

    /**
     * 查找所有的报表分组
     * @return array array('rs'=>'result','data'=>'datas') result=success/error,datas为查找出的数据
     */
    public function find($report_id,$ugroup_id){
        if($report_id!==0){
            $this->db->where(self::$COL_REPORT_ID, $report_id);
        }
        if($ugroup_id!==0){
            $this->db->where(array(self::$COL_UGROUP_ID=>$ugroup_id));
        }
        $q = $this->db->get(self::$DB);
        if($q->num_rows()>0){
            return array("rs"=>"success","data"=>$q->result());
        }
        return array("rs"=>"error","data"=>null);
//        $q = $this->db->get(self::$DB);
//        return array('rs'=>'success','data'=>$q->result());
    }

    public function find_by_report_id_and_ugroup_id($report_id,$ugroup_id,$pagenum){
        if($report_id!==0){
            $this->db->where(self::$COL_REPORT_ID, $report_id, 'both');
        }
        if($ugroup_id!==0){
            $this->db->where(array(self::$COL_UGROUP_ID=>$ugroup_id));
        }
        $count = $this->db->count_all_results(self::$DB,false);
        $q = $this->db->limit(self::$PAGE_SIZE,$pagenum*self::$PAGE_SIZE)->get();
        return array("rs"=>"success","data"=>$q->result(),"pagenum"=>$pagenum,"total"=>$count,"pagesize"=>self::$PAGE_SIZE);
    }
    /**
     * 查找所有的报表分组
     * @param int $id 报表分组的id
     * @return array array('rs'=>'result','data'=>'data') result=success/error,data为查找出的数据
     */
    public function findId($id){
        $this->db->where(self::$COL_ID,$id);
        $q = $this->db->get(self::$DB);
        if($q->num_rows()>0){
            return array('rs'=>'success','data'=>$q->first_row());
        }
        return array('rs'=>'error','data'=>null);
    }

    /**
     * 修改某个分组的信息,要验证分组是否已经存在（验证report_id和ugroup_id同时相等）
     * 验证report_id和Ugroup_id是否存在
     * @param int $id 需要修改的报表权限的id
     * @param array $update_key 需要修改的报表分组的数据库的列名
     * @param array $update_value 修改的报表分组的数据库的值
     * @return array 返回更新的结果，array("rs":"result",'msg':msg) ,result==success/error,msg错误提示
     */
    public function update($id,$update_key,$update_value){
        $arr=array_combine($update_key,$update_value);
        if($arr == null){
            return array('rs'=>'error');
        }
        if(array_key_exists(self::$COL_REPORT_ID,$arr)){
           $result = $this->checkReport_id($arr[self::$COL_REPORT_ID]);
            if($result['rs']=='error'){
                return array('rs'=>'error');
            }
        }
        if(array_key_exists(self::$COL_UGROUP_ID,$arr)){
            $result = $this->checkUgroup_id($arr[self::$COL_UGROUP_ID]);
            if($result['rs']=='error'){
                return $result;
            }
        }
        if(array_key_exists(self::$COL_UGROUP_ID,$arr)&&array_key_exists(self::$COL_REPORT_ID,$arr)){
            $result = $this->checkReportGrantByGroupIdAndReportId($id,$arr[self::$COL_UGROUP_ID],$arr[self::$COL_REPORT_ID]);
            if($result['rs']=='error'){
                return $result;
            }
        }
        $arr[self::$COL_UPDATE_TIME] = time();
        $this->db->trans_begin();
        $this->db->where(self::$COL_ID,$id);
        $this->db->update(self::$DB,$arr);
        if($this->db->affected_rows()>0){
            log_message('info',$this->db->last_query());
            $this->db->group_by(array(self::$COL_REPORT_ID,self::$COL_UGROUP_ID));
            $this->db->having('count(*)>1');
            $result = $this->db->get(self::$DB);
            if($result->num_rows()==0) {
                $this->db->trans_commit();
                return array('rs' => 'success','msg'=>null);
            }else{
                $this->db->trans_rollback();
                return array('rs'=>'error','msg'=>self::$REPORT_GRANT_HAS_EEXIST);
            }
        }
        return array('rs'=>'error');
    }
    /*验证是否有重复语句
    public function test(){
        $this->db->group_by(array(self::$COL_REPORT_ID,self::$COL_UGROUP_ID));
        $this->db->having('count(*)>1');
        $result = $this->db->get(self::$DB);
    print_r($result->num_rows());
    }*/


    /**核对创建者id是否存在
     * @param int $creator_id 创建者的ID
     * @return array array返回查找创建者的结果，array("rs":result,'msg'=>msg) ,result=='success'/'error',msg:错误信息
     */
    public function checkCreator_id($creator_id){
        $this->load->model('User_Model','user');
        if($this->user->findId($creator_id)['rs']=='error'){
            return array('rs'=>'error','msg'=>self::$CREATOR_ID_NOT_EXIST);
        }
        return array('rs'=>'success');
    }

    /**
     * 核对更新者id是否存在
     *
     * @param int $updator_id 更新者的ID
     * @return array array返回查找更新者的结果，array("rs":result,'msg'=>msg) ,result=='success'/'error',msg:错误信息
     */
    public function checkUpdator_id($updator_id){
        $this->load->model('User_Model','user');
        if($this->user->findId($updator_id)['rs']=='error'){
            return array('rs'=>'error',msg=>self::$UPDATOR_ID_NOT_EXIST);
        }
        return array('rs'=>'success');
    }
    /**核对报表id是否存在
     *
     * @param int $report_id 报表的ID
     * @return array array返回查找报表id的结果，array("rs":result,'msg'=>msg) ,result=='success'/'error',msg:错误信息
     */
    public function checkReport_id($report_id){
        $this->load->model('Report_Model','report');
        if($this->report->findId($report_id)['rs']=='error'){
            return array('rs'=>'error','msg'=>self::$REPORT_ID_NOT_EXIST);
        }
        return array('rs'=>'success');
    }
    /**核对用户分组id是否存在
     *
     * @param int $report_id 报表的ID
     * @return array array返回查找报表id的结果，array("rs":result,'msg'=>msg) ,result=='success'/'error',msg:错误信息
     */
    private function checkUgroup_id($ugroup_id){
        $this->load->model('Group_Model','ugroup');
        $arr = $this->ugroup->find();
        if($this->ugroup->findId($arr,$ugroup_id)['rs']=='eror'){
            return array('rs'=>'error','msg'=>self::$UGROUP_ID_NOT_EXIST);
        }
        return array('rs'=>'success');
    }
    
    /**核对敏感时期开始时间
     *
     * @param int $special_time_start 敏感时间开始时间
     * @return array array返回验证的结果，array("rs":result,'msg'=>msg) ,result=='success'/'error',msg:错误信息
     */
    private function checkSpecial_time_start($special_time_start){
        $current_time = time();
        if($special_time_start<$current_time){
            return array('rs'=>'error','msg'=>self::$SPECIAL_TIME_START_MUST_LARGE_THEN_CURRENT_TIME);
        }
        return array('rs'=>'success','msg'=>null);
    }
    /**核对敏感时间结束时间
     *
     * @param int $special_time_end 敏感时间结束时间
     * @return array array返回验证的结果，array("rs":result,'msg'=>msg) ,result=='success'/'error',msg:错误信息
     */
    private function checkSpecial_time_end($special_time_start,$special_time_end){
        $current_time = time();
        if($special_time_end<=$special_time_start){
            return array('rs'=>'error','msg'=>self::$SPECIAL_TIME_END_MUST_LARGE_THEN_SPECIAL_TIME_START);
        }
        return array('rs'=>'success','msg'=>null);
    }

    /**
     * 根据用户组id和报表id查询报表分组看是否存在，如果存在说明这个报表已经分配过给这个用户组了
     * 查找肯定存在，判断其是否id=id，如果不等说明重复
     * @param int $group_id 用户组id
     * @param int $report_id 报表的id
     * @return array array('rs'=>result,'msg'=>'message')如果存在result=error,msg为提醒的信息，
     */
    private function checkReportGrantByGroupIdAndReportId($id,$group_id,$report_id){
        $this->db->where(array(self::$COL_UGROUP_ID=>$group_id,self::$COL_REPORT_ID=>$report_id));
        $q = $this->db->get(self::$DB);
        if($q->num_rows()>0 &&($q->first_row()->id!=$id)){
            return array('rs'=>'error','msg'=>self::$REPORT_GRANT_HAS_EEXIST);
        }
        return  array('rs'=>'success','msg'=>null);
    }

    /**
     * @param array $group_id 用户所在分组的Id
     * @return array array根据分组的id找到的所有的报表Id的集合
     */
    public function get_report_ids_by_group_id($group_id){
        $this->db->where(self::$COL_IS_ENABLE,1);
        $report_grants = $this->db->where(self::$COL_UGROUP_ID,$group_id)->get(self::$DB)->result();//到这里是获取到了报表还没有处理敏感时间
        $report_ids = array();
        for($i=0;$i<count($report_grants);$i++){
            if(!(time()>=$report_grants[$i]->special_time_start&&time()<=$report_grants[$i]->special_time_end&&$report_grants[$i]->is_special_enable==0)){//在敏感时期并且敏感时期内是关闭的就移除
               array_push($report_ids,$report_grants[$i]->report_id);
            }
        }
        return $report_ids;
    }
}