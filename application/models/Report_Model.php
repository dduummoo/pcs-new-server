<?php
/**
 * Created by PhpStorm.
 * User: Realdata
 * Date: 2016/7/28
 * Time: 12:51
 */
class Report_Model extends  CI_Model
{
    public static $MSG_EXIST_NAME = "name_has_existed"; //报表名已经存在
    public static $MSG_NAME_EXIST_TEST = "name_exist_test"; //报表名中包含test_
    private static $report_db='report';//数据库表名
    private static $report_id='id';//报表id
    private static $mreport_id='_id';//mongo中对应的报表模板的id
    private static $report_create_time='create_time';//创建使劲啊
    private static $report_update_time='update_time';//更新的时间
    private static $report_name='name';//报表的名字
    private static $report_creator_id='creator_id';//创建报表的人的id
    private static $report_updator_id='updator_id';//最后更新报表的人的id
    private static $report_is_enable = "is_enable"; //是否启动这个报表
    private static $report_tmpl='formdesign_tmpl';//预览报表确认后的报表模板（formdesign控件的参数）
    public static $report_parse='formdesign_parse';//formdesign控件的参数
    public static $report_data = 'formdesign_data';//formdesign控件的参数
    public static  $CREATOR_ID_NOT_EXIST='creator id not exist';//创建人不存在
    public static  $UPDATOR_ID_NOT_EXIST='updator id not exist';//更新者的id不存在
    public static $UPDATE_ARRAY_IS_NULL='update array is null';//更新的条件为空
    public static $MISSING_UPDATOR_ID='misssing the updator id';//更新的条件为空
    static  $PAGE_SIZE = 20;
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    public function change_page_size($page_size){
        self::$PAGE_SIZE = $page_size;
        return array("data"=>self::$PAGE_SIZE);
    }
    /**
     * 添加一个报表(报表名不能以test_开头，也不能重名)
     *
     * @param int $_id mongo_db中对应的id
     * @param string $name  报表的名称
     * @param int $creator_id 创建报表人的id
     * @param int $updator_id  最后一次更新人的id
     * @param int $is_enable 0/1 是否暂停这个表单,全局暂停,无论那个分组分配了,都会暂停,0代表false,1代表true
     * @param string $tmpl 预览报表确认后的报表模板
     * @param string $formdesign_tmpl  设计好的报表模板的前端代码
     * @param string $formdesign_parse
     * @param string $formdesign_data
     * @return array 返回添加的结果，array("rs":"msg"，'msg'=>'data') ,msg==success/error
     */

    public function add($_id, $name, $creator_id, $updator_id, $is_enable=1,$formdesign_tmpl=null,$formdesign_parse,$formdesign_data){
       $result = $this->checkName($name,0);
        if($result['rs']=='error'){
            return $result;
        }
        if($creator_id!= 0){
            $result = $this->checkCreator_id($creator_id);
            if($result['rs']=='error'){
                return $result;
            }
        }
        if($updator_id!=0) {
            $result = $this->checkUpdator_id($updator_id);
            if ($result['rs'] == 'error') {
                return $result;
            }
        }

        $report = array(
            self::$mreport_id=>$_id,
            self::$report_create_time=>time(),
            self::$report_update_time=>time(),
            self::$report_name=>$name,
            self::$report_creator_id =>$creator_id,
            self::$report_updator_id=>$updator_id,
            self::$report_is_enable=>$is_enable,
            self::$report_tmpl => $formdesign_tmpl,
            self::$report_parse => $formdesign_parse,
            self::$report_data=> $formdesign_data
        );
        $this->db->insert(self::$report_db,$report);
        if($this->db->affected_rows()>0){
            log_message('info',$this->db->last_query());
            $report = array(
                self::$mreport_id=>$_id,
                self::$report_create_time=>time(),
                self::$report_update_time=>time(),
                self::$report_name=>'test_'.$name,
                self::$report_creator_id=>$creator_id,
                self::$report_updator_id=>$updator_id,
                self::$report_is_enable=>$is_enable,
                self::$report_tmpl => $formdesign_tmpl,
                self::$report_parse => $formdesign_parse,
                self::$report_data => $formdesign_data
            );
            $this->db->insert(self::$report_db,$report);
            log_message('info',$this->db->last_query());
            return array('rs'=>'success');
        }
        return array('rs'=>'error');
    }

    /**
     * 验证报表的名是否合理（是否包含test_和验证是否存在）
     *
     * @param string $name 报表的名字
     * @return array array("rs":"result"，'msg'=>'data') ,result==success/error,msg=MSG_NAME_EXIST_TEST表示报表重名
     *                  $MSG_NAME_EXIST_TEST表示报表名包含test_是不允许的
     */
    public function checkName($name,$id){
        if(stristr($name,"test_")!=null){
            return array('rs'=>'error','msg'=>self::$MSG_NAME_EXIST_TEST);
        }
        $this->db->where(self::$report_name,$name);
        $q = $this->db->where_not_in("id",array($id))-> get(self::$report_db);
        if($q->num_rows()>0){
            log_message('info',$this->db->last_query());
            return array('rs'=>'error','msg'=>self::$MSG_EXIST_NAME);
        }
    return array('rs'=>'success','msg'=>null);
}

    /**
    * 查找所有的报表（不包括以test_开头的）
     *
    * @return array array('rs'=>'result','data'=>'data') result=success/error,data为查找出的数据
    */
    public function find(){
        $this->db->not_like(self::$report_name,'test_','left');
        $q = $this->db->order_by(self::$report_create_time,"DESC")->get(self::$report_db);
        return array('rs'=>'success','data'=>$q->result());
    }
    public function find_by_name($name,$pagenum){
        if($name !=null) {
            $total = $this->db->not_like(self::$report_name,'test_','left')->like(self::$report_name, $name)->count_all_results(self::$report_db);
            $reports = $this->db->not_like(self::$report_name,'test_','left')->like(self::$report_name, $name)->order_by(self::$report_create_time,"DESC")->limit(self::$PAGE_SIZE,$pagenum*self::$PAGE_SIZE)->get(self::$report_db)->result();
        }else{
            $total = $this->db->not_like(self::$report_name,'test_','left')->count_all_results(self::$report_db);
            $reports = $this->db->not_like(self::$report_name,'test_','left')->like(self::$report_name, $name)->order_by(self::$report_create_time,"DESC")->limit(self::$PAGE_SIZE,$pagenum*self::$PAGE_SIZE)->get(self::$report_db)->result();
        }
//        print_r(array('total'=>$total,"reports"=>$reports,'page_size'=>self::$PAGE_SIZE,'page_num'=>$pagenum));
        return array('total'=>$total,"reports"=>$reports,'page_size'=>self::$PAGE_SIZE,'page_num'=>$pagenum+1);
    }
    /**
     * 查找所有符合条件的报表（不包括以test_开头的）
     *
     * @return array array('rs'=>'result','data'=>'data') result=success/error,data为查找出的数据
     */
    public function find_by_name_and_userid($name=null,$userid=null){
        if($name!=null){
            $this->db->like(self::$report_name,$name);
        }
            $this->db->not_like(self::$report_name, 'test_', 'left');
        if($userid !==null){
            $this->db->where(self::$report_creator_id,$userid);
        }
        $q = $this->db->order_by(self::$report_create_time,"DESC")->get(self::$report_db);
        return array('rs'=>'success','data'=>$q->result());
    }

    /**
     * 查找所有的报表（以test_开头的）
     *
     * @return array array('rs'=>'result','data'=>'data') result=success/error,data为查找出的数据
     */
    public function find_test(){
        $this->db->like(self::$report_name,'test_','left');
        $q = $this->db->get(self::$report_db);
        return array('rs'=>'success','data'=>$q->result());
    }

    /**
     * 删除一个报表
     * 
     * @param int $report_id 报表的id
     * @return array 返回删除的结果，array("rs":"msg") ,msg==success/error
     */
    public function delete($report_id){
        $this->db->where(self::$report_id,$report_id);
        $q = $this->db->delete(self::$report_db);
        if($q){
            log_message('info',$this->db->last_query());
            return array('rs'=>'success');
        }
        return array('rs'=>'error');
    }
    /**
     * 修改某报表的信息
     * 
     * @param int $id 需要修改的报表的id
     * @param array $update_key 需要修改的报表的数据库的列名集合
     * @param array $update_value 修改的报表的数据库的值集合
     * @return array array("rs":"result",'msg'=>message)返回更新的结果 ,result==success/error,$UPDATE_ARRAY_IS_NULL更新的条件数组为空
     *                  $MISSING_UPDATOR_ID更新的数组中缺少updatorid键值对
     */
    public function update($id,$update_key,$update_value){

        $arr=array_combine($update_key,$update_value);
        if($arr == null){
            return array('rs'=>'error','msg'=>self::$UPDATE_ARRAY_IS_NULL);
        }
        if(array_key_exists ( self::$report_name, $arr)){
            $result = $this->checkName($arr[self::$report_name],$id);
            if($result['rs']=='error'){
                return $result;
            }
        }
        if(!array_key_exists ( self::$report_updator_id, $arr)){
            return array('rs'=>'error','msg'=>self::$MISSING_UPDATOR_ID);
        }else{
            $result = $this->checkUpdator_id($arr[self::$report_updator_id]);
            if($result['rs']=='error'){
                return $result;
            }
        }
        if(array_key_exists ( self::$report_creator_id, $arr)){
            $result = $this->checkCreator_id($arr[self::$report_creator_id]);
            if($result['rs']=='error'){
                return $result;
            }
        }


        $arr[self::$report_update_time] = time();
        $this->db->where(self::$report_id,$id);
        $this->db->update(self::$report_db,$arr);
        if($this->db->affected_rows()>0){
            log_message('info',$this->db->last_query());
            return array('rs'=>'success');
        }
        return array('rs'=>'error','msg'=>null);
    }


    /**核对创建者id是否存在
     * @param int  $creator_id 创建者的ID
     * @return int array返回查找创建者的结果，array("rs":result,'msg'=>msg) ,result=='success'/'error',msg:错误信息
     */
    private function checkCreator_id($creator_id){
        $this->load->model('User_Model','user');
       if($this->user->findId($creator_id)['rs']=='error'){
            return array('rs'=>'error','msg'=>self::$CREATOR_ID_NOT_EXIST);
       }
        return array('rs'=>'success','msg'=>null);
    }

    /**
     * 核对更新者id是否存在
     * 
     * @param int $updator_id 更新者的ID
     * @return int array返回查找更新者的结果，array("rs":result,'msg'=>msg) ,result=='success'/'error',msg:错误信息
     *               $UPDATOR_ID_NOT_EXIST更新者id在数据库中不存在
     */
    private function checkUpdator_id($updator_id){
        if($updator_id === 0){
            return array('rs'=>'success','msg'=>null);
        }
        $this->load->model('User_Model','user');
        if($this->user->findId($updator_id)['rs']=='error'){
            return array('rs'=>'error','msg'=>self::$UPDATOR_ID_NOT_EXIST);
        }
        return array('rs'=>'success','msg'=>null);
    }

    /** 
     * 根据报表的id查找报表是否存在,如果存在返回此对象
     * 
     * @paramc int $report_id 报表id
     * @return int array 返回查找的结果 array("rs":result,'data'=>data) ,result=='success'/'error'
     */
    public function findId($report_id){
        $this->db->where(self::$report_id,$report_id);
        $q = $this->db->get(self::$report_db);
        if($q->num_rows()>0){
            return array('rs'=>'success','data'=>$q->first_row());
        }
        return array('rs'=>'error','data'=>null);
    }

    /**
     * @param int $mreport_id mongo中对应的报表模板的id
    * @return array   array('rs'=>'success','id'=>$q->first_row()->id); rs表示搜索结果，‘id’为查找到的报表的id
     */
    public function get_id_by_report_template_id($mreport_id){
        $this->db->where(self::$mreport_id,$mreport_id);
        $q = $this->db->get(self::$report_db);
        if($q->num_rows()>0){
            return array('rs'=>'success','id'=>$q->first_row()->id);
        }
        return array('rs'=>'error','id'=>null);
    }
    /**
     * 确认添加参数为（$report_id,$test_report_id），如果是取消操作参数为（$test_report_id，$report_id）
     * 
     * @param int $report_id 报表的id
     * @param int $test_report_id 用于预览的报表的id
     */
    public function confirm($report_id,$test_report_id){
        $test_report = $this->findId($test_report_id)['data'];
        $update_key=array(self::$report_create_time,
                           self::$report_update_time,
                           self::$report_creator_id ,
                           self::$report_updator_id,
                           self::$report_is_enable,
                           self::$report_tmpl,
                           self::$report_parse,
                           self::$report_data
        );

        $update_value=array($test_report->create_time,
                             $test_report->update_time,
                             $test_report->creator_id,
                             $test_report->updator_id,
                             $test_report->is_enable,
                             $test_report->formdesign_tmpl,
                             $test_report->formdesign_parse,
                             $test_report->formdesign_data,
            );
        return $this->update($report_id,$update_key,$update_value);
    }

    /**
     * 根据报表的id集合获取到对应的没有呗暂停的报表集合
     * @param array $report_ids 报表的Id的集合
     * @return array 返回对应id集合内没有呗暂停的报表
     *
     */
    public function get_reports_by_report_ids($report_ids,$pageNum,$name=null){
        if(count($report_ids) === 0){
            return array('reports'=>array(),"page_size"=>self::$PAGE_SIZE,'total'=>0,"page_num"=>$pageNum+1);
        }
        if($name != null) {
            $total = $this->db->not_like(self::$report_name, 'test_', 'left')->like(self::$report_name,$name)->where_in(self::$report_id,$report_ids)->where(self::$report_is_enable,1)->count_all_results(self::$report_db);
            $current_page_report = $this->db->not_like(self::$report_name, 'test_', 'left')->like(self::$report_name, $name)->order_by(self::$report_create_time,"DESC")->where_in(self::$report_id, $report_ids)->where(self::$report_is_enable, 1)->limit( self::$PAGE_SIZE, $pageNum *self::$PAGE_SIZE)->get(self::$report_db)->result();
        }else{
            $total = $this->db->not_like(self::$report_name, 'test_', 'left')->where_in(self::$report_id,$report_ids)->where(self::$report_is_enable,1)->count_all_results(self::$report_db);
            $current_page_report = $this->db->not_like(self::$report_name, 'test_', 'left')->where_in(self::$report_id, $report_ids)->order_by(self::$report_create_time,"DESC")->where(self::$report_is_enable, 1)->limit(self::$PAGE_SIZE, $pageNum *self::$PAGE_SIZE)->get(self::$report_db)->result();
        }
        return array('reports'=>$current_page_report,"page_size"=>self::$PAGE_SIZE,'total'=>$total,"page_num"=>$pageNum+1);
    }
    public function get_all_reports_by_report_ids($report_ids){
        if(count($report_ids) === 0){
            return array('reports'=>array(),"page_size"=>self::$PAGE_SIZE,'total'=>0,"page_num"=>0);
        }
        $total = $this->db->where_in(self::$report_id,$report_ids)->count_all_results(self::$report_db);
        $reports = $this->db->where_in(self::$report_id, $report_ids)->order_by(self::$report_create_time,"DESC")->get(self::$report_db)->result();
        return array('reports'=>$reports,'total'=>$total,"page_size"=>self::$PAGE_SIZE);
    }
    
}