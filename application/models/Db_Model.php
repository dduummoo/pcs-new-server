<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/10/20
 * Time: 13:07
 */
class Db_Model extends CI_Model
{
    private $Separate_str = ";$$";//sql语句用什么隔开
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function back_up_db()
    {
        header("Content-type:text/html;charset=utf8");
        echo "<html>开始中，请耐心等待...<br/></html>";

//配置信息
        $cfg_dbhost = $this->db->hostname;
        $cfg_dbname = $this->db->database;
        $cfg_dbuser = $this->db->username;
        $cfg_dbpwd = $this->db->password;
        $cfg_db_language = "utf8";
        $to_file_name = $_SERVER['DOCUMENT_ROOT']."/" . date("Y-m-d") . "pcs.sql";
// END 配置
//链接数据库
        $link = mysqli_connect($cfg_dbhost, $cfg_dbuser, $cfg_dbpwd);
        mysqli_select_db($link, $cfg_dbname);
//选择编码i
        mysqli_query($link, "set names utf8");
        $sql = "SHOW TABLES FROM $cfg_dbname";
///数据库中有哪些表
        $tables = mysqli_query($link, $sql);
//将这些表记录到一个数组
        $tabList = array();
        while ($row = mysqli_fetch_row($tables)) {
            $tabList[] = $row[0];
        }

        echo "</html>运行中，请耐心等待...<br/></html>";
        $info = "-- ----------------------------\r\n";
        $info .= "-- 备份日期：" . date("Y-m-d H:i:s", time()) . "\r\n";
        $info .= "-- 备份派出所Mysql数据库111\r\n";
        $info .= "-- ----------------------------\r\n\r\n";
        file_put_contents($to_file_name, $info, FILE_APPEND);
//将每个表的表结构导出到文件
        foreach ($tabList as $val) {
            $sql = "show create table " . $val;
            $res = mysqli_query($link, $sql);
            $row = mysqli_fetch_array($res);
            $info = "-- ----------------------------\r\n";
            $info .= "-- Table structure for `" . $val . "`\r\n";
            $info .= "-- ----------------------------\r\n";
            $info .= "DROP TABLE IF EXISTS `" . $val . "`;$$\r\n";
            $sqlStr = $info . $row[1] . ";$$\r\n\r\n";
            $sql = "select * from " . $val;
            $r = mysqli_query($link, $sql);
            $values="";
            while ($data = mysqli_fetch_assoc($r))
            {
                $keys = array_keys($data);
                $keys = array_map('addslashes', $keys);
                $keys = join('`,`', $keys);
                $keys = "`" . $keys . "`";
                $vals = array_values($data);
                $vals = array_map('addslashes', $vals);
                $vals = join("','", $vals);
                $vals = "'" . $vals . "'";
                $values .="(".$vals."),\r\n";
            }
            //去掉value最后面的一个,
            if($values != null) {
                $values = substr($values, 0, strlen($values) - 3);//应该是收到\r\n影响，去掉最后一位要减3
                $sql = "insert into `$val`(" . $keys . ") values \r\n" . $values . ";$$\r\n";
                $sqlStr .= $sql;
            }
            //追加到文件
            file_put_contents($to_file_name, $sqlStr, FILE_APPEND);
            //释放资源
//            mysqli_free_result($res);
        }
        echo "<html>结束</html>";
    }

    function restore_db(){
        $file_name = "C:/Users/Administrator/Desktop/2016-10-20pcs.sql"; //要导入的SQL文件名
        $dbhost = "localhost"; //数据库主机名
        $dbuser = "root"; //数据库用户名
        $dbpass = "root"; //数据库密码
        $dbname = "pcs"; //数据库名
        header("Content-type:text/html;charset=utf8");
        set_time_limit(0); //设置超时时间为0，表示一直执行。当php在safe mode模式下无效，此时可能会导致导入超时，此时需要分段导入
        $fp = @fopen($file_name, "r") or die("不能打开SQL文件 $file_name");//打开文件
        $link = mysqli_connect($dbhost, $dbuser, $dbpass) or die("不能连接数据库 $dbhost");//连接数据库
        mysqli_query($link, "set names utf8");
        mysqli_select_db($link,$dbname) or die ("不能打开数据库 $dbname");//打开数据库

        echo "<p>正在清空数据库,请稍等....<br>";
        $result = mysqli_query($link,"SHOW tables");
        while ($currow = mysqli_fetch_array($result)) {
            mysqli_query($link,"drop TABLE IF EXISTS $currow[0]");
            echo "清空数据表【" . $currow[0] . "】成功！<br>";
        }
        echo "<br>恭喜你清理MYSQL成功<br>";

        echo "正在执行导入数据库操作<br>";
// 导入数据库的MySQL命令
        $data=@file_get_contents($file_name);
        $data=explode(";$$",trim($data));
        if(!empty($data)){
            $i = 0;
            foreach($data as $k=>$v){
                if($v == null){
                    continue;
                }
//			$i++;
//			if($i>10){
//				return;
//			}
                if(!mysqli_query($link,$v)){
                    echo "还原出错";
                }
            }
        }
        echo "数据还原成功";
//	var_dump(exec("mysqli -u$dbuser -p$dbpass $dbname< " . $file_name));
//	echo "<br>导入完成！";
        mysqli_close($link);
    }
}