<?php

/**
 * Created by PhpStorm.
 * User: cyy
 * Date: 2016/7/20
 * Time: 14:14
 * 主要负责对用户（普通用户）的基本功能进行操作
 */
class User_Model extends CI_Model
{
    static  $PAGE_SIZE = 20;
    private static $user_id='id';
    private static $user_name='username';
    private  static $user_password='password';
    private static $user_groupid = 'group_id';
    private static $user_db= 'user';
    private static $PSW_BETWEEN_SIX_AND_TWENTY='password must between 6 and 20';
    private  static $PSW_CANNOT_CONTAIN_ILLEGAL_CHARACTER='password can not contain illegal character';
    private  static $NAME_HAS_EXISTED='name has esisted';
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    /**
     * 一个普通用户进行登录
     * @param string $username 用户名
     * @param string $password 经过hash的用户的密码，hash由前端负责hash，这里只管查询
     * @param  string $group_id 用户分组的id
     * @return array 返回数据库中命中的那一列的数组形式array("rs":"msg","data":obj),msg==success/error
     */

    public function change_page_size($page_size){
        self::$PAGE_SIZE = $page_size;
        return array("data"=>self::$PAGE_SIZE);
    }
    public function login($username, $password,$group_id){
        $this->db->where(array(self::$user_name=>$username));
        $this->db->where(array(self::$user_password=>$password));
        $this->db->where(array(self::$user_groupid=>$group_id));
        $q = $this->db->get(self::$user_db);
        if($q->num_rows() > 0) {
            return array("rs" => "success","data"=>$q->first_row(),"count"=>$q->num_rows());
        }
        return array("rs" => "error","data"=>null);
    }
  
    /**
     * 增加一个普通用户
     * 
     * 同一分组下不允许有重名，不同分组允许有重名
     * 
     * @param string $username 用户名
     * @param string $password 经过hash的用户的密码，hash由前端负责hash，这里只管插入
     * @param int $group_id 用户组，默认是0，表示无用户组
     * @return array array("rs":"msg")  返回插入的结果 ,msg==success/error
     */
    public function add($username, $password, $group_id = 0){
        $result = $this->checkName($username,$group_id);
        if($result['rs']=='error'){
            return $result;
        }
        $user = array(
            self::$user_name=>$username,
            self::$user_password=>$password,
            self::$user_groupid=>$group_id
        );
        $this->db->insert(self::$user_db,$user);
        if($this->db->affected_rows()>0){
          log_message('info',$this->db->last_query());
          return array('rs'=>'success');
        }
        return array('rs'=>'error');
    }


    /**
     * 删除一个普通用户
     * @param int $user_id 用户的id
     * @return array 返回删除的结果，array("rs":"msg") ,msg==success/error
     */
    public function delete($user_id){
        $this->db->where(self::$user_id,$user_id);
        $q = $this->db->delete(self::$user_db);
        if($q){
            return array('rs'=>'success');
        }
//        log_message('info',$this->db->last_query().'删除用户出错');
        return array('rs'=>'error');
    }

    /**
     * 查找一个普通用户，如果两个条件都不为空，则同时生效
     *
     * @param string $username 如果不为空，则LIKE搜索用户名
     * @param int $group_id 如果不为0，则搜索所在分组
     * @param int $start 从第几页开始返回，默认是第0页
     * @return array 返回查找的结果，array("rs":"msg",data":array,'total':int,'page_size':int) ,msg==success/error
     */
    public function find($username,$group_id,$start=0){
        if($group_id!=0){
            $this->load->model('Group_Model','group_model');
            $groups = $this->group_model->find_self_and_children_by_id($group_id);
            $this->db->where_in(self::$user_groupid,$groups);
        }
        if($username!=null){
            $this->db->like(self::$user_name, $username, 'both');
        }
        $total = $this->db->count_all_results(self::$user_db,FALSE);
        $this->db->limit(self::$PAGE_SIZE,$start*self::$PAGE_SIZE);
        $q = $this->db->get();
        if($q->num_rows()>0){
            return array("rs"=>"success","data"=>$q->result(),'total'=>$total,'page_size'=>self::$PAGE_SIZE);
        }
        return array("rs"=>"error","data"=>null,'total'=>$total,'page_size'=>self::$PAGE_SIZE);
    }
    //所有用户不分页
    public function find_users($username,$group_id){
        if($group_id!=0){
            $this->db->where(self::$user_groupid,$group_id);
        }
        if($username!=null){
            $this->db->like(self::$user_name, $username, 'both');
        }
        $total = $this->db->count_all_results(self::$user_db,FALSE);
        $q = $this->db->get();
        if($q->num_rows()>0){
            return array("rs"=>"success","data"=>$q->result(),'total'=>$total,'page_size'=>self::$PAGE_SIZE);
        }
        return array("rs"=>"error","data"=>null);
    }
//获取所有用户
    public function find_all(){
        $q = $this->db->get(self::$user_db);
        if($q->num_rows()>0){
            return array("rs"=>"success","data"=>$q->result());
        }
        return array("rs"=>"error","data"=>null);
    }
    public function update_pwd($user_id, $update_key, $update_value){
        $arr=array_combine($update_key,$update_value);
        $this->db->where(self::$user_id,$user_id); //uid 数据库中自增id ，$id 控制器中传入id
        $this->db->update(self::$user_db,$arr);//表名字 传入数组
        if($this->db->affected_rows()>0){
            log_message('info', $this->db->last_query());
            return array('rs'=>'success');
        }
        return array('rs'=>'error',"msg"=>"修改数据库失败");
    }
    /**
     * 修改某个用户的信息，根据键值对数组的形式
     * @param int $user_id 需要修改的用户的id
     * @param array $update_key 需要修改的用户的数据库的列名
     * @param array $update_value 修改的用户的数据库的值
     * @return array 返回更新的结果，array("rs":"msg") ,msg==success/error
     */
    public function update($user_id,$update_key,$update_value){
        $arr=array_combine($update_key,$update_value);
        $flag = $this->check_repeat($user_id, $arr['username'], $arr['group_id']);
        if(!$flag){
            return array('rs'=>'error',"msg"=>"该组已经有过此名字用户");
        }
        $this->db->where(self::$user_id,$user_id); //uid 数据库中自增id ，$id 控制器中传入id
        $this->db->update(self::$user_db,$arr);//表名字 传入数组
        if($this->db->affected_rows()>0){
            log_message('info', $this->db->last_query());
            return array('rs'=>'success');
        }
        return array('rs'=>'error',"msg"=>"原因可能是未修改任何数据");
    }

    private function check_repeat($user_id,$user_name,$group_id){
       $users =  $this->db->where_not_in(self::$user_id,array($user_id))->where(self::$user_name,$user_name)->where(self::$user_groupid,$group_id)->get(self::$user_db)->result();
        if(count($users) > 0){
            return false;
        }
        return true;
    }

    /**查找对应id的用户是否存在
     * @param $id  需要查找的id
     * @return array 返回查找的结果,array("rs":"msg") ,msg==success/error
     */
    public function findId($id){
        $this->db->where(self::$user_id,$id); //uid 数据库中自增id ，$id 控制器中传入id
        $q = $this->db->get(self::$user_db);
        if($q->num_rows()>0){
            log_message('info', $this->db->last_query());
            return array('rs'=>'success','data'=>$q->first_row());
        }
        return array('rs'=>'error','data'=>null);
    }

    /**
     * @param string $name 用户名
     * @return array array 搜索用户名是否存在， return array('rs'=>'error')/ return array('rs'=>'success');
     */
    private function checkName($name,$group_id){
        if($group_id == 0){
            return array('rs'=>'error','msg'=>"group is null");
        }
        $this->db->where(array(self::$user_name=>$name));
        $this->db->where(array(self::$user_groupid=>$group_id));
        $q = $this->db->get(self::$user_db);
        if($q->num_rows()>0){
            return array('rs'=>'error','msg'=>self::$NAME_HAS_EXISTED);
        }
        return array('rs'=>'success','msg'=>null);
    }

    /**
     * 根据用户id获取同组人员的id的集合,一个人可以看到本组人和管理员创建的表
     *
     * @param int $user_id 用户所在的分组id
     * @return array array|null 返回的是用户所在分组所有人员的id
     */
    public function get_users_id_by_user_id($user_id){
        $result = $this->findId($user_id);
        if($result['rs']=='error'){
            return null;
        }
        $group_id = $result['data']->group_id;
       $result =  $this->find(null,$group_id);
        if($result['rs']=='error'){
            return null;
        }
        $users = array();
        foreach ($result['data'] as $user)
        {
          array_push($users,$user->id);
        }
        return $users;
    }

    /**
     * 获取所有本组人的id的集合
     *
     *  @param int $user_id 用户的id
     * @return array array所有本组和子分组的人员的Id
     */
    public function get_group_users_by_userid($userid){
        $r = $this->findId($userid);
        if($r['rs']=='error'){
            return null;
        }
        $group_id = $r['data']->group_id;
        $users = $this->find_users(null,$group_id)['data'];

        $users_id = array();
        for($i = 0;$i<count($users);$i++){
            array_push($users_id,$users[$i]->id);
        }
        return $users_id;
    }
    public function get_self_group_users_by_userid($userid){
        $r = $this->findId($userid);
        if($r['rs']=='error'){
            return null;
        }
        $group_id = $r['data']->group_id;
        $users = $this->find_users(null,$group_id)['data'];

        $users_id = array();
        for($i = 0;$i<count($users);$i++){
            array_push($users_id,$users[$i]->id);
        }
        return $users_id;
    }
}