<?php
/**
 * Created by PhpStorm.
 * User: bb
 * Date: 2016/8/15
 * Time: 11:10
 */
class Mobile_Model extends CI_Model
{
    //映射数据库的表名
    public static $DB = "mobile_token";

    //映射数据库的列
    public static $COL_TOKEN = 'token';
    public static $COL_USER_ID = 'user_ID';
    public static $COL_EXPIRED_TIME = 'expired_time';


    //表示类型信息，按需要进行扩展
    public static $TYPE_ERROR = "error"; 	// 操作失败
    public static $TYPE_SUCCESS = "success"; 	// 操作失败

    //表示函数返回信息，按需要进行扩展
    public static $MSG_NOT_EXIST = "not_exist"; // 不存在
    public static $MSG_NOT_EMPTY = "not_empty"; // 当前不空
    public static $MSG_DB_ERROR = "db_error"; // 数据库错误

    /**
     * Mobile_Model constructor.
     */
    function __construct(){
        parent::__construct();
        $this->load->database();
    }

    /**
     * 根据uid和计算出的过期时间，生成token, 添加一条记录到 表中.
     * @param int $user_ID 用户的id
     * @return array  array('rs'=>'success', msg=>null, token=>生成的token ) 或者 array('rs'=>'error', msg=> 出错原因, token=>null);
     */
    public function add($user_ID){
        if($this->find_by_uid($user_ID) != null){
           $this->delete_by_uid($user_ID);
        }
        //计算过期日期
        $peroid = config_item('expired_day');
        $str = '+'.$peroid." day";
        $expired_time = strtotime(date('Y-m-d H:i:s',strtotime($str)));
        $token = md5($expired_time.$user_ID);

        $data = array(
            self::$COL_TOKEN=> $token,
            self::$COL_USER_ID=>$user_ID,
            self::$COL_EXPIRED_TIME=>$expired_time
        );

        $this->db->insert(self::$DB,$data);
        if($this->db->affected_rows()>0){
            log_message('info',$this->db->last_query());
            return array('rs'=>'success', 'msg'=>null, 'token'=>$token);
        }

        return array('rs'=>'error', "msg" => self::$MSG_DB_ERROR, 'token'=> null);
    }

    /**
     * 根据传入的 token 删除记录
     * @param  string $token
     * @return array array('rs'=>'success'); 或者 array('rs'=>'error');
     */
    public function delete($token){
        $this->db->where(self::$COL_TOKEN,$token);
        $q = $this->db->delete(self::$DB);
        if($q){
            log_message('info', $this->db->last_query());
            return array('rs'=>'success');
        }
        return array('rs'=>'error');
    }

    /**
     * 根据传入的 uid 删除记录
     * @param  string $token
     * @return array array('rs'=>'success'); 或者 array('rs'=>'error');
     */
    public function delete_by_uid($uid){
        $this->db->where(self::$COL_USER_ID,$uid);
        $q = $this->db->delete(self::$DB);
        if($q){
            log_message('info', $this->db->last_query());
            return array('rs'=>'success');
        }
        return array('rs'=>'error');
    }

    /**
     * 根据 uid 查找一条数据的记录， 返回 token 过期时间等
     * @param  int $uid
     * @return 查询到的记录， 如果没有查询到就返回null
     *         Array ( [0] => stdClass Object ( [token] => 5f9f714ed2a85b3119d3a365b8c6dc04 [user_ID] => 1 [expired_time] => 1473839593 ) )
     *
     */
    public function find_by_uid($uid){
        $this->db->where(self::$COL_USER_ID,$uid);
        $q = $this->db->get(self::$DB);
        if($q->num_rows()>0){
            return $q->result();
        }
        return null;
    }

    /**
     * 根据 token 查找一条数据的记录
     * @param string $token
     * @return  查询到的记录， 如果没有查询到就返回null
     *            Array ( [0] => stdClass Object ( [token] => 5f9f714ed2a85b3119d3a365b8c6dc04 [user_ID] => 1 [expired_time] => 1473839593 ) )
     */
    public function find_by_token($token){
        $this->db->where(self::$COL_TOKEN,$token);
        $q = $this->db->get(self::$DB);
        if($q->num_rows()>0){
            return $q->result();
        }
        return null;
    }

}