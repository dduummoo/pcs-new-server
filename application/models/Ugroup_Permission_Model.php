<?php
/**
 * Created by PhpStorm.
 * User: BB
 * Date: 2016/8/2
 * Time: 23:18
 */

class Ugroup_Permission_Model extends CI_Model
{
    //映射数据库的表名
    public static $DB = "ugroup_permission";
    public static $COL_NAME = 'name';
    //映射数据库的列
    public static $COL_ID = 'id';
    public static $COL_UGROUP_ID = 'ugroup_id';
    public static $COL_PERMISSION_ID = 'permission_id';


    //表示类型信息，按需要进行扩展
    public static $TYPE_ERROR = "error"; 	// 操作失败
    public static $TYPE_SUCCESS = "success"; 	// 操作失败

    //表示函数返回信息，按需要进行扩展
    public static $MSG_NOT_EXIST = "not_exist"; // 不存在
    public static $MSG_NOT_EMPTY = "not_empty"; // 当前不空
    public static $MSG_DB_ERROR= "db_error"; // 数据库错误
    public static $MSG_ALREADY_EXIST = "already_exist"; // 已经存在

    /**
     * Ugroup_Permission_Model constructor.
     */
    function __construct(){
        parent::__construct();
        $this->load->database();
    }

    /**
     * 添加一条记录 到 group_permission表 先检查是否重复
     * @param int $ugroup_id
     * @param  int $permission_id
     * @return array array  array("rs" => , "msg"=>)成功 'rs'=>'success', msg => null; 失败返回 array('rs'=>'error', msg=> 失败原因);
     */
    public function add($ugroup_id,$permission_id){
        $this->db->where(array(self::$COL_UGROUP_ID =>$ugroup_id, self::$COL_UGROUP_ID=>$permission_id));
        $q = $this->db->get(self::$DB);
        if($q->num_rows() > 0) {
            //已经存在
            return array('rs'=>'error', 'msg' => $this::$MSG_ALREADY_EXIST);
        }
        $data = array(
            self::$COL_UGROUP_ID=>$ugroup_id,
            self::$COL_PERMISSION_ID=>$permission_id
        );
        $this->db->insert(self::$DB,$data);
        if($this->db->affected_rows()>0){
            log_message('info',$this->db->last_query());
            return array('rs'=>'success','msg' => null);
        }

        return array('rs'=>'error');
    }

    /**
     * 按照id 删除一条记录
     * @param int $id
     * @return array array("rs" =>) rs 为 error 或 success
     */
    public function delete($id){
        $this->db->where(self::$COL_ID,$id);
        $q = $this->db->delete(self::$DB);
        if($q){
            log_message('info', $this->db->last_query());
            return array('rs'=>'success');
        }

        log_message('error',$this->db->last_query().'数据库ugroup_permission删除失败');
        return array('rs'=>'error');
    }

    /**
     * 根据id查找一条记录
     * @param int $id
     * @return 返回数据库记录结果
     */
    public function find($id){
        $this->db->where(array(self::$COL_ID =>$id));
        $q = $this->db->get(self::$DB);
        if($q->num_rows()>0){
            return $q->result();
        }
        return null;
    }

    /**
     * 返回permission id 为pid 的查询结果
     * @param int $id permission id
     * @return 数据库结果集
     */
    public function  findByPermissionId($pid){
        $this->db->where(self::$COL_PERMISSION_ID,$pid);
        $q = $this->db->get(self::$DB);
        if($q->num_rows()>0){
            return $q->result();
        }
        return null;
    }

    /**
     *  获取一个分组id的权限的数组形式,返回的是 permission表一行一行的数据
     * @param  int $ugroup_id
     * @return array 形式为 array(id=> permission表的相应的记录) 其中 id为ugroup_permission表的id， 例如
                         Array ( [1] => stdClass Object ( [id] => 1 [name] => report [txt] => 需要进行日常打开报告的权限 )
     *                          [3] => stdClass Object ( [id] => 2 [name] => montor [txt] => 领导层，需要日常进行监管的 ) )
     */
    public function get_ugroup_permission($ugroup_id){
        $this->db->where(self::$COL_UGROUP_ID,$ugroup_id);
        $q = $this->db->get(self::$DB);

        if($q->num_rows()>0){
            $key_pid = self::$COL_PERMISSION_ID;
            $arrReturn = array();
            $this->load->model("permission_Model","pModel");
            $pModel = $this->pModel;
            foreach($q->result() as $obj){
                $pid = $obj->$key_pid;
                $data = $pModel->find($pid);
                if($data != null){
                    $id = self::$COL_ID;
                    $arrReturn[$obj->$id] = $data;
                }
            }
            return $arrReturn;
        }
        return array();
    }

    /**
     * 检测一个分组id 和权限，其实是调用上面的那个函数，进行了一下封装
     * @param  int $ugroup_id
     * @param  string $permission_name
     * @return true or false
     */
    public function check_permission($ugroup_id,$permission_name){
        $result  = $this->get_ugroup_permission($ugroup_id);
        $key_pname = self::$COL_NAME;
        foreach ($result as $obj){
           if($obj->$key_pname == $permission_name){
               return true;
           }
        }
        return false;
    }

    /**
     * 修改
     *
     * @param int $id 需要修改的ugroup_permission的id
     * @param array $update_key 需要修改的数据库的列名
     * @param array $update_value 修改的数据库的值
     * @return array 返回更新的结果，array("rs"=>,, 'msg'=>) ,rs==success/error  msg出错信息或null
     */
    public function update($id,$update_key,$update_value){
        $this->db->where(self::$COL_ID,$id);
        $q = $this->db->get(self::$DB);
        if($q->num_rows()<= 0){
            return array("rs"=>"error", 'msg'=>self::$MSG_NOT_EXIST);
        }
        $key_pid = self::$COL_PERMISSION_ID;
        $pid = $q->result()[0] -> $key_pid;
        $key_gid = self::$COL_UGROUP_ID;
        $gid = $q->result()[0] -> $key_gid;

        $arr=array_combine($update_key,$update_value);
        if(array_key_exists(self::$COL_PERMISSION_ID,$arr)){
            $pid = $arr[self::$COL_PERMISSION_ID];
        }
        if(array_key_exists(self::$COL_UGROUP_ID, $arr)){
            $gid = $arr[self::$COL_UGROUP_ID];
        }
        if(  $gid != $q->result()[0] -> $key_gid ||  $pid != $q->result()[0] -> $key_pid ){

            $this->db->where(array(self::$COL_PERMISSION_ID=>$pid));
            $this->db->where(array(self::$COL_UGROUP_ID=>$gid));
            $q = $this->db->get(self::$DB);
            if($q->num_rows() > 0) {
                //已经存在
                return array('rs'=>'error', 'msg' => $this::$MSG_ALREADY_EXIST);
            }
        }

        $this->db->where(self::$COL_ID,$id);
        $arr=array_combine($update_key,$update_value);
        $this->db->update(self::$DB,$arr);
        if($this->db->affected_rows()>0){
            log_message('info', $this->db->last_query());
            return array('rs'=>'success', 'msg'=>null);
        }
        log_message('error',$this->db->last_query().'数据库从ugoup_permission更新失败');
        return array('rs'=>'error', 'msg' => $this::$MSG_DB_ERROR);
    }

//    /**
//     * @param array $group_ids分组id的集合 
//     *
//     * @return array 返回搜索到的所有的permission_ids，array()
//     */
//    public function find_user_permission_ids($group_ids){
//        $this->db->where_in(self::$COL_UGROUP_ID,$group_ids);
//        $ugroup_permissions = $this->db->get(self::$DB)->result();
//        if($ugroup_permissions == null){
//            return null;
//        }
//        $permission_ids = array();
//       for($i =0;$i<count($ugroup_permissions);$i++){
//           array_push($permission_ids,$ugroup_permissions[$i]->permission_id);
//       }
//        $permission_ids = array_flip(array_flip($permission_ids));
//        return $permission_ids;
//    }
}