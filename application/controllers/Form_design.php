<?php

/**
 * Created by PhpStorm.
 * User: cyy
 * Date: 2016/8/4
 * Time: 17:56
 */
class Form_design extends CI_Controller{
    function __construct()
    {
        $this->need_login = true;
        parent::__construct();
        $this->load->helper('url_helper');
        $this->load->library('System_security');
        $this->load->library('session');
        $this->load->model('Report_Model','report_model');
    }


    //预览表单的功能
    public function preview_form(){
        $this->check_login("suser_or_user");
        if(!isset($_POST['design_content'])){
            header("Content-type: text/html; charset=utf-8");
            die("必须从报表创建页预览那里跳转此网页，不能直接打开此网页");
        }

        $this->load->library('Formdesign');

        $parse_content = $this->formdesign->parse_form($_POST['design_content'],$_POST['fields']);

        $design_content = $this->formdesign->unparse_form(array(
            'content_parse'=>$parse_content['parse'],
            'content_data'=>serialize($parse_content['data']),
        ),array(),array('action'=>'preview'));

        $data["content"] = $design_content;

        $this->load->view("report/test_one_report_view",$data);
    }

    //预览表单的时候，执行ajax请求表单的功能
    public function ajax_form(){
        $this->check_login("suser_or_user");
        $this->load->library('Formdesign');
        $parse_content = json_decode($_POST["parse_form"],true);

        $design_content = $this->formdesign->unparse_form(array(
            'content_parse'=>$parse_content['parse'],
            'content_data'=>serialize($parse_content['data']),
        ),array(),array('action'=>'preview'));

        $data["content"] = $design_content;

        $this->load->view("report/test_one_report_view",$data);
    }

    //预览统计表单，执行ajax请求表单的功能
    public function ajax_statistics_form(){
        $this->check_login("suser_or_user");
        $this->load->library('Formdesign');

        $parse_content = json_decode($_POST["parse_form"],true);


        $design_content = $this->formdesign->unparse_form(array(
            'content_parse'=>$parse_content['parse'],
            'content_data'=>serialize($parse_content['data']),
        ),array(),array('action'=>'preview'));

        $data["content"] = $design_content;

        $this->load->view("report/preview_one_statistics_report_view",$data);
    }

    public function show_one_statistics_report(){
//        if((!isset($_SESSION['uid']))&&(!isset($_SESSION['suid']))){
//            echo "您尚未登录或者登录已过期";
//            return;
//        }
        $this->check_login("suser_or_user");
        $this->load->library('Formdesign');
        $id =  $this->input->get_post("id");
        $this->load->model("Statistics_Report_Model","statistics_report_model");
        $statistics_report = $this->statistics_report_model->findId($id)["data"];
        $column_data = "formdesign_data";
        $column_parse = "formdesign_parse";
        
        $design_content = $this->formdesign->unparse_form(array(
            'content_parse'=>$statistics_report->$column_parse,
            'content_data'=>$statistics_report->$column_data
        ),array(),array('action'=>'preview'));

        $data["content"] = $design_content;
        $data["title"] = $this->statistics_report_model->findId($id)['data']->name;
        $this->load->view("report/show_one_statistics_report",$data);
    }
    //创建填报用的表单
    public function create_form(){
//        if(!isset($_SESSION['suid'])){
//            echo "您尚未登录或者登录已过期";
//            return;
//        }
        $this->check_login("suser");
        $this->load->view("report/create_report");
    }

    //创建统计用的表单
    public function create_statistics_form(){
//        if((!isset($_SESSION['uid']))&&(!isset($_SESSION['suid']))){
//            echo "您尚未登录或者登录已过期";
//            return;
//        }
        $this->check_login("suser_or_user");
        $this->load->view("report/create_statistics_report");
    }

    //最终确认提交报表
    public function commit_report(){
        $this->check_login("suser");
        $id = 0;
        //$this->load->library('Formdesign');
        $this->load->library("Form_Parse_Tool");
        $this->load->model("Report_Template_Model","tmpl_model");
        $this->load->model("Report_Model","report_model");
//        if(isset($_SESSION['suid'])) {
//            $id = 0;
//        }else{
//            echo json_encode(array("rs"=>"error","msg"=>"您尚未登录或登录已过期"));
//            return;
//        }
        $request = json_decode($_POST["parse_form"],true);
        $template = $request["template"];
        $parse = $request["parse"];
        $data = $request["data"];
        $title = $_POST['title'];
        //$json_data = json_encode($data);
        $serialize_data =  serialize($data);
//        echo json_encode($data);
//        return;
        $insert_data = $this->form_parse_tool->create_mongo_column_from_formdesign($data);//作为Mongo中tmpl模板的insertdata
        //这里验证报表是否有以_type结尾的元素
        $keys = array_keys($insert_data);
        $mongo_id = $this->tmpl_model->init($insert_data);
        if($mongo_id){
            $msg = $this->report_model->add($mongo_id, $title, $id, $id, 1,$template,$parse,$serialize_data);
        }else{
           $msg=array("rs"=>"error","msg"=>"mongo中报表模板初始化失败");
        }
        echo json_encode($msg);
    }


    //最终确认提交统计报表的信息
    public function commit_statistics_report(){
        $this->check_login("suser_or_user");
        $this->load->library("Form_Parse_Tool");
        $this->load->model("Report_Template_Model","tmpl_model");
        $this->load->model("Statistics_Report_Model","statistics_report_model");
        $request = json_decode($_POST["parse_form"],true);
        $template = $request["template"];
        $parse = $request["parse"];
        $data = $request["data"];
        $serialize_data =  serialize($data);
        if(isset($_SESSION['uid'])){
            $id = $_SESSION['uid'];
        }else if(isset($_SESSION['suid'])){
            $id = 0;
        }
//        else{
//            $msg = array('rs'=>'fail',"msg"=>"您尚未登录或者登录已过期");
//            echo json_encode($msg);
//            return;
//        }
        $msg = $this->statistics_report_model->add(26,$_POST['statistics_report_name'], $id, $id,$template,$parse,$serialize_data);
        echo json_encode($msg);
    }


//    public function show_report(){
//        $this->load->library('Formdesign');
//        $this->load->model("Report_Model","report_model");
////        $report = $this->report_model->findId(32)["data"];
//        $this->load->model("Statistics_Report_Model","statistics_report_model");
//        $statistics_report = $this->statistics_report_model->findId(9)["data"];
//        $column_data = "formdesign_data";
//        $column_parse = "formdesign_parse";
//


//
//        $design_content = $this->formdesign->unparse_form(array(
//            'content_parse'=>$statistics_report->$column_parse,
//            'content_data'=>$statistics_report->$column_data
//        ),array(),array('action'=>'preview'));
//
//        $data["content"] = $design_content;
//        $data["report_id"] = 26;
//        $this->load->view("report/show_one_report_view",$data);
//    }

    public function statistics_report_list(){
        $this->check_login("suser_or_user");
        $this->load->model('User_Model','user_model');
        $this->load->model('Statistics_Report_Model','statistics_report');
        if(isset($_POST['name'])){
            $name = $_POST['name'];
        }else{
            $name =null;
        }
        $data['info']['name']=$name;
        $pageNum = isset($_POST['pageNum']) ? $_POST['pageNum'] : 1;
        $numperpage = isset($_POST['numPerPage']) ? $_POST['numPerPage'] : 20;
        $this->statistics_report->change_page_size($numperpage);
        if(isset($_SESSION['uid'])){
            //获取本组人所有人的用户id
           $group_users_id =  $this->user_model->get_self_group_users_by_userid($_SESSION['uid']);
            //根据用户组所有人的id，检索名，page_size,pagenum获取对应页数上的统计报表
            $rs = $this->statistics_report->get_statistics_report_by_usersid_and_statistics_name($group_users_id,$name,$pageNum-1);
        }else if(isset($_SESSION['suid'])){
            $rs = $this->statistics_report->find_by_name($name,$pageNum-1);
        }
//        else{
//            echo "您尚未登录或者登录已经过期";
//            return;
//        }
        $data['datalist']=$rs['reports'];
        $i =0;
        foreach ($data['datalist'] as $row) {
            $data['datalist'][$i]->id= $row->id;
            $data['datalist'][$i]->name= $row->name;
            $data['datalist'][$i]->num = $i+1;
            $i++;
        }
        $data['info']['total'] = $rs['total'];
        $data['info']['page_size'] =$rs['page_size'];
        $data['info']['current'] = $rs["current"]+1;
        $this->load->view("report/statistics_report_list",$data);
    }

    public function commit_update_report_data(){
//        if((!isset($_SESSION["uid"]))){
//            echo json_encode(array("rs"=>"error","msg"=>"您尚未登录或者您不是普通用户"));
//            return;
//        }
        $this->check_login("user");
        $this->load->model("Report_Record_Model","report_record_model");
        $this->load->model("Report_Model","report_model");
        $this->load->library("Form_Parse_Tool");
        $data_report_template = $_POST["report_template"];
        $report_id = $_POST["report_id"];
        $mongo_id = $_POST["mongo_id"];
        $report_data = $_POST['data'];
        $this->form_parse_tool->change_report_data_type($report_data,$data_report_template);
        $flag = $this->report_record_model->update_report_data($mongo_id,$report_id,$report_data);
        if($flag){
            echo json_encode(array("rs"=>"success"));
        }else{
            echo json_encode(array("rs"=>"error","msg"=>"数据保存失败"));
        }
    }
    //填报报表
    public function commit_report_data(){
//        if(isset($_SESSION['uid'])){
//            $id=$_SESSION['uid'];
//        }else{
//            echo "您尚未登录或者登录已经过期";
//            return;
//        }
        $this->check_login("user");
        $id=$_SESSION['uid'];
        $this->load->model("Report_Record_Model","record_model");
        $this->load->model("Report_Model","report_model");
        $this->load->library("Form_Parse_Tool");
        $this->load->model('Report_Grant_Model','report_grant');
        $data_report_template = $_POST["report_template"];
        $report_id = $_POST["report_id"];
        $report_data = $_POST['data'];
        //首先需要获取最后一次本人填充的时间
        $last_record_time = $this->record_model->get_last_record_time_by_userid_and_report_id($id,$report_id);
        $report_grant = $this->report_grant->find($report_id,$_SESSION['ugroupid'])['data'][0];
//        if($report_grant === null){
//            echo "找不到对应的分配报表权限（report_grant）";
//            return;
//        }
        //报表分配权限开始时间
        $special_time_start = $report_grant->special_time_start;
        //报表分配权限结束时间
        $special_time_end = $report_grant->special_time_end;
        $this->load->library("My_time");
        if(time()>=$special_time_start&&time()<=$special_time_end){
            $time = $this->my_time->get_start_and_end_time($special_time_start,$report_grant->special_frequency);
            if($last_record_time>=$time[0]&&$last_record_time<$time[1]){
                echo json_encode(array("rs"=>"error","msg"=>"该报表的填充间隔是".$report_grant->special_frequency.",本次时间间隔内已经填充过了，上次填充时间".date('Y-m-d H:i:s',(string)$last_record_time)."下次可以在".date('Y-m-d H:i:s',(string)$time[1])."填充"));
                return ;
            }
        }else {
            $time = $this->my_time->get_start_and_end_time($report_grant->update_time, $report_grant->frequency);
            if($last_record_time>=$time[0]&&$last_record_time<$time[1]){
                echo json_encode(array("rs"=>"error","msg"=>"该报表的填充间隔是".$report_grant->frequency.",本次时间间隔内已经填充过了，上次填充时间".date('Y-m-d H:i:s',(string)$last_record_time)."下次可以在".date('Y-m-d H:i:s',(string)$time[1])."填充"));
                return ;
            }
        }
//        unset($_POST["report_id"]);
//        unset($_POST["report_type"]);
        $data = array();
        //获取一个报表的模板，判断对应的表单name的类型
        $report = $this->report_model->findId($report_id)["data"];
        if($report == null){
            die("no report to access");
        }
//        $data_type = $this->form_parse_tool->get_data_type(unserialize($report->formdesign_data));
        $this->form_parse_tool->change_report_data_type($report_data,$data_report_template);

        $rs = $this->record_model->add($report_id,$report_data,array('time'=>time(),'GPS'=>'','IP'=>$_SERVER['SERVER_ADDR'],'UA'=>$_SERVER['HTTP_USER_AGENT'] ,'IMEI'=>'','uid'=>(int)$id,'from'=>'pc'));
        if($rs){
            echo json_encode(array("rs"=>"success"));
        }else{
            echo json_encode(array("rs"=>"error"));
        }
    }

    //以下均是 演示区使用的内容
    //演示图表报表
    public function show_graph_report_demo(){
        $this->load->view("report/show_graph_report_demo.php");
    }
    //显示一个演示用的填写报表
    public function demo_show_one_report(){
        $this->load->view("report/demo_show_one_report.php");
    }

    public function demo_show_one_statistics_report(){
        $this->load->view("report/demo_show_one_statistics_report.php");
    }

    /**
     * 获取所有自己能编辑的报表，显示出来选择更新
     */
    public function report_list(){
        $this->check_login("suser_or_user");
        $this->load->model("Report_Model", "report_model");
        $this->load->model("User_Model", "user_model");
        $this->load->model('Report_Grant_Model','report_grant');
        $this->load->model("Group_Model","group_model");
        $data['reportlist'] = array();
        //搜索的报表名
        $name = isset($_POST['name'])?$_POST['name']:null;
        $data['info']['name']=$name;
        if(isset($_SESSION['uid'])) {
           $id= $_SESSION['uid'];
        }else if(isset($_SESSION['suid'])){;
          $id=0;
        }
//        else{
//            echo "您尚未登录或者登录已过期";
//        }
        $pageNum = isset($_POST['pageNum']) ? $_POST['pageNum'] : 1;
        $numperpage = isset($_POST['numPerPage']) ? $_POST['numPerPage'] : 20;
        //修改每页显示的数量
       $this->report_model->change_page_size($numperpage);
        if($id !==0){//不是超级管理员只是普通用户
//           $user =  $this->user_model->findId($id)['data'];
            //获取用户所在的分组
            $group_id = $_SESSION['ugroupid'];
            $report_ids = $this->report_grant->get_report_ids_by_group_id($group_id);
            //获取当前页需要显示的报表以及总数以及每页显示的数字
            $rs= $this->report_model->get_reports_by_report_ids($report_ids,$pageNum-1,$name);
        }else if($id === 0){
            //获取所有的报表
            $rs = $this->report_model->find_by_name($name,$pageNum-1);
        }
        $data['reportlist']  = $rs['reports'];
        $i = 0;
        foreach ($data['reportlist'] as $row) {
            $data['reportlist'][$i]->num = $i + 1;
            $i++;
        }

        $data['info']['total'] = $rs['total'];
        $data['info']['page_size'] =$rs['page_size'];
        $data['info']['current'] = $rs['page_num'];
        if(isset($_SESSION['uid'])) {
            $this->load->view("report/user_report_list.php", $data);
        }else if(isset($_SESSION['suid'])){
            $this->load->view("report/super_report_list.php", $data);
        }
    }
    public function filled_report_find(){
//        if(!isset($_SESSION['uid'])){
//            echo "您尚未登录或者登录已过期";
//            return;
//        }
        $this->check_login("user");
        $this->load->model('Report_Grant_Model','report_grant');
        $this->load->model("Report_Record_Model","report_record");
        $pageNum = isset($_POST['pageNum']) ? $_POST['pageNum'] : 1;
        $numperpage = isset($_POST['numPerPage']) ? $_POST['numPerPage'] : 20;
        $this->report_record->change_page_size($numperpage);
        $data['info']['current'] = $pageNum;
        $data['info']['numperpage'] = $numperpage;
        $start_time = strtotime($this->input->get_post("date_start"));
        $end_time = strtotime($this->input->get_post("date_end"));
        if(!$start_time){
            $start_time = null;
        }
        if(!$end_time){
            $end_time = null;
        }
//        $start_time = isset($_POST['start_time'])?$_POST['start_time']:null;
//        $end_time = isset($_POST['end_time'])?$_POST['end_time']:null;
        //这里的reportid由于第一次是选择报表所以用get_post获取，如果是翻页就需要用$_POST来获取
        $reportid = $this->input->get_post("reportid");
        if($reportid == 0){
            $reportid = isset($_POST['report_id']) ? $_POST['report_id'] : 0;
        }
        $data['info']['reportid'] = $reportid;
        $data['info']['start_time'] = $start_time;
        $data['info']['end_time'] = $end_time;
        $rs =$this->report_record->get_records_by_userid_and_start_time_and_end_time_and_report_id($_SESSION['uid'],$start_time,$end_time,$reportid,$pageNum-1);
        $data['datalist'] = $rs['report_records'];
        $data['info']['total'] = $rs['total'];
        $data['info']['page_size'] = $rs['page_size'];
        if($data['datalist']){
            $i = 0;
            foreach ($data['datalist'] as $row1) {
                $data['datalist'][$i]['time'] = date('Y-m-d H:i',$row1['time']);
                $data['datalist'][$i]['id'] = $i+1;
                $data['datalist'][$i]['_id'] = (string)$row1['_id'];
                $i++;
            }
        }
        //获取用户所在的分组
        $group_id = $_SESSION['ugroupid'];
        $report_ids = $this->report_grant->get_report_ids_by_group_id($group_id);
        //获取当前页需要显示的报表以及总数以及每页显示的数字
        $rs= $this->report_model->get_all_reports_by_report_ids($report_ids);
        $data['reportlist']  = array();
        $i = 0;
        foreach ($rs['reports'] as $row) {
            $data['reportlist'][$i]['num'] = $i + 1;
            $data['reportlist'][$i]['name'] = $row->name;
            $data['reportlist'][$i]['id'] = $row->id;
            $i++;
        }
        $this->load->view("report/filled_in_report_list.php", $data);
    }


    public function filled_in_report_list(){
        $this->check_login("user");
        $this->load->model('Report_Grant_Model','report_grant');
        $this->load->model('User_Model','user_model');
        $this->load->model("Report_Model", "report_model");
        if(isset($_SESSION['uid'])) {
            $userid= $_SESSION['uid'];
        }
//        else{
//            echo "您尚未登录或者登录已过期";
//            return;
//        }
        $data['info']['ugroupid'] = '';
            $user =  $this->user_model->findId($userid)['data'];
            if($user == null){
                echo "用户不存在";
                log_message("error","id为".$userid."的用户不存在");
                return;
            }
//            获取用户所在的分组
            $group_id = $_SESSION['ugroupid'];
            $report_ids = $this->report_grant->get_report_ids_by_group_id($group_id);
            //获取当前页需要显示的报表以及总数以及每页显示的数字
            $rs= $this->report_model->get_all_reports_by_report_ids($report_ids);
        $data['reportlist']  = array();
        $i = 0;
        foreach ($rs['reports'] as $row) {
            $data['reportlist'][$i]['num'] = $i + 1;
            $data['reportlist'][$i]['name'] = $row->name;
            $data['reportlist'][$i]['id'] = $row->id;
            $i++;
        }
        $pageNum = isset($_POST['pageNum']) ? $_POST['pageNum'] : 1;
        $numperpage = isset($_POST['numPerPage']) ? $_POST['numPerPage'] : 20;
        $data["info"]["page_size"]=$numperpage;
        $data['info']['current'] = $pageNum;
        $data['info']['reportid'] = 0;
        $data['info']['start_time'] = 0;
        $data['info']['end_time'] = 0;
        $data['info']['total'] = 0;
        $data["datalist"]=array();
        $this->load->view("report/filled_in_report_list.php", $data);    
    }


    public function prepare_update_one_statistics_report(){
        $this->check_login("suser_or_user");
        $this->load->library('Formdesign');
        $this->load->model("Statistics_Report_Model","statistics");
        $id = $this->input->get_post("id");
        $data['id'] = $id;
        $statistics_report = $this->statistics->findId($id)['data'];
        $column_data = "formdesign_data";
        $column_parse = "formdesign_parse";
//        $design_content = $this->formdesign->unparse_form(array(
//            'content_parse'=>$statistics_report->$column_parse,
//            'content_data'=>$statistics_report->$column_data
//        ),array(),array('action'=>'preview'));
//        $data['content'] = $design_content;
        $data['content'] = $statistics_report->formdesign_tmpl;
        $data['name'] = $statistics_report->name;
        $this->load->view("report/update_statistics_report.php",$data);
    }
    /**
     * 准备更新报表，根据选择的报表id获取report表中的参数以及报表的id放在data中跳转到update_report.php并且显现出来
     */
    public function prepare_update_one_report(){
//        if(!isset($_SESSION['suid'])){
//            echo "您尚未登录或者登录已过期";
//            return;
//        }
        $this->check_login("suser");
        $this->load->library('Formdesign');
        $this->load->model("Report_Model","report_model");
        $id = $this->input->get_post("id");
        $data['id'] = $id;
        $report = $this->report_model->findId($id)['data'];
        $column_data = "formdesign_data";
        $column_parse = "formdesign_parse";
//        var_dump($report->$column_data);
//        return;
//        $design_content = $this->formdesign->unparse_form(array(
//            'content_parse'=>$report->$column_parse,
//            'content_data'=>$report->$column_data
//        ),array(),array('action'=>'preview'));
//        $data['content'] = $design_content;
        $data['content'] = $report->formdesign_tmpl;
        $data['name'] = $report->name;
//        var_dump($design_content);
//        return;
//        return;
        $this->load->view("report/update_one_report.php",$data);
    }
//这里是以后可以显示所有已经填写的报表了，只要修改$records[0]这个参数
//    public function show_report_record(){
//        $this->load->library('Formdesign');
//        $this->load->model("Report_Model","report_model");
//        $this->load->model("Report_Record_Model","report_record_model");
//
////        $id = $this->input->get_post("id");//报表的Id
//        $id =27;
//        $data['report_id'] = $id;
//        $report = $this->report_model->findId($id)['data'];
////        if(isset($_SESSION['uid'])){
////            $user_id=$_SESSION['uid'];
////        }else if(isset($_SESSION('suid'))){
////            $user_id=0;
////        }else{
////            echo "您尚未登录或者登录已经过期";
////            return;
////        }
//        $records = $this->report_record_model->get_records_by_userid_and_report_id(44,27);
//        $column_data = "formdesign_data";
//        $column_parse = "formdesign_parse";
//        $design_content = $this->formdesign->unparse_form(array(
//            'content_parse'=>$report->$column_parse,
//            'content_data'=>$report->$column_data
//        ),$records[0],array('action'=>'preview'));
//        $data['content'] = $design_content;
//        $this->load->view("report/show_one_report_view.php",$data);
//    }
    /**
     * 准备填充数据，选择报表，将需要的参数放在data中传递到show_one_report_view.php中，在界面上将报表显示出来
     */
    public function prepare_fill_in_one_report(){
        $this->check_login("user");
        $this->load->model("Report_Grant_Model","report_grant");
        $this->load->library('Formdesign');
        $this->load->library('Form_Parse_Tool');
        $this->load->model("Report_Model","report_model");
        $this->load->model("Report_Record_Model","report_record");
        $id = $this->input->get_post("id");//报表的Id
        if(isset($_SESSION['uid'])){
            $user_id=$_SESSION['uid'];
        }
//        else{
//            echo "您尚未登录或者登录已经过期";
//            return;
//        }
        //首先需要获取最后一次本人填充的时间
        $last_record_time = $this->report_record->get_last_record_time_by_userid_and_report_id($user_id,$id);
        $report_grant = $this->report_grant->find($id,$_SESSION['ugroupid'])['data'][0];
        if($report_grant === null){
            echo "找不到对应的分配报表权限（report_grant）";
            return;
        }
        //报表分配权限开始时间
        $special_time_start = $report_grant->special_time_start;
        //报表分配权限结束时间
        $special_time_end = $report_grant->special_time_end;
        $this->load->library("My_time");
        if(time()>=$special_time_start&&time()<=$special_time_end){
            $time = $this->my_time->get_start_and_end_time($special_time_start,$report_grant->special_frequency);
            if($last_record_time>=$time[0]&&$last_record_time<$time[1]){
                echo "该报表的填充间隔是".$report_grant->special_frequency.",本次时间间隔内已经填充过了，上次填充时间".date('Y-m-d H:i:s',(string)$last_record_time)."下次可以在".date('Y-m-d H:i:s',(string)$time[1])."填充";
                return ;
            }
        }else {
            $time = $this->my_time->get_start_and_end_time($report_grant->update_time, $report_grant->frequency);
            if($last_record_time>=$time[0]&&$last_record_time<$time[1]){
                echo "该报表的填充间隔是".$report_grant->frequency.",本次时间间隔内已经填充过了，上次填充时间".date('Y-m-d H:i:s',(string)$last_record_time)."下次可以在".date('Y-m-d H:i:s',(string)$time[1])."填充";
                return ;
            }
        }

        $data['report_id'] = $id;
        $report = $this->report_model->findId($id)['data'];
        $this->load->model("Report_Template_Model","report_template_model");
        $report_template =  $this->report_template_model->get_one_report_template($report->_id);
        $column_data = "formdesign_data";
        $column_parse = "formdesign_parse";
        $design_content = $this->formdesign->unparse_form(array(
            'content_parse'=>$report->$column_parse,
            'content_data'=>$report->$column_data
        ),array(),array('action'=>'preview'));
        $data['content'] = $design_content;
        $data['report_type']=json_encode($report_template);//
        $this->load->view("report/fill_in_one_report_view.php",$data);
    }

    public function prepare_update_one_report_data(){
        $this->check_login("user");
        $this->load->model("Report_Grant_Model","report_grant");
        $this->load->library('Formdesign');
        $this->load->library('Form_Parse_Tool');
        $this->load->model("Report_Model","report_model");
        $this->load->model("Report_Record_Model","report_record");
        $mongo_id = $this->input->get_post("mongo_id");
        $report_id =  $this->input->get_post("report_id");
        if(isset($_SESSION['uid'])){
            $user_id=$_SESSION['uid'];
        }
//        else{
//            echo "您尚未登录或者登录已经过期";
//            return;
//        }
       
        $data['report_id'] = $report_id;
        $data['mongo_id'] = $mongo_id;
        $report = $this->report_model->findId($report_id)['data'];
        $record = $this->report_record->get_record_by_mongo_id_and_report_id($mongo_id,$report_id);
        $this->load->model("Report_Template_Model","report_template_model");
        $report_template =  $this->report_template_model->get_one_report_template($report->_id);
        $column_data = "formdesign_data";
        $column_parse = "formdesign_parse";
        $design_content = $this->formdesign->unparse_form(array(
            'content_parse'=>$report->$column_parse,
            'content_data'=>$report->$column_data
        ),$record,array('action'=>'preview'));
        $data['content'] = $design_content;
        $data['report_type']=json_encode($report_template);
        $this->load->view("report/prepare_update_one_report_data_view.php",$data);
    }
    public function show_one_report(){
        $this->check_login("suser_or_user");
        $this->load->model("Report_Grant_Model","report_grant");
        $this->load->library('Formdesign');
        $this->load->model("Report_Model","report_model");
        $this->load->model("Report_Record_Model","report_record");
        $id = $this->input->get_post("id");//报表的Id
        if(isset($_SESSION['uid'])){
            $user_id=$_SESSION['uid'];
        }else if(isset($_SESSION['suid'])){
            $user_id= 0;
        }
//        else {
//            echo "您尚未登录或者登录已经过期";
//            return;
//        }
        $data['report_id'] = $id;
        $report = $this->report_model->findId($id)['data'];
        $column_data = "formdesign_data";
        $column_parse = "formdesign_parse";
        $design_content = $this->formdesign->unparse_form(array(
            'content_parse'=>$report->$column_parse,
            'content_data'=>$report->$column_data
        ),array(),array('action'=>'preview'));
        $data['content'] = $design_content;
        $this->load->view("report/show_one_report_view.php",$data);
    }

    public function show_one_filled_report(){
        $this->check_login("user");
        $this->load->model("Report_Model", "report_model");
        if(isset($_SESSION['uid'])){
         $user_id=$_SESSION['uid'];
      }
//    else{
//        echo "您尚未登录或者登录已经过期";
//        return;
//    }
         $mongo_id = $this->input->get_post("mongo_id");
         $report_id =  $this->input->get_post("report_id");
         $this->load->library('Formdesign');
         $this->load->model("Report_Model","report_model");
         $this->load->model("Report_Record_Model","report_record_model");
         $data['report_id'] = $report_id;
         $report = $this->report_model->findId($report_id)['data'];
         $record = $this->report_record_model->get_record_by_mongo_id_and_report_id($mongo_id,$report_id);
         $column_data = "formdesign_data";
         $column_parse = "formdesign_parse";
         $design_content = $this->formdesign->unparse_form(array(
              'content_parse'=>$report->$column_parse,
                'content_data'=>$report->$column_data
         ),$record,array('action'=>'preview'));
        $data['content'] = $design_content;
        $this->load->view("report/show_one_report_data_view.php",$data);
}
    public function update_one_filled_report(){
        $this->check_login("user");
        $this->load->model("Report_Model", "report_model");
        if(isset($_SESSION['uid'])){
            $user_id=$_SESSION['uid'];
        }
//        else{
//            echo "您尚未登录或者登录已经过期";
//            return;
//        }
        $mongo_id = $this->input->get_post("mongo_id");
        $report_id =  $this->input->get_post("report_id");
        $this->load->library('Formdesign');
        $this->load->model("Report_Model","report_model");
        $this->load->model("Report_Record_Model","report_record_model");
        $data['report_id'] = $report_id;
        $report = $this->report_model->findId($report_id)['data'];
        $record = $this->report_record_model->get_record_by_mongo_id_and_report_id($mongo_id,$report_id);
        $column_data = "formdesign_data";
        $column_parse = "formdesign_parse";
        $design_content = $this->formdesign->unparse_form(array(
            'content_parse'=>$report->$column_parse,
            'content_data'=>$report->$column_data
        ),$record,array('action'=>'preview'));
        $data['content'] = $design_content;
        $data['reportid'] = $report_id;
        $data['mongo_id'] = $mongo_id;
        $this->load->view("report/prepare_update_one_report_data_view.php",$data);
    }

    public function update_commit_statistics_report(){
        $this->check_login("suser_or_user");
        $this->load->library('Formdesign');
        $this->load->library("Form_Parse_Tool");
        $this->load->model("Statistics_Report_Model","statistics");
        $this->load->model("Report_Model","report_model");

        $request = json_decode($_POST["parse_form"],true);
        $template = $request["template"];
        $parse = $request["parse"];
        $data = $request["data"];
        //$json_data = json_encode($data);
        $insert_data = $this->form_parse_tool->create_mongo_column_from_formdesign($data);//作为Mongo中tmpl模板的insertdata

        $serialize_data =  serialize($data);
            if(isset($_SESSION['uid'])){
                $id=$_SESSION['uid'];
            }else if(isset($_SESSION['suid'])){
                $id= 0;
            }
//            else{
//                echo "您尚未登录或者登录已经过期";
//                return;
//            }

            $msg = $this->statistics->update($_POST["id"],array("name","updator_id","update_time","formdesign_tmpl","formdesign_parse","formdesign_data"),array($_POST['statistics_report_name'],$id,time(),$template,$parse,$serialize_data));
        echo json_encode($msg);
    }
    /**
     * 用户修改报表后提交后修改report表中的参数（html参数，时间，修改人id）
     */
    
    public function update_commit_report(){
//        if(!isset($_SESSION['suid'])){
//            echo json_encode(array("rs"=>"fail","msg"=>"您尚未登录或者你不是超级管理员"));
//            return;
//        }
        $this->check_login("suser");
        $this->load->library('Formdesign');
        $this->load->library("Form_Parse_Tool");
        $this->load->model("Report_Template_Model","tmpl_model");
        $this->load->model("Report_Model","report_model");
        $request = json_decode($_POST["parse_form"],true);
        $template = $request["template"];
//        echo json_encode($template);
//        return;
        $parse = $request["parse"];
        $data = $request["data"];
        if($this->verify_is_exists($data)) {//此处验证数据中是否有两个是一样的名字
            echo json_encode(array("rs"=>"fail","msg"=>"有两个控件的名字一致","data"=>$data));
            return;
        }
        $serialize_data =  serialize($data);
        $r = $this->report_model->findId((int)$_POST["id"]);
        if($r['rs']=="error"){
            echo json_encode(array("rs"=>"fail","msg"=>"找不到这个报表"));
            return;
        }
        $mongo_id = $r['data']->_id;
        $report_template = $this->tmpl_model->get_one_report_template($mongo_id);//获取mongo中本报表的模板
        $insert_data = $this->form_parse_tool->create_mongo_column_from_formdesign($data);//作为Mongo中tmpl模板的insertdata

//        echo json_encode($data);
//        return;
        $update_report_template = $report_template;
//        array_push($update_report_template,array("_id"=>$report_template['_id']));
//        echo json_encode($insert_data);
//        return;
        foreach($insert_data as $key=>$value){
            if($value['type']!==null){
            //需要判断如果修改的键值存在并且对应的类型与之前的不一致则报错
                if(array_key_exists($key,$report_template)&&$report_template[$key]['type']!==$value['type']){
                    $msg = array(
                        "rs" => "fail",
                        "msg"=>"请修改键名称，键值所对应的类型已经被定义过了"
                    );
                echo json_encode($msg);
                    return;
                }
                $update_report_template[$key]['type'] = $value['type'];
            }else{
                $update_report_template[$key]['type'] = $report_template[$key]['type'];
            }
        }

        $flag = $this->tmpl_model->update_all_column($mongo_id,$update_report_template);
        if($flag){
           if(isset($_SESSION['suid'])){
                $id= 0;
            }else{
               $msg = array(
                   "rs" => "fail",
                   "msg"=>"模板保存失败"
               );
               echo json_encode($msg);
               return;
            }
            $report_name = $_POST['report_name'];
            $msg = $this->report_model->update($_POST["id"],array("updator_id","update_time","formdesign_tmpl","formdesign_parse","formdesign_data","name"),array($id,time(),$template,$parse,$serialize_data,$report_name));
        }else{
            $msg = array(
                "rs" => "fail",
                "msg"=>"模板保存失败"
            );
        }
        echo json_encode($msg);
    }
    //将mongo中的对象转换成数组


    //演示用的 统计报表中的表格生成列。demo使用完毕后可删除
    public function demo_statistics_report_table(){
        $this->load->view("report/demo_show_one_statistics_report_table");
    }
//修改报表后，验证报表中是否有两个控件的名字是一致的
    private function verify_is_exists($array){
        for($i=0;$i<count($array)-1;$i++){
            for($j=$i+1;$j<count($array);$j++){
                if($array[$i]['name']==$array[$j]['name']){
                    return true;
                }
            }
        }
        return false;
    }
}