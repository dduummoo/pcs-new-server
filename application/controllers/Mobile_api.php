<?php

/**
 * Created by PhpStorm.
 * User: cyy
 * Date: 2016/8/14
 * Time: 23:12
 *
 *
 *  android和ios共用一套接口，访问C=mobile_api&F=api&model=xxx&action=yyy
 *  其中xxx代表手机端请求的模块，yyy代表手机端请求的方法
 *
 */
class Mobile_api extends CI_Controller
{
    private $rs;
    public static $COOKIE_TIME;

    //禁止在构造函数里，初始化的时候，初始化各种model，在对应的function里分别load 初始化model
    // 因为一次请求，并不需要一次性全部加载所有model
    function __construct()
    {
        parent::__construct();
        $this->rs = array(
            "status" => "need_login",    //请求的状态，分为3种，error,ok,need_login
            "data" => null,          //这里的data直接放返回的数据
            "msg" => "no model and method to handle this request " //这里可以附带对status的解释信息
        );
        self::$COOKIE_TIME = config_item('cookie_time');

    }


    /**
     * 查看token对应的user是否登录了，如果token存在没过期就返回true， 过期了删掉记录返回false.
     * @param string $token
     * @param string $tsoken
     * @return  array 如果token存在并且 没过期就返回 array("rs"=>"ok", uid=>查询到的结果)
     *             过期了删掉记录返回array("rs"=>"error", uid=>null)
     */
    private function checkLogin()
    {
        $token = $this->input->cookie("token");
        if ($token == "" || $token == null) {
            return array("rs" => "error", "uid" => null);
        }
        $this->load->model("Mobile_Model", "mobileModel");
        $result = $this->mobileModel->find_by_token($token);
        if ($result == null) {
            return array("rs" => "error", "uid" => null);
        }
        $time = "expired_time";
        $uid = "user_ID";
        $expired_time = $result[0]->$time;
        if (time() > $expired_time) {
            $this->mobileModel->delete($token);
            return array("rs" => "error", "uid" => null);
        }
        return array("rs" => "ok", "uid" => $result[0]->$uid);
    }

    /**
     *
     * 共用入口函数，负责处理各项请求的事项,
     *
     *  android和ios共用一套接口，访问C=mobile_api&F=api&model=xxx&action=yyy
     *  其中xxx代表手机端请求的模块，yyy代表手机端请求的方法
     *
     */
    public function api()
    {
        $model = isset($_GET["model"]) ? $_GET["model"] : "";
        $action = isset($_GET["action"]) ? $_GET["action"] : "";


        if ($model == "user") {
            $this->user($action, $_GET, $_POST);
        } else if ($model == "ugroup") {
            $this->ugroup($action, $_GET, $_POST);
        } else if ($model == "acl") {
            $this->acl($action, $_GET, $_POST);
        } else if ($model == "permission") {
            $this->permission($action, $_GET, $_POST);
        } else if ($model == "report") {
            $this->report($action, $_GET, $_POST);
        } else if ($model == "statistics") {
            $this->statistics($action, $_GET, $_POST);
        } else if ($model == "test") {
            $this->test($action, $_GET, $_POST);
        } else if ($model == "system") {
            $this->system($action, $_GET, $_POST);
        } else if ($model == "view") {
            $this->view($action, $_GET, $_POST);
            return;
        }

        //如果没有任何数据对rs进行覆盖，将会返回一个错误信息
        echo json_encode($this->rs);

    }

    //--------------------------------------------------
    //配置相关的接口

    /**
     * 返回基本配置，这个东西目前，只要能返回一个简单数组就行了。后序的再说
     * @param $post
     * @return array 如果登录过，rs == ok，如果没有登录过 rs == need_login
     * 不管有没有登录，data里面都返回至少如下内容。
     * array(
     * "software_pcs_name" :"xxx派出所",   //使用单位的姓名
     * "forget_password_tip":"忘记密码，请拨打xxxxxx", //点忘记密码弹出的提示框
     * "backgroup_picture":"http://xxx.xx.xx/bg.jpg"  //登录页背景图片
     * “level_groups": 返回关于模拟用户分组的层级数组
     * Array ( [0] => Array ( [id] => 26 [name] => 海淀区 [father_id] => 0 ) [1] => Array ( [id] => 29 [name] => 大钟寺派出所 [father_id] => 26 ) 主要是看name前面空格的个数，用于下拉列表框
     * );
     */
    private function system_config($post)
    {
        $res = $this->checkLogin();
        if ($res["rs"] == "ok") {
            $this->rs["status"] = "ok";
        } else {
            $this->rs['status'] = "need_login";
        }
        $this->rs["msg"] = null;
        $this->rs["data"] = array(
            "software_pcs_name" => "xxx派出所",   //使用单位的姓名
            "forget_password_tip" => "忘记密码，请拨打xxxxxx", //点忘记密码弹出的提示框
            "backgroup_picture" => "http://xxx.xx.xx/bg.jpg"  //登录页背景图片
        );
        $this->load->model("Group_Model", "groupModel");
        $this->rs["data"]["level_groups"] = $this->groupModel->level_groups();
    }

    /**
     * system 移动端模块的处理
     * @param $action
     * @param $get
     * @param $post
     */
    private function system($action, $get, $post)
    {
        if ($action == "config") {
            $this->system_config($post);
        } else {

        }

    }


    //---------------------------------User 相关的接口------------------------------------------
    /**
     * 根据post 的用户名， 密码 ，进行登录验证
     * @param $post 需要传入用户名 密码 分组id   密码是经过加密的（md5($username + "^_^" + $password);）
     * @return  array   array(
     * " status" => "error" 或者 ok,
     * "data" => 登录成功返回array(uid=>"") 失败返回null
     * "msg" => null 或者出错信息
     * );
     */
    private function userLogin($post)
    {
        $username = isset($post["username"]) ? $post["username"] : "";
        $password = isset($post["password"]) ? $post["password"] : "";
        $group_id = isset($post["group_id"]) ? $post["group_id"] : "";
        if ($username == "" || $password == "" || $group_id == "") {
            $this->rs["status"] = "error";
            $this->rs["msg"] = "empty input";
        }
        $res = $this->userModel->login($username, $password, $group_id);
        if ($res["rs"] == "error") {
            $this->rs["status"] = "error";
            $this->rs["msg"] = "wrong input";
        } else {
            $uidStr = "id";
            $this->rs["status"] = "ok";
            $this->rs["msg"] = null;
            $this->rs["data"] = array("uid" => $res["data"]->$uidStr);


            //设置token 到 cookie
            $uid = $res["data"]->$uidStr;
            $this->mobileModel->delete_by_uid($uid);
            $res = $this->mobileModel->add($uid);
            if ($res['token'] != "") {
                $this->input->set_cookie("token", $res['token'], self::$COOKIE_TIME);
            }
        }
    }

    /**
     * 用户下线 删除cookie以及token
     */
    private function userLogout()
    {
        $token = $this->input->cookie("token");
        if ($token != "") {
            $this->mobileModel->delete($token);
        }
        $this->input->set_cookie("token", "", self::$COOKIE_TIME);
        $this->rs = array(
            "status" => "ok",
            "data" => null,
            "msg" => "logout "
        );
    }

    /**
     * 修改密码， 需要用户id 和 原来的密码， 以及新更改的密码(密码都已经加密)
     * @return   array 修改成功  array(
     * "status" => "ok",
     * "data" => null,
     * "msg" => null
     * );
     *             失败的话 status 为 error ,  msg 显示出错原因
     *
     */
    private function updatePwd($post)
    {
        $user_id = isset($post["user_id"]) ? $post["user_id"] : "";
        $old_password = isset($post["old_password"]) ? $post["old_password"] : "";
        $new_password = isset($post["new_password"]) ? $post["new_password"] : "";
        if ($user_id == "" || $old_password == "" || $new_password == "") {
            $this->rs["status"] = "error";
            $this->rs["msg"] = "empty input";
            return;
        }
        if($old_password == $new_password ){
            $this->rs["status"] = "error";
            $this->rs["msg"] = "new password should not be same as old password";
            return;
        }
        $res = $this->userModel->findId($user_id);
        if ($res["rs"] == "error") {
            $this->rs["status"] = "error";
            $this->rs["msg"] = "user not exist";
            return;
        }

        $passWordStr = "password";
        $real_password = $res["data"]->$passWordStr;

        if ($real_password != $old_password) {
            $this->rs["status"] = "error";
            $this->rs["msg"] = "wrong password";
            return;
        }
        $res = $this->userModel->update_pwd($user_id, array($passWordStr), array($new_password));
        if ($res["rs"] == "error") {
            $this->rs["status"] = "error";
            $this->rs["msg"] = "database update error";
            return;
        }

        $this->rs["status"] = "ok";
        $this->rs["msg"] = null;
    }


    /**
     *
     * 处理用户信息相关的请求
     *
     * @param $action 请求的方法
     * @param $get 请求的GET的参数
     * @param $post 请求的POST的参数
     */
    private function user($action, $get, $post)
    {
        $this->load->model("Mobile_Model", "mobileModel");
        $this->load->model("User_Model", "userModel");
        if ($action == "login") {
            $this->userLogin($post);
        } elseif ($action == "logout") {
            $this->userLogout();
        } elseif ($action == "update_pwd") {
            $this->updatePwd($post);
        }
        return $this->rs;
    }


    //--------------------------------------------------------------------------------------
    // 报表相关的接口

    /**
     * 获取所有自己能编辑的报表，显示出来选择更新
     *
     */
    private function show_all_report($post)
    {
        /**
         * 获取所有自己能编辑的报表，显示出来选择更新
         */

        $loginInfo = $this->checkLogin();
        if( $loginInfo["rs"] != "ok"){
            $data["url"]="";
            $data["url_info"]="登录";
            $data["info"]="您尚未登录或者登录已过期!";
            $this->load->view("mobile/report/msg.php", $data);
            return;
        }else{
            $uid = $loginInfo["uid"];
        }
        $this->load->model("Report_Model", "report_model");
        $this->load->model("User_Model", "user_model");
        $this->load->model('Report_Grant_Model','report_grant');
        $this->load->model("Group_Model","group_model");
        $this->load->helper('url');
        $this->load->library('pagination');
      //  $uid = 113;
        $data['reportlist'] = array();
        $name = isset($_POST['name']) ? $_POST['name'] : null;
        $name = isset($_GET['name']) ? $_GET['name'] : $name;
        $start = 0;
        if(isset($_GET["per_page"])){
            $start = $_GET["per_page"] - 1;
        }
        $this->report_model->change_page_size(10);

        $userInfo =  $this->user_model->findId($uid)['data'];
        $gidStr = "group_id";
        $group_id = $userInfo->$gidStr;
        $report_ids = $this->report_grant->get_report_ids_by_group_id($group_id);
        //获取当前页需要显示的报表以及总数以及每页显示的数字
        $data= $this->report_model->get_reports_by_report_ids($report_ids,$start,$name);

        $data['reportlist'] = $data['reports'];
        $i = 0;
        foreach ($data['reportlist'] as $row) {
            $data['reportlist'][$i]->num = $i + 1;
            $i++;
        }
        $data['name'] = $name;
        //创建分页
        $config['base_url'] = 'index.php?C=Mobile_api&F=api&model=view&action=show_all_report&name='.$name;
        $config['total_rows'] = $data["total"];
        $config['per_page'] = $data["page_size"];
        $this->pagination->initialize($config);
        $this->load->view("mobile/report/user_report_list.php", $data);
    }

    private function do_one_report($get)
    {

        $loginInfo = $this->checkLogin();
        if( $loginInfo["rs"] != "ok"){
            $data["url"]="";
            $data["url_info"]="登录";
            $data["info"]="您尚未登录或者登录已过期!";
            $this->load->view("mobile/report/msg.php", $data);
            return;
        }else{
            $uid = $loginInfo["uid"];
        }
        $this->load->model("Report_Grant_Model","report_grant");
        $this->load->library('Formdesign');
        $this->load->library('Form_Parse_Tool');
        $this->load->model("Report_Model","report_model");
        $this->load->model("Report_Record_Model","report_record");
        $report_id = isset($get["id"]) ? $get["id"] : "";
        if ($report_id == "") {
            $this->rs["status"] = "error";
            $this->rs["msg"] = "empty id";
            return;
        }
        $data['report_id'] = $report_id;
        $report = $this->report_model->findId($report_id)['data'];
        $this->load->model("Report_Template_Model","report_template_model");
        $report_template =  $this->report_template_model->get_one_report_template($report->_id);
        $column_data = "formdesign_data";
        $column_parse = "formdesign_parse";
        $column_id = "id";
        $column_name = "name";
        $design_content = $this->formdesign->unparse_form(array(
            'content_parse'=>$report->$column_parse,
            'content_data'=>$report->$column_data
        ),array(),array('action'=>'preview'));
        $data['id'] = $report->$column_id;
        $data['name'] = $report->$column_name;
        $data['content'] = $design_content;
        $data['report_type']=json_encode($report_template);//

        $this->load->view("mobile/report/pre_fill_report.php",$data);
    }

    //填报报表的时候，进行信息的收集
    public function commit_report($get, $post){
        $loginInfo = $this->checkLogin();
        if( $loginInfo["rs"] != "ok"){
            $data["url"]="";
            $data["url_info"]="登录";
            $data["info"]="您尚未登录或者登录已过期!";
            $this->load->view("mobile/report/msg.php", $data);
            return;
        }else{
            $id = $loginInfo["uid"];
        }
        $data = array();
       // $id = 113;
        $this->load->model("Report_Record_Model","record_model");
        $this->load->model("Report_Model","report_model");
        $this->load->library("Form_Parse_Tool");
        $this->load->model('Report_Grant_Model','report_grant');
        $this->load->model("User_Model", "user_model");
        $userInfo =  $this->user_model->findId($id)['data'];
        $gidStr = "group_id";
        $group_id = $userInfo->$gidStr;

        $data_report_template = $post["report_template"];
        $report_id = $post["report_id"];
        $report_data = $post['data'];
        //首先需要获取最后一次本人填充的时间
        $last_record_time = $this->record_model->get_last_record_time_by_userid_and_report_id($id,$report_id);
        $report_grant = $this->report_grant->find($report_id,$group_id)['data'][0];


        //报表分配权限开始时间
        $special_time_start = $report_grant->special_time_start;
        //报表分配权限结束时间
        $special_time_end = $report_grant->special_time_end;
        $this->load->library("My_time");
        if(time()>=$special_time_start&&time()<=$special_time_end){
            $time = $this->my_time->get_start_and_end_time($special_time_start,$report_grant->special_frequency);
            if($last_record_time>=$time[0]&&$last_record_time<$time[1]){
                $data["url"]="index.php?C=Mobile_api&F=api&model=view&action=show_all_report";
                $data["url_info"]="返回";
                $data["info"]= "该间隔内已经填过了！";
                $this->load->view("mobile/report/msg.php", $data);
                return;
            }
        }else {
            $time = $this->my_time->get_start_and_end_time($report_grant->update_time, $report_grant->frequency);
            if($last_record_time>=$time[0]&&$last_record_time<$time[1]){
               // echo json_encode(array("rs"=>"error","msg"=>"该报表的填充间隔是".$report_grant->frequency.",本次时间间隔内已经填充过了，上次填充时间".date('Y-m-d H:i:s',(string)$last_record_time)."下次可以在".date('Y-m-d H:i:s',(string)$time[1])."填充"));
                $data["url"]="index.php?C=Mobile_api&F=api&model=view&action=show_all_report";
                $data["url_info"]="返回";
                $data["info"]= "该间隔内已经填过了！";
                $this->load->view("mobile/report/msg.php", $data);
                return;
            }
        }
//        unset($_POST["report_id"]);
//        unset($_POST["report_type"]);
        $data = array();
        //获取一个报表的模板，判断对应的表单name的类型
        $report = $this->report_model->findId($report_id)["data"];
        if($report == null){
            $data["url"]="index.php?C=Mobile_api&F=api&model=view&action=show_all_report";
            $data["url_info"]="返回";
            $data["info"]="报表错误!";
            $this->load->view("mobile/report/msg.php", $data);
            return;
        }
//        $data_type = $this->form_parse_tool->get_data_type(unserialize($report->formdesign_data));
        $this->form_parse_tool->change_report_data_type($report_data,$data_report_template);
        $GPS = $post['GPS'];
        $IMEI = $post['IMEI'];
        $rs = $this->record_model->add($report_id,$report_data,array('time'=>time(),'GPS'=>$GPS,'IP'=>$_SERVER['SERVER_ADDR'],'UA'=>$_SERVER['HTTP_USER_AGENT'] ,'IMEI'=>$IMEI,'uid'=>(int)$id,'from'=>'moblie'));

        $report_id = $post["report_id"];
//        unset($post["report_id"]);
//

//        //获取一个报表的模板，判断对应的表单name的类型
//        $report = $this->report_model->findId($report_id)["data"];
//        if($report == null){
//            $data["url"]="index.php?C=Mobile_api&F=api&model=view&action=show_all_report";
//            $data["url_info"]="返回";
//            $data["info"]="报表错误!";
//            $this->load->view("mobile/report/msg.php", $data);
//            return;
//        }
//        $data_type = $this->form_parse_tool->get_data_type(unserialize($report->formdesign_data));
//        $this->form_parse_tool->change_report_data_type($post,$data_type);
//        $GPS = $post['GPS'];
//        $IMEI = $post['IMEI'];
//        $rs = $this->record_model->add($report_id,$post,array('time'=>time(),'GPS'=>$GPS,'ip'=>$_SERVER['SERVER_ADDR'],'UA'=>$_SERVER['HTTP_USER_AGENT'] ,'IMEI'=>$IMEI,'uid'=>(int)$id,'from'=>'mobile'));

        if($rs){
            $data["url"]="index.php?C=Mobile_api&F=api&model=view&action=show_all_report";
            $data["url_info"]="返回";
            $data["info"]="填写成功!";
            $this->load->view("mobile/report/msg.php", $data);
            return;
        }else{
            $data["url"]="index.php?C=Mobile_api&F=api&model=view&action=show_all_report";
            $data["url_info"]="返回";
            $data["info"]="提交失败!";
            $this->load->view("mobile/report/msg.php", $data);
        }
    }



    private function view($action, $get, $post)
    {
        if ($action == "show_all_report") {
            $this->show_all_report($post);
        } elseif ($action == "do_one_report") {
            $this->do_one_report($get);
        } elseif ($action == "statistics_report") {
            $this->load->view("mobile/report/statistics_report");
        } elseif($action = "commit_report"){
            $this->commit_report($get, $post);
        }
    }


//-------------------------------------------
    /**
     *
     * 处理用户组的相关的请求
     *
     * @param $action 请求的方法
     * @param $get 请求的GET的参数
     * @param $post 请求的POST的参数
     */
    private
    function ugroup($action, $get, $post)
    {
        $this->rs = array();
    }


    /**
     *
     * 处理acl访问控制相关的请求
     *
     * @param $action 请求的方法
     * @param $get 请求的GET的参数
     * @param $post 请求的POST的参数
     */
    private
    function acl($action, $get, $post)
    {
        $this->rs = array();
    }

    /**
     *
     * 处理权限部分的相关的请求
     *
     * @param $action 请求的方法
     * @param $get 请求的GET的参数
     * @param $post 请求的POST的参数
     */
    private
    function permission($action, $get, $post)
    {
        $this->rs = array();
    }

    /**
     *
     * 处理报表相关的内容的请求
     *
     * @param $action 请求的方法
     * @param $get 请求的GET的参数
     * @param $post 请求的POST的参数
     */
    private
    function report($action, $get, $post)
    {
        $this->rs = array();
    }

    /**
     *
     * 处理统计报表相关的部分的内容
     *
     * @param $action 请求的方法
     * @param $get 请求的GET的参数
     * @param $post 请求的POST的参数
     */
    private
    function statistics($action, $get, $post)
    {
        $this->rs = array();
    }

    /**
     *
     * 处理测试请求的部分，内部使用，仅用来测试，比如测试登录情况，测试错误的内部信息
     *
     * @param $action 请求的方法
     * @param $get 请求的GET的参数
     * @param $post 请求的POST的参数
     */
    private
    function test($action, $get, $post)
    {
        $this->rs = array();
    }

}