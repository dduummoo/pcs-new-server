<?php

/**
 * 曹原野的私有请求
 * Created by PhpStorm.
 * User: cyy
 * Date: 2016/8/2
 * Time: 20:55
 */
class Cli extends CI_Controller{


    public function test_user(){
        $this->load->model("User_Model","user");
        $rs = $this->user->find(null,0,0);

        print_r($rs);
    }

    public function test_group(){
        $this->load->model("Group_Model","group");

        $rs = $this->group->get_path_for_id(32);

        print_r($rs);

        $rs2 = $this->group->level_groups();

        print_r($rs2);
    }


    public function test_widget(){
        $this->load->model("Pc_Widget_Model","widget");
        $this->load->model("Report_Template_Model","report");

        $one_report = $this->report->get_visible_by_id(20);
        print_r($one_report);
        $this->widget->init($one_report);

        $data["css_js"] = $this->widget->get_js_css_include();

        $data["widget"] = $this->widget->get_widget_html();

        $data["init_js"] = $this->widget->get_init_widget_js();

        $this->load->view("report/do_one_report",$data);


    }
}