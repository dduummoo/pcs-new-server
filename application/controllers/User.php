<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2016/7/29
 * Time: 15:58
 */
class User extends CI_Controller
{
    public function __construct()
    {
        $this->need_login = true;
        parent::__construct();
        $this->load->model('User_Model',"user_model");
        $this->load->model('Group_Model',"group_model");
        $this->load->helper('url_helper');
        $this->load->library('System_security');
        $this->load->library('session');

//        $this->check_user_login();
    }

    public function get_image_and_str(){
//        header("Access-Control-Allow-Origin: *");
//        $this->load->model("Vcode","vcode");
//        $code = $this->vcode;
//        $_SESSION['code']=$code->getcode();
//      return $code->outimg();
        $string = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $str = "";
        for($i=0;$i<6;$i++){
            $pos = rand(0,61);
            $str .= $string{$pos};
        }
//session_start();
        $_SESSION['code'] = $str;
        $img_handle = Imagecreate(80, 30);  //图片大小80X20
        $back_color = ImageColorAllocate($img_handle, 255, 255, 255); //背景颜色（白色）
        $txt_color = ImageColorAllocate($img_handle, 0,0, 0);  //文本颜色（黑色）

        //加入干扰线
        for($i=0;$i<3;$i++)
        {
            $line = ImageColorAllocate($img_handle,rand(0,255),rand(0,255),rand(0,255));
            Imageline($img_handle, rand(0,15), rand(0,15), rand(100,150),rand(10,50), $line);
        }
        //加入干扰象素
        for($i=0;$i<200;$i++)
        {
            $randcolor = ImageColorallocate($img_handle,rand(0,255),rand(0,255),rand(0,255));
            Imagesetpixel($img_handle, rand()%100 , rand()%50 , $randcolor);
        }
        Imagefill($img_handle, 0, 15, $back_color);             //填充图片背景色
        ImageString($img_handle, 38, 10, 7, $str, $txt_color);//水平填充一行字符串第三个参数表示字符离左边框的距离
        ob_clean();   // ob_clean()清空输出缓存区
        header("Content-type: image/png"); //生成验证码图片
       return Imagepng($img_handle);//显示图片

//        print_r($this->vcode->outimg());
//        return;
//       echo json_encode(array("obj"=> $this->vcode->createcode()));
//        echo json_encode(array("img"=>"6666"));
    }

    public function login(){
        unset(
            $_SESSION['uid'],
            $_SESSION['ugroupid'],
            $_SESSION['suid']
        );
//        $_SESSION["sss"]="aaaaa";
        $this->load->helper('form');
        $this->load->library('form_validation');

        $r1 = $this->group_model->level_groups();
        $i = 0;
        foreach ($r1 as $row) {
            $data['ugrouplist'][$i]['id'] = $row["id"];
            $data['ugrouplist'][$i]['name'] = $row["name"];
            $i++;
        }
        $this->form_validation->set_rules('ugroupid', 'ugroupid', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');


        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('user/login',$data);
        }
        else
        {
            $verifycode = $this->input->post('code');
            $code=$_SESSION['code'];
            if(strtolower($verifycode) !=strtolower($code)){
                $code=$_SESSION['code'];
//                echo "<script>alert($verifycode);alert($code;)</script>";
                echo "<script>alert('验证码错误！');
                location.href='index.php?C=User&F=login';</script>";
                return;
            }
            $username = $this->input->post('username');
            $password_f = $this->input->post('password');
            $password = $this->system_security->hash_password($username,$password_f);
            $ugroupid = $this->input->post('ugroupid');
            $r = $this->user_model->login($username,$password,$ugroupid);
            if($r['rs'] === "success")  {
                $uid = $r["data"]->id;
                $ugroupid =  $r["data"]->group_id;
//                print_r($r["data"]);
//                $data['datalist'] = $this->user_model->find($username,$ugroupid,0)['data'];
//                foreach ($data['datalist'] as $row1) {
//                    $uid = $row1->id;
//                }
//                print_r($uid);
                $_SESSION['uid'] = $uid;
                $_SESSION['ugroupid'] = $ugroupid;
                $_SESSION['test'] = 'test';
                $data['info']['uname'] = $username;
                $data['info']['uid'] = $uid;
                $r['data']=json_encode($data);
   
//                $this->load->view('user/login_success');
                $this->load->view('user/success',$data);
            }
            else {
        echo "<script>alert('用户名或者密码或者用户分组错误！');
        location.href='index.php?C=User&F=login';</script>";
            };
        }

    }
   
   

    public function ulist(){
        $this->check_login("suser");
//        if(!isset($_SESSION["suid"])){
//            echo "您尚未登录或者登录已过期";
//            return;
//        }
//        $this->load->library('tablecount'); //导入数据库表自定义类
//        $total_data = $this->tablecount->get_tablecount('superuser', $this->where); //总条数
        $pageNum = isset($_POST['pageNum']) ? $_POST['pageNum'] : 1;
        $numperpage = isset($_POST['numPerPage']) ? $_POST['numPerPage'] : 20;
        $this->user_model->change_page_size($numperpage);
//        $pageinfo = page($total_data, $pageNum); //计算分页总数和分页条件
//        $this->load->library('tablelist'); //导入数据库表自定义类
//        $this->limit = $pageinfo['limit'];
//        $data = $pageinfo['pageinfo'];
        $data['datalist'] = $this->user_model->find('','',$pageNum-1)['data'];
        //print_r($this->user_model->find('','',0)['data']);
        //$r1 = $this->group_model->get_path_for_id();
        $data['info']['total'] = $this->user_model->find('','',$pageNum-1)['total'];
        $data['info']['page_size'] = $this->user_model->find('','',$pageNum-1)['page_size'];
        //echo $pageNum;
        $data['info']['current'] = $pageNum;
        $data['info']['numperpage'] = $numperpage;
        $data['info']['uname'] = '';
        $data['info']['ugroupid'] = '';
        $i = 0;
//        print_r($data['datalist']);
        foreach ($data['datalist'] as $row1) {
            $id = $row1->group_id;
            $row = $this->group_model->get_path_for_id($id);
            $a = '';
            foreach($row as $row){
                $a = $a."->".$row['name'];
            }
            $data['datalist'][$i]->group_name = substr($a,2);
            $data['datalist'][$i]->uid = $row1->id;
            $data['datalist'][$i]->id = $i+1;
            $i++;
        }
        $r1 = $this->group_model->level_groups();
        $i = 0;
        foreach ($r1 as $row) {
            $data['ugrouplist'][$i]['id'] = $row["id"];
            $data['ugrouplist'][$i]['name'] = $row["name"];
            $i++;
        }

        $this->load->view('center/user/userlist', $data);
    }


    public function uadd_view(){
//        if(!isset($_SESSION["suid"])){
//            echo "您尚未登录或者登录已过期";
//            return;
//        }
        $this->check_login("suser");
        $r1 = $this->group_model->level_groups();
        $i = 0;
        foreach ($r1 as $row) {
            $data['ugrouplist'][$i]['id'] = $row["id"];
            $data['ugrouplist'][$i]['name'] = $row["name"];
            $i++;
        }

        $this->load->view('center/user/useradd', $data);
    }
    public function uadd(){
        $this->check_login("suser");
        $name = $this->input->get_post("username");
        $pwd_f = $this->input->get_post("password");
        $pwd = $this->system_security->hash_password($name,$pwd_f);

        $ugroupid = $this->input->get_post("ugroupid");
        $r = $this->user_model->add($name,$pwd,$ugroupid);
        $re = $r['rs'];
        if ($re == "success") {
            $this->return['statusCode'] = '200';
            $this->return['message'] = '操作成功';
            $this->return['navTabId'] = 'showUserlist';
            $this->return['callbackType'] = 'closeCurrent';


        } else {
            $this->return['statusCode'] = '300';
            $this->return['message'] = '操作失败,'.$r['msg']."。";
        }
        echo json_encode($this->return);
    }

    public function udel(){
        $this->check_login("suser");
        $uid = $this->input->get_post("uid");
        $re = $this->user_model->delete($uid)['rs'];
        if ($re == "success") {
            $this->return['statusCode'] = '200';
            $this->return['message'] = '操作成功';
            $this->return['navTabId'] = '';
            $this->return['callbackType'] = '';


        } else {
            $this->return['statusCode'] = '300';
            $this->return['message'] = '操作失败';
        }
        echo json_encode($this->return);
    }

    public function uedit_view(){
        $this->check_login("suser");
        $r1 = $this->group_model->level_groups();
        $uid = $this->input->get_post("uid");
        $data['info']['id'] = $uid;
        $r = $this->user_model->findId($uid)['data'];
        $data['info']['name'] = $r->username;
        $data['info']['pwd'] = $r->password;
        $data['info']['groupid'] = $r->group_id;
        $i = 0;
        foreach ($r1 as $row) {
            $data['ugrouplist'][$i]['id'] = $row["id"];
            $data['ugrouplist'][$i]['name'] = $row["name"];
            if($data['ugrouplist'][$i]['id']==$r->group_id)$data['info']['groupname'] = $row["name"];
            $i++;
        }
        $this->load->view('center/user/useredit',$data);
    }
    public function uedit(){
//        if(!isset($_SESSION["suid"])){
//            echo "您尚未登录或者登录已过期";
//            return;
//        }
        $this->check_login("suser");
        $name = $this->input->get_post("username");
        $pwd_f = $this->input->get_post("password");
        $uid = $this->input->get_post("id");
        $groupid = $this->input->get_post("ugroupid");
        $array1 = array('username', 'password', 'group_id');
        $array2 = array('username', 'group_id');
        if($pwd_f=='') {
            $array3 = array($name,$groupid);
            $r = $this->user_model->update($uid, $array2, $array3);
        }
        else{
            $pwd = $this->system_security->hash_password($name,$pwd_f);
            $array4 = array($name,$pwd,$groupid);
            $r = $this->user_model->update($uid, $array1, $array4);
        }
        if ($r['rs'] == "success") {
            $this->return['statusCode'] = '200';
            $this->return['message'] = '操作成功';
            $this->return['navTabId'] = 'showUserlist';
            $this->return['callbackType'] = 'closeCurrent';


        } else {
            $this->return['statusCode'] = '300';
            $this->return['message'] = '操作失败,'.$r['msg'];
        }
        echo json_encode($this->return);
    }

    public function ufind(){
        $this->check_login("suser");
//        if(!isset($_SESSION["suid"])){
//            echo "您尚未登录或者登录已过期";
//            return;
//        }
        $pageNum = isset($_POST['pageNum']) ? $_POST['pageNum'] : 1;
        $numperpage = isset($_POST['numPerPage']) ? $_POST['numPerPage'] : 20;
        $this->user_model->change_page_size($numperpage);

        $data['info']['current'] = $pageNum;
        $data['info']['numperpage'] = $numperpage;
        $name = $this->input->get_post("username");
        $ugroupid = $this->input->get_post("ugroupid");
        $data['info']['uname'] = $name;
        $data['info']['ugroupid'] = $ugroupid;
        if($ugroupid!=0) {
            $data['datalist'] = $this->user_model->find($name,$ugroupid,$pageNum-1)['data'];
            $data['info']['total'] = $this->user_model->find($name,$ugroupid,$pageNum-1)['total'];
            $data['info']['page_size'] = $this->user_model->find($name,$ugroupid,$pageNum-1)['page_size'];
        }
        else {
            $data['datalist'] = $this->user_model->find($name,'',$pageNum-1)['data'];
            $data['info']['total'] = $this->user_model->find($name,'',$pageNum-1)['total'];
            $data['info']['page_size'] = $this->user_model->find($name,'',$pageNum-1)['page_size'];
        }
        if($data['datalist']){
            $i = 0;
            foreach ($data['datalist'] as $row1) {
                $id = $row1->group_id;
                $row = $this->group_model->get_path_for_id($id);
                $a = '';
                if(is_array($row)) {
                    foreach ($row as $row) {
                            $a = $a . "->" . $row['name'];
                    }
                }
                $data['datalist'][$i]->username = $row1->username;
                $data['datalist'][$i]->group_name = substr($a,2);
                $data['datalist'][$i]->uid = $row1->id;
                $data['datalist'][$i]->id = $i+1;
                $i++;
            }
            $r1 = $this->group_model->level_groups();
            $i = 0;
            foreach ($r1 as $row) {
                $data['ugrouplist'][$i]['id'] = $row["id"];
                $data['ugrouplist'][$i]['name'] = $row["name"];
                $i++;
            }

        }
        $this->load->view('center/user/userlist', $data);
//        else{
//            echo '找不到该用户，请检查输入';
//        }
    }

    public function changepwd_view(){
        $this->check_login("user");
        $uid = $this->session->uid;
//        if(!isset($uid)){
//            echo "您尚未登录或者登录已经过期";
//            return;
//        }
        $data['info']['id'] = $uid;
        $r = $this->user_model->findId($uid)['data'];
        $data['info']['name'] = $r->username;
        $data['info']['pwd'] = $r->password;
        $data['info']['groupid'] = $r->group_id;
        $this->load->view('user/changepwd',$data);
    }
    public function changepwd(){
        $this->check_login("user");
        $uid = $this->input->get_post("id");
        $pwd_f = $this->input->get_post("password_f");//旧密码
        $pwd1 = $this->input->get_post("password1");//新密码
        $pwd2 = $this->input->get_post("password2");//确认新密码
        $r = $this->user_model->findId($uid)['data'];
        $pwd = $r->password;//数据库中原来的密码
        $name = $r->username;
        $pwd_f_hash = $this->system_security->hash_password($name,$pwd_f);
        if($pwd1==$pwd2){
            if($pwd_f_hash==$pwd){
                $pwd_n = $this->system_security->hash_password($name,$pwd1);
                $array0 = array("password");
                $array1 = array($pwd_n);
                $re = $this->user_model->update_pwd($uid, $array0, $array1)['rs'];
                if($re == 'success'){
                    $this->return['statusCode'] = '200';
                    $this->return['message'] = '操作成功';
                    $this->return['callbackType'] = 'closeCurrent';
                }
                else{
                    $this->return['statusCode'] = '300';
                    $this->return['message'] = '修改失败';
                }
            }
            else{
                $this->return['statusCode'] = '300';
                $this->return['message'] = '旧密码输入错误';
            }
        }
        else{
            $this->return['statusCode'] = '300';
            $this->return['message'] = '两次密码不一致';
        }
        echo json_encode($this->return);
    }

    public function userinfo_view(){
        $this->check_login("user");
        $uid = $this->session->uid;
        $data['info']['id'] = $uid;
        $r = $this->user_model->findId($uid)['data'];
        $data['info']['name'] = $r->username;
        $data['info']['pwd'] = $r->password;
        $gid = $r->group_id;
        $row = $this->group_model->get_path_for_id($gid);
        $a = '';
        foreach($row as $row){
            $a = $a."->".$row['name'];
        }
        $data['info']['group_name']= substr($a,2);
//        $i = 0;
//        foreach ($data['datalist'] as $row1) {
//            $id = $row1->group_id;
//            $row = $this->group_model->get_path_for_id($id);
//            $a = '';
//            foreach($row as $row){
//                $a = $a."->".$row['name'];
//            }
//            $data['datalist'][$i]->group_name = substr($a,2);
//            $i++;
//        }
        $this->load->view('user/userinfo',$data);
    }
    
}