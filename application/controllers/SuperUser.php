<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 2016/7/31
 * Time: 13:08
 */
class SuperUser extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('SuperUser_Model',"superuser_model");
        $this->load->helper('url_helper');
        $this->load->library('session');
        $this->load->library('System_security','system_security');
    }
    public function get_image_and_str(){
//        header("Access-Control-Allow-Origin: *");
//        $this->load->model("Vcode","vcode");
//        $code = $this->vcode;
//        $_SESSION['code']=$code->getcode();
//      return $code->outimg();
        $string = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $str = "";
        for($i=0;$i<6;$i++){
            $pos = rand(0,61);
            $str .= $string{$pos};
        }
//session_start();
        $_SESSION['code'] = $str;
        $img_handle = Imagecreate(80, 30);  //图片大小80X20
        $back_color = ImageColorAllocate($img_handle, 255, 255, 255); //背景颜色（白色）
        $txt_color = ImageColorAllocate($img_handle, 0,0, 0);  //文本颜色（黑色）

        //加入干扰线
        for($i=0;$i<3;$i++)
        {
            $line = ImageColorAllocate($img_handle,rand(0,255),rand(0,255),rand(0,255));
            Imageline($img_handle, rand(0,15), rand(0,15), rand(100,150),rand(10,50), $line);
        }
        //加入干扰象素
        for($i=0;$i<200;$i++)
        {
            $randcolor = ImageColorallocate($img_handle,rand(0,255),rand(0,255),rand(0,255));
            Imagesetpixel($img_handle, rand()%100 , rand()%50 , $randcolor);
        }
        Imagefill($img_handle, 0, 15, $back_color);             //填充图片背景色
        ImageString($img_handle, 38, 10, 7, $str, $txt_color);//水平填充一行字符串第三个参数表示字符离左边框的距离
        ob_clean();   // ob_clean()清空输出缓存区
        header("Content-type: image/png"); //生成验证码图片
        return Imagepng($img_handle);//显示图片

//        print_r($this->vcode->outimg());
//        return;
//       echo json_encode(array("obj"=> $this->vcode->createcode()));
//        echo json_encode(array("img"=>"6666"));
    }

    public function login(){

        $this->load->library('session');
        unset(
            $_SESSION['suid'],
            $_SESSION['uid'],
            $_SESSION['ugroupid']
        );

        $this->load->helper('form');
        $this->load->library('form_validation');

        $data['title'] = '登陆';

        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('superuser/login');

        }
        else
        {
            $verifycode = $this->input->post('code');
            $code=$_SESSION['code'];
            if(strtolower($verifycode) !=strtolower($code)){
                $code=$_SESSION['code'];
//                echo "<script>alert($verifycode);alert($code;)</script>";
                echo "<script>alert('验证码错误！');
                location.href='index.php?C=SuperUser&F=login';</script>";
                return;
            }
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $pwd = $this->system_security->hash_password($username,$password);
            if($this->superuser_model->login($username,$pwd)['rs'] === "success") {
                $_SESSION['suid'] = $this->superuser_model->login($username,$pwd)['data']->id;
                $data['info']['suid']=$_SESSION['suid'];
                $_SESSION['data']=$data;
//                $this->load->view('superuser/success',$data);
                $this->load->view('superuser/login_success');
            }
            else echo "<script>alert('用户名或者密码错误！');
                location.href='index.php?C=SuperUser&F=login';</script>";
        }
    }
    public function to_success(){
//        $data = $_GET['data'];
//        var_dump($_GET);
//       return;
        $this->load->view('superuser/success', $_SESSION['data']);

    }
    public function changepwd_view(){
//        $suid = $this->input->get_post("suid");
//        $data['info']['suid'] = $suid;
//        $superuser = $this->superuser_model->findId($suid);
//        $data['info']['name'] = $superuser->username;
//        $data['info']['pwd'] = $superuser->password;
        $this->load->view('superuser/changepwd');
    }
    public function changepwd(){
        if(!isset($_SESSION['suid'])){
            echo "您没有登录或者登陆已经过期";
            return ;
        }
        $suid = $this->session->suid;
        $old_pwd = $this->input->get_post("old_password");//旧密码
        $new_pwd = $this->input->get_post("new_password");//新密码
        $c_new_pwd = $this->input->get_post("c_new_password");//确认新密码
        $superuser = $this->superuser_model->findId((int)$suid);
        $pwd = $superuser->password;//数据库中原来的密码
        $name = $superuser->username;
        $hash_pwd = $this->system_security->hash_password($name,$old_pwd);
        if($new_pwd==$c_new_pwd){
            if($hash_pwd==$pwd){
                $new_hash_pwd = $this->system_security->hash_password($name,$new_pwd);
                $array0 = array("password");
                $array1 = array($new_hash_pwd);
                $re = $this->superuser_model->update($suid, $array0, $array1)['rs'];
                if($re == 'success'){
                    $this->return['statusCode'] = '200';
                    $this->return['message'] = '操作成功';
                    $this->return['callbackType'] = 'closeCurrent';
                }
                else{
                    $this->return['statusCode'] = '300';
                    $this->return['message'] = '修改失败';
                }
            }
            else{
                $this->return['statusCode'] = '300';
                $this->return['message'] = '旧密码输入错误';
            }
        }
        else{
            $this->return['statusCode'] = '300';
            $this->return['message'] = '两次密码不一致';
        }
        echo json_encode($this->return);
    }

}