<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
        parent::__construct();
        $this->load->model("User_Model","user");
    }

    public function index(){
		$this->load->view('welcome_message');
	}

	public function hello(){
	    echo "hehe";
    }

    public function insert(){
        $this->user->insert();
    }

    public function get(){
        $data = $this->user->get();
//        print_r($data->result());
    }

    public function test_redis(){
        $this->load->driver('cache',array('adapter' => 'redis'));
        $this->cache->save("name","caoyuanye");
//        print_r('aa'.$this->cache->get("name"));
        echo "done";
    }

    public function test_mongo(){
        $this->load->library("mongo_db");

//        $rs = $this->mongo_db->insert("people",array(
//            "name" => "cao",
//            "age" => 18,
//            'address'=>'nanjing'
//        ));
        //var_dump( $rs->{'$id'});
//        $iii = "579dd9aba3eea04c10000031";
//
//        $get = $this->mongo_db->where(array("_id"=>new MongoID($iii)))->get("people");

//        $get2 = $this->mongo_db->select(array("name","age"))->get("people");
//        print_r($get);
//        print_r($get2);
        $rs = $this->mongo_db->in("people",array(
            "name" => "haha",
            "age" => 18,
            'address'=>'nanjing'
        ));
        var_dump( $rs);
        var_dump( $rs->{'$id'});
    }
}
