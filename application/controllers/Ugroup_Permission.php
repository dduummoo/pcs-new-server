<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 2016/8/12
 * Time: 23:42
 * @property ugroup_permission_model $ugroup_permission_model
 */
class Ugroup_Permission extends CI_Controller
{
    public function __construct()
    {
        $this->need_login = true;
        parent::__construct();
        $this->load->model('Group_Model','group_model');
        $this->load->model('Ugroup_Permission_Model','ugroup_permission_model');
        $this->load->model('Monitor_Grant_Model','monitor_grant_model');
        $this->load->model('Report_Grant_Model','report_grant_model');
        $this->load->model('Report_Model','report_model');
        $this->load->helper('url_helper');
        $this->load->library('session');
    }
    public function ugplist(){
        $this->check_login("suser");
        $r1 = $this->group_model->level_groups();
        $i = 0;
        foreach ($r1 as $row) {
            $data['ugrouplist'][$i]['gid'] = $row["id"];
            $data['ugrouplist'][$i]['name'] = $row["name"];
            $data['ugrouplist'][$i]['id'] = $i+1;
            $i++;
        }
        $this->load->view('center/ugroup_permission/ugplist', $data);
    }
    public function ugpedit_view(){
        $this->check_login("suser");
        $gid = $this->input->get_post('gid');
        $data['info']['gname'] = $this->group_model->find_by_id($gid)['data']->name;
        $data['info']['gid'] = $gid;
        $r = $this->ugroup_permission_model->get_ugroup_permission($gid);
        $i = 0;
        $data['u'] = array("0","0","0");
        foreach ($r as $row) {
            $data['ugplist'][$i]['pid'] = $row->id;
            $data['u'][$row->id-1] = $row->id;
            //$data['ugplist'][$i]['txt'] = $row->txt;
            //$data['ugplist'][$i]['id'] = $i+1;
            $i++;
        }
        $i = 0;
        foreach ($r as $key => $value){
            $data['id'][$i] = $key;
            $i++;
        }
        //print_r($data['id']);

        $this->load->view('center/ugroup_permission/ugpedit', $data);
    }

    public function ugpedit(){
        $this->check_login("suser");
//        $d = array();
//        $d[1] = $this->input->get_post("dbid0");
//        $d[2] = $this->input->get_post("dbid1");
//        $d[3] = $this->input->get_post("dbid2");
//        print_r($d);
        $c = array();
        $c[1] = $this->input->get_post("c1");
        $c[2] = $this->input->get_post("c2");
        $c[3] = $this->input->get_post("c3");
        //被选中（1，2，3）；没被选中（0，0，0）
        for($i = 1;$i<4;$i++){
            if($c[$i]=='')$c[$i]=0;
        }
        //print_r($c);
        $gid = $this->input->get_post("id");

        $u = array();
        $u[1] = $this->input->get_post("u1");
        $u[2] = $this->input->get_post("u2");
        $u[3] = $this->input->get_post("u3");//c是新的，u是旧的
       //print_r($u);

        for($i = 1;$i<4;$i++){
            if($c[$i]!=$u[$i]){
                if ($u[$i]==0){
                    $radd = $this->ugroup_permission_model->add($gid,$c[$i])['rs'];
                }
                else{
                    $r = $this->ugroup_permission_model->get_ugroup_permission($gid);
                    foreach ($r as $key => $value){
                        if($r[$key]->id == $i) $rdel = $this->ugroup_permission_model->delete($key)['rs'];
                    }
                }

            }
        }
        if($radd = 'success'&& $rdel = 'success'){
            $this->return['statusCode'] = '200';
            $this->return['message'] = '操作成功';
            $this->return['navTabId'] = 'showUserGroupPermissionlist';
            $this->return['callbackType'] = 'closeCurrent';
        }
        else{
            $this->return['statusCode'] = '300';
            $this->return['message'] = '操作失败';
        }
        echo json_encode($this->return);
    }

    public function ugmgedit_view(){
        $this->check_login("suser");
        $gid = $this->input->get_post('gid');
        $data['info']['gname'] = $this->group_model->find_by_id($gid)['data']->name;
        $data['info']['mid'] = $gid;
        $r = $this->group_model->level_groups();
        $r = $this->group_model->level_groups();
        $i = 0;
        foreach ($r as $row) {
            $data['ugrouplist'][$i]['gid'] = $row["id"];
            $data['ugrouplist'][$i]['name'] = $row["name"];
            $data['ugrouplist'][$i]['id'] = $i+1;
            $i++;
        }

        $r1 = $this->monitor_grant_model->find_by_monitor_group_id($gid);
        $i = 0;
        $data['monlist']=Array();
        foreach ($r1 as $row) {
            $data['monlist'][$i]['id'] = $row->monited_ugroup_id;

            $data['monlist'][$i]['dbid'] = $row->id;
            //$this->group_model->find_by_id($row->monited_ugroup_id)['data']->name;
            $data['monlist'][$i]['name'] = $this->group_model->find_by_id($row->monited_ugroup_id)['data']->name;
            $i++;
        }

        $this->load->view('center/ugroup_permission/ugmgedit', $data);
    }

    public function ugmgedit_add(){
        $this->check_login("suser");
        $mongid = $this->input->get_post('mongid');
        $mid = $this->input->get_post('mid');
        $r = $this->monitor_grant_model->add($mid,$mongid)['rs'];
        if($r == 'success'){
            $this->return['statusCode'] = '200';
            $this->return['message'] = '操作成功';
            $this->return['navTabId'] = 'showUserGroupPermissionlist';
            $this->return['callbackType'] = 'closeCurrent';
        }
        else{
            $this->return['statusCode'] = '300';
            $this->return['message'] = '操作失败,这个分组已经被监管';
        }
        echo json_encode($this->return);
    }

    public function ugmgedit_delete(){
        $this->check_login("suser");
        $dbid = $this->input->get_post("dbid");
        $re = $this->monitor_grant_model->delete($dbid)['rs'];
        if ($re == "success") {
            $this->return['statusCode'] = '200';
            $this->return['message'] = '操作成功';
            $this->return['navTabId'] = 'showUserGroupPermissionlist';
            //$this->return['dialogId'] = 'showmonitoredit';
        } else {
            $this->return['statusCode'] = '300';
            $this->return['message'] = '操作失败';
        }
        echo json_encode($this->return);
    }

    public function rglist(){
        $this->check_login("suser");
        $pageNum = isset($_POST['pageNum']) ? $_POST['pageNum'] : 1;
        $numperpage = isset($_POST['numPerPage']) ? $_POST['numPerPage'] : 20;
        $this->report_grant_model->change_page_size($numperpage);
        $rs = $this->report_grant_model->find_by_report_id_and_ugroup_id(0,0,$pageNum-1);
        $r1 = $rs['data'];
//        print_r($r1);
        $i = 0;
        foreach ($r1 as $row) {
            $data['rglist'][$i]['id'] = $i+1;
            $data['rglist'][$i]['dbid'] = $row->id;
            $report_name = $this->report_model->findId($row->report_id)['data']->name;
            $data['rglist'][$i]['report_name'] = $report_name;
            $ugroup_name = $this->group_model->find_by_id($row->ugroup_id)['data']->name;
            $data['rglist'][$i]['ugroup_name'] = $ugroup_name;
//            $data['rglist'][$i]['create_time'] = date('Y-m-d-H:i:s', $row->create_time);
//            $data['rglist'][$i]['update_time'] = date('Y-m-d-H:i:s', $row->update_time);
//            $data['rglist'][$i]['creator_id'] = $row->creator_id;
//            $data['rglist'][$i]['updator_id'] = $row->updator_id;
            $data['rglist'][$i]['frequency'] = $row->frequency;
            $data['rglist'][$i]['is_special_enable'] = $row->is_special_enable;
            $data['rglist'][$i]['special_time_start'] = date('Y-m-d-H:i:s',$row->special_time_start);
            $data['rglist'][$i]['special_time_end'] = date('Y-m-d-H:i:s',$row->special_time_end);
            $data['rglist'][$i]['special_frequency'] = $row->special_frequency;
            $data['rglist'][$i]['is_enable'] = $row->is_enable;
            $i++;
        }
        $r0 = $this->report_model->find()['data'];
        $i = 0;
        foreach ($r0 as $row) {
            $data['reportlist'][$i]['id'] = $row->id;
            $data['reportlist'][$i]['name'] = $row->name;
            $i++;
        }

        $r1 = $this->group_model->level_groups();
        $i = 0;
        foreach ($r1 as $row) {
            $data['ugrouplist'][$i]['id'] = $row["id"];
            $data['ugrouplist'][$i]['name'] = $row["name"];
            $i++;
        }
        $data['info']['total'] = $rs['total'];
        $data['info']['page_size'] =$rs['pagesize'];
        $data['info']['current'] = $rs['pagenum']+1;
        $this->load->view('center/ugroup_permission/report_grant_list', $data);
    }

    public function rgadd_view(){
        $this->check_login("suser");
        $r0 = $this->report_model->find()['data'];
        $i = 0;
        foreach ($r0 as $row) {
            $data['reportlist'][$i]['id'] = $row->id;
            $data['reportlist'][$i]['name'] = $row->name;
            $i++;
        }

        $r1 = $this->group_model->level_groups();
        $i = 0;
        foreach ($r1 as $row) {
            $data['ugrouplist'][$i]['id'] = $row["id"];
            $data['ugrouplist'][$i]['name'] = $row["name"];
            $i++;
        }

        $this->load->view('center/ugroup_permission/report_grant_add', $data);
    }

    public function rgadd(){
        $this->check_login("suser");
        $reportid = $this->input->get_post("report_id");
        $ugroupid = $this->input->get_post("ugroupid");
        if($this->report_grant_model->find($reportid,$ugroupid)['rs']=="success"){
            $this->return['statusCode'] = '300';
            $this->return['message'] = '操作失败,这个报表已经被分配过改用户分组';
            echo json_encode($this->return);
            return;
        }
        $frequency = $this->input->get_post("frequency");
        $sp = $this->input->get_post("sp");
        if($sp!=1)$sp=0;
//        echo $this->input->get_post("date_start");
//        echo $this->input->get_post("date_end");
        $date_start = strtotime($this->input->get_post("date_start"));
        $date_end = strtotime($this->input->get_post("date_end"));
        $s_frequency = $this->input->get_post("s_frequency");
        $suid = $this->input->get_post('suid');
        $stop = $this->input->get_post("stop");
        if($frequency==0||(!is_numeric($frequency))||floatval($frequency)<0) {
            $this->return['statusCode'] = '300';
            $this->return['message'] = '操作失败,填写频率必须为大于0的数字';
            echo json_encode($this->return);
            return;
        }
        if($s_frequency==0||(!is_numeric($s_frequency))||floatval($s_frequency)<0) {
            $this->return['statusCode'] = '300';
            $this->return['message'] = '操作失败,敏感期频率必须为大于0的数字';
            echo json_encode($this->return);
            return;
        }
        if($stop!=1)$stop=0;
        $re = $this->report_grant_model->add($reportid,$ugroupid,$suid,'0',$frequency*3600,$date_start,$date_end,$s_frequency*3600,$sp,$stop)['rs'];
        if ($re == "success") {
            $this->return['statusCode'] = '200';
            $this->return['message'] = '操作成功';
            $this->return['navTabId'] = 'showReportGrantlist';
            $this->return['callbackType'] = 'closeCurrent';


        } else {
            $this->return['statusCode'] = '300';
            $this->return['message'] = '操作失败';
        }
        echo json_encode($this->return);
    }

    public function rgdel(){
        $this->check_login("suser");
        $dbid = $this->input->get_post("dbid");
        $re = $this->report_grant_model->delete($dbid)['rs'];
        if ($re == "success") {
            $this->return['statusCode'] = '200';
            $this->return['message'] = '操作成功';
            $this->return['navTabId'] = 'showReportGrantlist';


        } else {
            $this->return['statusCode'] = '300';
            $this->return['message'] = '操作失败';
        }
        echo json_encode($this->return);
    }

    public function rgedit_view(){
        $this->check_login("suser");
        $dbid = $this->input->get_post("dbid");
        $re = $this->report_grant_model->findId($dbid)['data'];
        //print_r($re);
        $data['info']['dbid'] = $dbid;
        $data['info']['reportid'] = $re->report_id;
        $data['info']['ugroupid'] = $re->ugroup_id;
        $data['info']['frequency'] = $re->frequency;
        $data['info']['sp'] = $re->is_special_enable;
        $data['info']['date_start'] = $re->special_time_start;
        $data['info']['date_end'] = $re->special_time_end;
        $data['info']['s_frequency'] = $re->special_frequency;
        $data['info']['stop'] = $re->is_enable;
        $r0 = $this->report_model->find()['data'];
        $i = 0;
        foreach ($r0 as $row) {
            $data['reportlist'][$i]['id'] = $row->id;
            $data['reportlist'][$i]['name'] = $row->name;
            $i++;
        }

        $r1 = $this->group_model->level_groups();
        $i = 0;
        foreach ($r1 as $row) {
            $data['ugrouplist'][$i]['id'] = $row["id"];
            $data['ugrouplist'][$i]['name'] = $row["name"];
            $i++;
        }

        $this->load->view('center/ugroup_permission/report_grant_edit', $data);
    }

    public function rgedit(){
        $this->check_login("suser");
        $dbid = $this->input->get_post("dbid");
        $reportid = $this->input->get_post("report_id");
        $ugroupid = $this->input->get_post("ugroupid");
        $frequency = $this->input->get_post("frequency");
        $sp = $this->input->get_post("sp");
        if($sp!=1)$sp=0;
        $date_start = strtotime($this->input->get_post("date_start"));
        $date_end = strtotime($this->input->get_post("date_end"));
        $s_frequency = $this->input->get_post("s_frequency");
        $suid = $this->input->get_post('suid');
        $stop = $this->input->get_post("stop");
        if($stop!=1)$stop=0;
        if($frequency==0||(!is_numeric($frequency))||floatval($frequency)<0) {
            $this->return['statusCode'] = '300';
            $this->return['message'] = '操作失败,填写频率必须为大于0的数字';
            echo json_encode($this->return);
            return;
        }
        if($s_frequency==0||(!is_numeric($s_frequency))||floatval($s_frequency)<0) {
            $this->return['statusCode'] = '300';
            $this->return['message'] = '操作失败,敏感期频率必须为大于0的数字';
            echo json_encode($this->return);
            return;
        }
        $array1 = array($reportid,$ugroupid,time(),$suid,$frequency*3600,$sp,$date_start,$date_end,$s_frequency*3600,$stop);
        $array2 = array('report_id','ugroup_id','update_time','updator_id','frequency','is_special_enable','special_time_start','special_time_end','special_frequency','is_enable');
        $re = $this->report_grant_model->update($dbid,$array2,$array1)['rs'];
        if ($re == "success") {
            $this->return['statusCode'] = '200';
            $this->return['message'] = '操作成功';
            $this->return['navTabId'] = 'showReportGrantlist';
            $this->return['callbackType'] = 'closeCurrent';


        } else {
            $this->return['statusCode'] = '300';
            $this->return['message'] = '操作失败';
        }
        echo json_encode($this->return);
    }
    public function rgfind(){
        $this->check_login("suser");
        $reportid = $this->input->get_post("report_id");
        $ugroupid = $this->input->get_post("ugroupid");
        if($reportid =="" ||$reportid == null){
            $reportid =0;
        }else{
            $reportid = (int)$reportid;
        }
        if($ugroupid == ""||$ugroupid== null){
            $ugroupid =0;
        }else{
            $ugroupid = (int)$ugroupid;
        }
        $pageNum = isset($_POST['pageNum']) ? $_POST['pageNum'] : 1;
        $rs = $this->report_grant_model->find_by_report_id_and_ugroup_id($reportid,$ugroupid,$pageNum-1);
        $r1 = $rs["data"];
        $i = 0;
        if(count($r1)>0) {
            foreach ($r1 as $row) {
                $data['rglist'][$i]['id'] = $i + 1;
                $data['rglist'][$i]['dbid'] = $row->id;
                $report_name = $this->report_model->findId($row->report_id)['data']->name;
                $data['rglist'][$i]['report_name'] = $report_name;
                $ugroup_name = $this->group_model->find_by_id($row->ugroup_id)['data']->name;
                $data['rglist'][$i]['ugroup_name'] = $ugroup_name;
                $data['rglist'][$i]['frequency'] = $row->frequency;
                $data['rglist'][$i]['is_special_enable'] = $row->is_special_enable;
                $data['rglist'][$i]['special_time_start'] = date('Y-m-d-H:i:s', $row->special_time_start);
                $data['rglist'][$i]['special_time_end'] = date('Y-m-d-H:i:s', $row->special_time_end);
                $data['rglist'][$i]['special_frequency'] = $row->special_frequency;
                $data['rglist'][$i]['is_enable'] = $row->is_enable;
                $i++;
            }
        }else{
            $data['rglist'] = array();
        }
            $r0 = $this->report_model->find()['data'];
            $i = 0;
            foreach ($r0 as $row) {
                $data['reportlist'][$i]['id'] = $row->id;
                $data['reportlist'][$i]['name'] = $row->name;
                $i++;
            }

            $r1 = $this->group_model->level_groups();
            $i = 0;
            foreach ($r1 as $row) {
                $data['ugrouplist'][$i]['id'] = $row["id"];
                $data['ugrouplist'][$i]['name'] = $row["name"];
                $i++;
            }
        $data['info']['total'] = $rs['total'];
        $data['info']['page_size'] =$rs['pagesize'];
        $data['info']['current'] = $rs['pagenum']+1;
        $this->load->view('center/ugroup_permission/report_grant_list', $data);

    }


}