<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 2016/8/11
 * Time: 16:25
 */
class Group extends CI_Controller
{
    public function __construct()
    {
        $this->need_login=true;
        parent::__construct();
        $this->load->model('Group_Model',"group_model");
        $this->load->helper('url_helper');
    }

    public function glist(){
//        $data['datalist'] = $this->user_model->find('','',0)['data'];
//        $i = 0;
//        foreach ($data['datalist'] as $row1) {
//            $id = $row1->group_id;
//            $row = $this->group_model->get_path_for_id($id);
//            $a = '';
//            foreach($row as $row){
//                $a = $a."->".$row['name'];
//            }
//            $data['datalist'][$i]->group_name = substr($a,2);
//            $data['datalist'][$i]->uid = $row1->id;
//            $data['datalist'][$i]->id = $i+1;
//            $i++;
//        }
        $this->check_login("suser");
        $r1 = $this->group_model->level_groups();
        $i = 0;
        foreach ($r1 as $row) {
            $data['ugrouplist'][$i]['gid'] = $row["id"];
            $data['ugrouplist'][$i]['name'] = $row["name"];
            $data['ugrouplist'][$i]['id'] = $i+1;
            $i++;
        }
        $this->load->view('center/group/grouplist', $data);
    }
    public function gadd_view(){
        $this->check_login("suser");
        $r1 = $this->group_model->level_groups();
        $i = 0;
        foreach ($r1 as $row) {
            $data['ugrouplist'][$i]['id'] = $row["id"];
            $data['ugrouplist'][$i]['name'] = $row["name"];
            $i++;
        }

        $this->load->view('center/group/groupadd', $data);
    }
    public function gadd(){
        $this->check_login("suser");
        $groupname = $this->input->get_post("groupname");
        $fgroupid = $this->input->get_post("fgroupid");

        $re = $this->group_model->add($groupname,$fgroupid)['rs'];
        if ($re == "success") {
            $this->return['statusCode'] = '200';
            $this->return['message'] = '操作成功';
            $this->return['navTabId'] = 'showUserGrouplist';
            $this->return['callbackType'] = 'closeCurrent';


        } else {
            $this->return['statusCode'] = '300';
            $this->return['message'] = '操作失败';
        }
        echo json_encode($this->return);
    }

    public function gedit_view(){
        $this->check_login("suser");
        $r1 = $this->group_model->level_groups();
        $gid = $this->input->get_post("gid");
        $data['info']['gid'] = $gid;
        $data['info']['fgid'] = $this->group_model->find_by_id($gid)['data']->father_id;
        $data['info']['gname'] = $this->group_model->find_by_id($gid)['data']->name;
        $i = 0;
        $data['ugrouplist'][$i]['id'] = 0;
        $data['ugrouplist'][$i]['name'] = '根分组';
        $i++;
        foreach ($r1 as $row) {
            $data['ugrouplist'][$i]['id'] = $row["id"];
            $data['ugrouplist'][$i]['name'] = $row["name"];
            $i++;
        }
        $this->load->view('center/group/groupedit',$data);
    }
    public function gedit(){
        $this->check_login("suser");
        $groupname = $this->input->get_post("groupname");
        $fgroupid = $this->input->get_post("fgroupid");
        $gid = $this->input->get_post("id");
        $array1 = array('name','father_id');
        $array0 = array($groupname,$fgroupid);
        $r = $this->group_model->update($gid, $array1, $array0);
        $re = $r['rs'];
        if ($re == "success") {
            $this->return['statusCode'] = '200';
            $this->return['message'] = '操作成功';
            $this->return['navTabId'] = 'showUserGrouplist';
            $this->return['callbackType'] = 'closeCurrent';


        } else {
            $this->return['statusCode'] = '300';
            $this->return['message'] = '操作失败:'.$r['msg'];
        }
        echo json_encode($this->return);
    }

    public function gdel(){
        $this->check_login("suser");
        $gid = $this->input->get_post("gid");
        $r = $this->group_model->delete($gid);
        $re = $r['rs'];
        if ($re == "success") {
            $this->return['statusCode'] = '200';
            $this->return['message'] = '操作成功';
            $this->return['navTabId'] = 'showUserGrouplist';
            $this->return['callbackType'] = '';


        } else {
            $this->return['statusCode'] = '300';
            $this->return['message'] = '操作失败'.$r['msg'];
        }
        echo json_encode($this->return);
    }
}