<?php
/**
 * Created by PhpStorm.
 * User: Realdata
 * Date: 2016/9/6
 * Time: 14:40
 */
class MyTime extends CI_Controller{

//    public function get(){
//        $start = 111111111;
//        $interval = 1000;
//        $current_time = time();
//        $i = (int)((time()-$start)/$interval);
//        $start_time = $start+$i*$interval;
//
//        $end_time = $start+($i+1)*$interval;
//        echo time();
//        echo "</br>";
//        echo $start_time;
//        echo "</br>";
//        echo $end_time;
//    }
public function get_week_time(){
   $start = strtotime("last sunday next day", strtotime(date("Y-m-d",time()))); //取上周日的下一天，也就是取周一，这里不能使用monday直接取周一
   $end = strtotime("next monday", $start) - 1;  // 取下周一的前一秒，也就是本周日的最后一秒
//   echo sprintf("%s 当周开始时间戳和结束时间戳: <br/>\n", date('Y-m-d',time()));
//   echo sprintf("周一: %d -> %s <br/>\n", $start, strftime("%c", $start));
//   echo sprintf("周日: %d -> %s <br/><br/>\n\n", $end, strftime("%c", $end));
   $week_time = array();
   $i = 0;
   for(;$start<$end;$start=$start+24*60*60){
         $week_time[$i][0] = $start;
         $week_time[$i][1]=$start+24*60*60-1;
         $i++;
   }
   return $week_time;
}

public function  get_month(){
   $beginThismonth=mktime(0,0,0,date('m'),1,date('Y'));
   $endThismonth=mktime(23,59,59,date('m'),date('t'),date('Y'));
   $month_time = array();
   $i = 0;
   for(;$beginThismonth<$endThismonth;$beginThismonth=$beginThismonth+24*60*60){
      $month_time[$i][0] = $beginThismonth;
      $month_time[$i][1]=$beginThismonth+24*60*60-1;
      $i++;
   }
   return $month_time;
}
   function get_today_time(){
      $beginToday=mktime(0,0,0,date('m'),date('d'),date('Y'));
      $endToday=mktime(0,0,0,date('m'),date('d')+1,date('Y'))-1;
      return array(array($beginToday,$endToday));
}
}