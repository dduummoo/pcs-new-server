<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 负责运行各个测试用例，可能会发生冲突现象，需要自行处理  冲突现象，不要直接覆盖别人的修改。
 * Created by PhpStorm.
 * User: cyy
 * Date: 2016/7/20
 * Time: 15:52
 * @property user_model $user_model
 */
class Test extends CI_Controller
{
    function __construct(){
        parent::__construct();
        $this->load->library('unit_test');
        $this->load->library('System_security','system_security');
    }
    public function test_hash(){
//        echo($this->system_security->hash_password("aa","aa"));
        print_r(is_numeric("1"));
    }
    public static $UN_VISIBLE_ITEMS;
    function test_example(){
        $this->load->model("User_model");
        $this->load->library('Formdesign');
        $this->load->library("Form_Parse_Tool");
        $this->load->model("Report_Template_Model","tmpl_model");
        $this->load->model("Report_Model","report_model");
        $array = array(array("name"=>"data_wenben"),array("name"=>"data_duohang"),array("name"=>"data_xial"),array("name"=>"data_duoxuan"),array("name"=>"data_wenben"));
        for($i=0;$i<count($array)-1;$i++) {
            print_r($i);
            for ($j = $i + 1; $j < count($array); $j++) {
                print_r($j);
                if ($array[$i]['name'] == $array[$j]['name']) {
                    echo "aaaa";
                    return;
                };
            }
        }
        $data =  array(array(
"name"=> "data_aa",
"type"=> "text",
"value"=> ""),
array(
"name"=> "data_xxx",
"type"=> "text",
"value"=> ""
),
array(
"name"=> "data_aaaaa",
"type"=> "text",
"value"=> ""
),
array(
"name"=> "data_xxccc",
"type"=> "text",
"value"=> ""),
array(
"id"=> "data_mmm",
"name"=> "data_mmm",
"style"=> "width:300px;height:80px;",
"value"=> ""),
array(
"id"=> "data_nnnn",
"name"=> "data_nnnn",
"value"=> ""
)
        );

        $insert_data = $this->form_parse_tool->create_mongo_column_from_formdesign($data);//作
        print_r($insert_data);
//        $login_rs = $this->user->login("张三","123456",32);
//        $this->unit->run($login_rs["rs"], "success", "检测登录是否成功");
        //$this->unit->run('Foo', 'is_string');
        //只是一个示例，更多的查看http://ci.phpxy.com/libraries/unit_testing.html
    }


    function test_user(){
        $this->load->model("User_model","user");
        print_r($this->user->find_all());
//        print_r($this->user->get_self_group_users_by_userid(26));
//        $this->unit->run('error',$this->user->login('张三','123456',34)['rs'],"检测登录失败");
//        $this->unit->run('error',$this->user->login('张三','1234561',32)['rs'],"检测登录失败");
//        $this->unit->run('success',$this->user->login('张三','123456',32)['rs'],  "检测登录成功");
//         $this->unit->run('success',$this->user->delete(10)['rs'], "检测删除成功");
//        $this->unit->run('success',$this->user->delete(1)['rs'],  "检测删除失败");
//          print_r($this->user->update(2,array('username','password','group_id'),array('张思','1234567',2)));
//           print_r($this->user->get_users_id_by_user_id(40));
//        $this->user->add('什么','123456',26);
        //继续添加其他测试用例，进行一系列自动测试
//    print_r($this->user->update(91,array("username","group_id"),array("李四",29)));

//        echo $this->unit->report();

    }
    function test_group(){
        $this->load->model("Group_Model","group");
//        $this->unit->run('success',$this->group->add('石景山区11')['rs'],'测试正确添加一个用户分组');
//        $this->unit->run('success',$this->group->add('花园路派出所',34)['rs'],'测试添加不在同一级用户分组重名');
//        $this->unit->run('error',$this->group->add('花园路派出所',34)['rs'],'测试添加用户分组重名');
//        $this->unit->run('error',$this->group->add('花园路派出所',1)['rs'],'测试添加用户分组父亲id不存在');
//        $this->unit->run('error',$this->group->delete(32)['rs'],'测试删除一个用户分组，下面有user');
//        $this->unit->run('error',$this->group->delete(32)['rs'],'测试删除一个下面有子分组的用户组');
//        $this->unit->run('success',$this->group->delete(28)['rs'],'测试正确删除用户组');
//        $this->unit->run('success',$this->group->update(29,array('name'),array('大钟寺派出所'))['rs'],'测试正确修改用户组');
//        $this->unit->run('error',$this->group->update(31,array('name'),array('大钟寺派出所'))['rs'],'测试错误修改用户组');
//        print_r($this->group->level_groups());
//           print_r($this->group->find_by_id(26));
//          print_r($this->group->get_path_for_id(31));
        print_r($this->group->checkLoop(26,46));
//        print_r($this->group->find());
//        $this->group->refresh_cache();

//        echo $this->unit->report();
    }

    function  test_super(){
        $this->load->model("SuperUser_Model","super");
        print_r($this->super->update(1, array("password"), array("aaaaaaa")));
//        $this->super->find();
    }


    function test_acl(){
        $this->load->model("Acl_Model","acl");
        //print_r( $this->acl->add("Test", "test_acl3", "super"));
        //print_r( $this->acl->delete(1));
       // print_r( $this->acl->find(array("control"=>"Test", "func" => "test_acl2")));

       //print_r( $this->acl->update(2,array("func"), array("newvalue")));
    }

    function test_aclRules(){
        $this->load->model("Acl_Rule_Model","acls");
       //  print_r( $this->acl->add(1,1));
//        print_r( $this->acl->add(1,5));
//        print_r( $this->acl->add(2,5));
        //  print_r( $this->acl->findByAclId(1));
        print_r( $this->acls->find());
    }

    function test_report(){
        $this->load->library('unit_test');
        $this->load->model("Report_Model","report");
        $this->load->model("Report_Grant_Model","report_grant");
//        $repor1 = array('rs'=>'error');
//        print_r($this->report-> get_reports_by_report_ids(array(44,62,25,66),0));
//       print_r($this->report_grant->find_by_report_id_and_ugroup_id(0,0,0));
//        print_r($this->report-> get_reports_by_report_ids($report_ids,2,null));
            print_r($this->report->findId(167));
//         print_r($this->unit->run('success',$this->report->add('57a2b3e8a3eea0841b00002b','闯红灯',2,2,1)['rs'],'测试添加不为test开头的报表'));
//        $this->unit->run('success',$this->report->add(1111111111111,111111111111111,'second',2,2,1)['rs'],'测试添加不为test开头的报表');
//      $this->unit->run('error',$this->report->add(1111111111111,111111111111111,'test_test',2,2,1)['rs'],'测试添加test开头的报表');
//      $this->unit->run('success',$this->report->add(1111111111111,111111111111111,'gamble2221',2,2,1)['rs'],'测试添加不为test开头的报表');
//      $this->unit->run('error',$this->report->add(11,11,'gamble2221',1,1,1)['rs'],'测试添加重复name开头的报表');
//        $this->unit->run($result,$this->report->delete(9),'测试删除的报表');
//      $this->unit->run('success',$this->report->add(1111111111111,111111111111111,'gamble22211',2,2,1)['rs'],'测试再次添加报表');
//      $this->unit->run(1,count($this->report->find()['data']),'测试查找不为test开头报表');
//      $this->unit->run(2,count($this->report->find_test()['data']),'测试查找test开头的报表');
//      $this->unit->run($result,$this->report->update(4,array('name','creator_id'),array('gambleaaa2',2)),'测试修改报表');
//        print_r($this->report->confirm(13,14));
//        $this->unit->run('success',$this->report->confirm(13,14)['rs']);//验证确认添加
//        $this->unit->run('success',$this->report->confirm(14,13)['rs']);//验证取消
//        echo $this->unit->report();
    }
    function test_report_grant(){
        $this->load->model('Report_Grant_Model','reportgrant');
//        $result = array('rs'=>'success');
//        $this->unit->run($result,$this->reportgrant->add(16,2,2,2,1,11111111111111,111111111111111,1),'测试添加一个报表分组');
//        $this->unit->run('error',$this->reportgrant->update($this->getLastInsertId(),array('report_id'),array(15))['rs'],'测试错误修改报表分组(report_id)');
//        $this->unit->run($result,$this->reportgrant->add(15,11,2,2,1,11111111111111,111111111111111,1),'测试添加一个报表分组');
//        $this->unit->run('error',$this->reportgrant->update($this->getLastInsertId(),array('ugroup_id'),array(2))['rs'],'测试错误修改报表分组（ugroup_id）');
//        $this->unit->run('error',$this->reportgrant->update($this->getLastInsertId(),array('report_id','ugroup_id'),array(2,11))['rs'],'测试错误修改报表分组(report_id,ugroup_id)');
//        $this->unit->run('success',$this->reportgrant->update($this->getLastInsertId(),array('report_id','ugroup_id'),array(11,16))['rs'],'测试正确修改报表分组(report_id,ugroup_id)');
//        $this->unit->run($result,$this->reportgrant->delete(11),'测试删除一个报表分组');
//        $this->unit->run('error',$this->reportgrant->add(11,3, 11,2,2,13,1,1,11111111111111,111111111111111111,1)['rs'],'测试添加重复报表分组');
//        $this->unit->run('success',$this->reportgrant->find()['rs'],'测试查找报表分组');
//        $this->unit->run($result,$this->reportgrant->update(2,array('report_id','ugroup_id','create_time', 'update_time'),array(11,12,3,3)),'测试修改重复报表分组');
//        $this->unit->run($result,$this->reportgrant->update(2,array('report_id','ugroup_id','create_time', 'update_time'),array(15,13,3,3)),'测试正确修改报表分组');
//        $this->unit->run('success',$this->reportgrant->find()['rs'],'测试查找报表分组');
//        echo $this->unit->report();
//        print_r($this->reportgrant->get_report_ids_by_group_id(26));
        print_r($this->reportgrant->find(0,0));
    }
    function test_permission(){
        $this->load->model('Permission_Model','permissionModel');
        $result = array('rs'=>'error', 'msg' =>"already_exist");
//        $this->unit->run($result,$this->permissionModel->add("report", "重复的"),'测试添加一个');
//        $this->unit->run(array('rs'=>'success', 'msg' =>null),$this->permissionModel->add("dddd", "test"),'测试添加一个');
//        $this->unit->run(array('rs'=>'error', 'msg' =>"not_empty"),$this->permissionModel->delete(1),'测试删除一个正在使用的');
        // $this->unit->run(array('rs'=>'success', 'msg'=>null), $this->permissionModel->update(11,array("name"),array("news")));
//        $this->unit->run(array('rs'=>'success', 'msg' =>null),$this->permissionModel->delete(12),'测试删除一个');
//        $this->unit->run(null, $this->permissionModel->find(12),'查找不存在的');
        echo $this->unit->report();
    }

    function test_mobile(){
        $this->load->model('Mobile_Model','mobileModel');

//        $this->unit->run(array('rs'=>'success', 'msg'=>null),$this->mobileModel->add(1));
//       print_r($this->mobileModel->add(1));
//
//        $this->unit->run(array('rs'=>'success'), $this->mobileModel->delete(12),'测试删除一个');
//        $this->unit->run(null, $this->mobileModel->find_by_uid(12),'查找不存在的');
//        print_r( $this->mobileModel->find_by_uid(1));
        //echo $this->unit->report();

        $this->load->controller("Mobile_api", "m");
        echo $this->m->checkLogin(1);
    }

//    function test_ugroup_permission(){
//       $this->load->model('Ugroup_Permission_Model','permissionModel');
//        $result = array('rs'=>'error', 'msg' =>"already_exist");
//        $this->unit->run($result,$this->permissionModel->add(1, 1),'测试添加一个');
//        $this->unit->run(array('rs'=>'success', 'msg' =>null),$this->permissionModel->add(1, 2),'测试添加一个');
//        $this->unit->run(array('rs'=>'success', 'msg'=>null), $this->permissionModel->update(1,array("ugroup_id"),array(4)));
      //  $this->unit->run(array('rs'=>'error', 'msg'=>"already_exist"), $this->permissionModel->update(2,array("ugroup_id", "permission_id"),array(4,1)));
      //  $this->unit->run(array('rs'=>'success'),$this->permissionModel->delete(1),'测试删除一个');
       // $this->unit->run(null, $this->permissionModel->find(12),'查找不存在的');

       // $this->unit->run($this->permissionModel->check_permission(1, "test"), false);
      //  $this->unit->run($this->permissionModel->check_permission(1, "montor"), true);

//        print_r($this->permissionModel->get_ugroup_permission(34));
//        echo $this->unit->report();
//    }





    private  function getLastInsertId(){
        return $this->db->insert_id();
    }
    public function test_report_template(){
        $this->load->model('Report_Template_Model','report');
        print_r( $this->report->get_one_report_template_by_report_id(167));
//        "data_names": {
//            "type": null
//  },
//   "data_how_much_money": {
//            "type": null
//  },
//   "data_whodo": {
//            "type": null
//  },
//   "data_do_time": {
//            "type": null
//  },
//        $this->unit->run('success',$this->report-> add_one_column($id,'name','string','名字')['rs'],'测试添加一列');
//        $this->unit->run('success',$this->report-> add_one_column($id,'age','number','年龄')['rs'],'测试添加一列');
//        $this->unit->run('success',$this->report-> add_one_column($id,'date','date','日期')['rs'],'测试添加一列');
//        $this->unit->run('success',$this->report-> add_one_column($id,'datetime','date_time','时间')['rs'],'测试添加一列');
//        $this->unit->run('success',$this->report-> add_one_column($id,'num','checkbox','序号',array('1','2','3'))['rs'],'测试添加一列');
//        $this->unit->run('success',$this->report-> add_one_column($id,'sex','radio','性别',array('男','女'))['rs'],'测试添加一列');
//        $this->unit->run('success',$this->report-> add_one_column($id,'ptype','select','警种',array('辅警','交警','刑警'))['rs'],'测试添加一列');
//        $this->unit->run('success',$this->report-> add_one_column($id,'thing','textarea','事情经过')['rs'],'测试添加一列');
//        $this->unit->run('success',$this->report-> add_one_column($id,'事情经过','textarea','please input your xxxx')['rs'],'测试添加一列');
//        $this->unit->run('success',$this->report-> add_one_column($id,'日期','date','please input your xxxx')['rs'],'测试添加一列');
//        $this->unit->run('error',$this->report->delete_one_column($id,'time')['rs'],'测试删除一列');
//        $this->unit->run('success',$this->report->delete_one_column($id,'事情经过')['rs'],'测试删除一列');
//        $this->unit->run('success',$this->report-> add_one_column($id,'name','string','please input your name')['rs'],'测试添加一列');
//        $this->unit->run('error',$this->report-> update_one_column($id,'time','string1','please INPUT your Name')['rs'],'测试错误修改一列');
//       $this->unit->run('success',$this->report-> add_one_column($id,'姓名','textarea','姓名')['rs'],'测试修改一列');
//       print_r($this->report->update_all_column("57ce5ad7a3eea0ac26000029",array("name"=>array("type"=>"float"))));
//        print_r($this->report->get_visible_by_id(20));
//       echo $this->unit->report();
    }
    public function test_report_column_type(){
        $this->load->model('Report_Column_Type_Model','report_column_type');
        print_r($this->report_column_type->find(1,'aa'));
    }

    public  function test_report_record(){
        $this->load->model('Report_Record_Model','report_record');
//        $date=$this->report_record->get_records_by_userid_and_start_time_and_end_time_and_report_id(50,null,null,44,0)['report_records'][0]['time'];
//        $record = $this->report_record->get_one_report_record("57e1f63aa3eea0302600002d",105);
        $record = $this->report_record->update_report_data("57e1f63aa3eea0302600002d",105,array("data_fuxuankuang"=>"c"));
       print_r($record);
    }

    public  function test_statistics_model(){
        $this->load->model('Statistics_Report_Model','staticstics');
//        print_r($this->staticstics->get_statistics_report_by_usersid_and_statistics_name(array(44,49),"2",0));
        print_r(count($this->staticstics->find_by_name(null,1)['reports']));
    }
    public  function test_monitor_model(){
        $this->load->model('Monitor_Grant_Model','monitor');
//       print_r($this->monitor->get_groups_and_users());
//        print_r($this->monitor->get_groups_and_users_by_group_id(29));
//        print_r($this->monitor->find_by_monitor_group_id(26));
        print_r($this->monitor->get_groups_and_users_by_group_id(26));
    }
    public function test_query(){
        $this->load->model('Statistic_Query_Model','t');
//        print_r($this->t->query( 'result=26.uid.where(uid==13).sum()'));
//        print_r($this->t->calc_one_line('result=167.uid.where(time>=1111111111,time<=2016-10-14 17:10:0).sum()'));
        print_r($this->t->query("result=167.uid.where(time<=2016-10-18).list_sum(group_by_uid)"));

//        $array_query = explode(";",$query);
//        for($k = 0;$k<count($array_query);$k++){//遍历多个表达式...
//            $arr = explode(".",$array_query[$k]);//分割一个表达式
//            if(array_key_exists(2,$arr)&&strpos($arr[2],"where")===0){
//                $where_arr = explode(",",substr($arr[2],6,strlen($arr[2])-7));//分割条件
//                $count = count($where_arr);
//                for($j=0;$j<$count;$j++) {
//                    if (strpos($where_arr[$j], "time") === 0) {
//                          if(stripos($where_arr[$j],">=")){
//                              $num = stripos($where_arr[$j],">=");
//                          }else if(stripos($where_arr[$j],"<=")){
//                              $num = stripos($where_arr[$j],"<=");
//                          }
//                        $time = substr($where_arr[$j],$num+2,strlen($where_arr[$j])-6);
//                        if(!is_numeric($time)){
//                            $time = strtotime($time);
//                        }
//                        $where_arr[$j] = substr($where_arr[$j],0,$num+2).$time;
//                    }
//                }
//                //首先需要得到where套件，
//                $arr[2] = "where(".implode(",", $where_arr).")";
//                $array[$k] =  implode(".",$arr);
//            }
//        }
//        print_r( implode(";",$array));
//        print_r(strtotime("2016-10-14 17:10:0"));
//        print_r($this->t->query('result=66.uid.where().list_count(group_by_uid);'));
//
//        print_r($this->t->query('result=26.uid.where(uid==13).list_count(group_by_data_thing)'));
//         print_r($this->t->query( 'total=26.uid.where(uid==13).sum()+3;count=26.uid.where(name=李四).count()+3;result=total*count;'));
//         print_r($this->t->query( 'total=26.uid.where(uid==13).sum()+3;count=26.uid.where(name=李四).count();result=total;'));
//       $result = $this->t->query( 'result=26.uid.where(uid==13).list_sum("group_by_data_name");');
//        print_r($result);
//        $result = array(
//            "type"=> "list",
//            "data" =>array(
//                "result"=>
//                array(
//                    array("_id"=> "1111",
//                    "count_userid"=>4),
//                    array("_id"=> "1111",
//                        "count_userid"=>4))
//            ),
//         "ok"=> 1,
//        "column"=>"GPS"
//        );
//        $str = '';
////        print_r($result);
//        echo '</br>';
//        for($i = 0;$i <count($result['data']['result']);$i++){
//            $str = $str.$result['column']."=".$result['data']['result'][$i]['_id'].":".$result['data']['result'][$i]['count_userid'];
//        }
//       echo($str);

//        print_r($this->t->query( 'result=26.uid.where(uid==13).sum()+3;'));
//        echo '</br>';
//        print_r($this->t->query( 'result=26.uid.where(uid==13).count()+3;'));
//        echo '</br>';
//        print_r($this->t->query( 'result=26.uid.where(uid==13).list_sum("group_by_data_name")'));
//        echo '</br>';
//        print_r($this->t->query( 'result=26.uid.where(uid==13).list_avg("group_by_GPS")'));
//        echo '</br>';
//        print_r($this->t->query( 'result=26.uid.where(uid==13).list_count("group_by_data_name")'));
//        echo '</br>';

//        print_r($this->t->query( 'total=26.uid.where(uid==13).sum()+3;avg=26.uid.where(name==李四).count();result=total*3;'));
//        print_r($this->t->query( 'total=26.uid.where(uid==13).sum()+3;count=26.uid.where(name==李四).count();result=total*3;'));
    }
    public function test_statistics_report(){
        $this->load->model('Statistics_Report_Model','s');
//        $this->unit->run('success',$this->s->add(26,'总计',13,13,'123','123','234')['rs'],'测试添加一个');
//        $this->unit->run('error',$this->s->add(26,'总计',13,13,'123','123','234')['rs'],'测试添加重名');
//        $this->unit->run('success',$this->s->add(26,'总计1',13,13,'123','123','234')['rs'],'测试添加一个');
//        $this->unit->run('error',$this->s->update(2,array('name','creator_id'),array('总计',14)),'测试更新重名');
        print_r($this->s->get_statistics_report_by_report_id(26));
//        print_r($this->s->update(3,array('name','updator_id'),array('总计2',14)));
//        $this->unit->run('error',$this->s->update(3,array('name','creator_id'),array('总计2',14)),'测试更新');
//        $this->unit->run('success',$this->s->delete(2),'测试删除');
        echo $this->unit->report();
    }

    public function test_ugroup_permission(){
        $this->load->model('Ugroup_Permission_Model','ugroup_permission');
        print_r($this->ugroup_permission->find_user_group_ids(array(26,27,31)));
    }

    public function test_back_up_db(){
        $this->load->model('Db_Model','db_model');
       print_r($this->db_model->back_up_db());
    }
    public function test_canl_week(){//按周每天来统计
        $this->load->model('Statistic_Query_Model','t');
        $this->load->library("My_time","my_time");
        $week_time = $this->my_time->get_week_time();
        $canl_array = array();
        $query = "result=167.uid.where(time>=0,time<10).sum()";
        $array_query = explode(";",$query);
        for($i = 0;$i < count($week_time);$i++){
            for($k = 0;$k<count($array_query);$k++){//遍历多个表达式...
                $arr = explode(".",$array_query[$k]);//分割一个表达式
                if(array_key_exists(2,$arr)&&strpos($arr[2],"where")===0){
                    $where_arr = explode(",",substr($arr[2],6,strlen($arr[2])-7));//分割条件
                    $count = count($where_arr);
                for($j=0;$j<$count;$j++) {
                    if (strpos($where_arr[$j], "time") === 0) {
                        unset($where_arr[$j]);
                    }
                }
                    array_push($where_arr, "time>=".$week_time[$i][0]);
                    array_push($where_arr, "time<=".$week_time[$i][1]);//将需要的条件已经放进数组
                    //首先需要得到where套件，
                    $arr[2] = "where(".implode(",", $where_arr).")";
                    $array[$k] =  implode(".",$arr);
                }
            }
            $query =  implode(";",$array);
            $canl_array[$i] =  $this->t->query($query);
        }
print_r($canl_array);
}
    public function test_canl_month_day(){//按今年开始到这个月每个月统计
        $this->load->model('Statistic_Query_Model','t');
        $this->load->library("My_time","my_time");
        $month_time = $this->my_time->get_month_time();
        $canl_array = array();
        $query = "result=167.uid.where(time>=0,time<10).list_sum(group_by_uid)";
        $array_query = explode(";",$query);
        for($i = 0;$i < count($month_time);$i++){
            for($k = 0;$k<count($array_query);$k++){//遍历多个表达式...
                $arr = explode(".",$array_query[$k]);//分割一个表达式
                if(array_key_exists(2,$arr)&&strpos($arr[2],"where")===0){
                    $where_arr = explode(",",substr($arr[2],6,strlen($arr[2])-7));//分割条件
                    $count = count($where_arr);
                    for($j=0;$j<$count;$j++) {
                        if (strpos($where_arr[$j], "time") === 0) {
                            unset($where_arr[$j]);
                        }
                    }
                    array_push($where_arr, "time>=".$month_time[$i][0]);
                    array_push($where_arr, "time<=".$month_time[$i][1]);//将需要的条件已经放进数组
                    //首先需要得到where套件，
                    $arr[2] = "where(".implode(",", $where_arr).")";
                    $array[$k] =  implode(".",$arr);
                }
            }
            $query =  implode(";",$array);
            $canl_array[$i] =  $this->t->query($query);
        }
        print_r($canl_array);
    }
    public function test_canl_month(){//按今年开始到这个月每个月统计
        $this->load->model('Statistic_Query_Model','t');
        $this->load->library("My_time","my_time");
        $month_time = $this->my_time->get_month_time(2016,9);
        $canl_array = array();
        $query = "result=167.uid.where(time>=0,time<10).list_sum(group_by_uid)";
        $array_query = explode(";",$query);
        for($i = 0;$i < count($month_time);$i++){
            for($k = 0;$k<count($array_query);$k++){//遍历多个表达式...
                $arr = explode(".",$array_query[$k]);//分割一个表达式
                if(array_key_exists(2,$arr)&&strpos($arr[2],"where")===0){
                    $where_arr = explode(",",substr($arr[2],6,strlen($arr[2])-7));//分割条件
                    $count = count($where_arr);
                    for($j=0;$j<$count;$j++) {
                        if (strpos($where_arr[$j], "time") === 0) {
                            unset($where_arr[$j]);
                        }
                    }
                    array_push($where_arr, "time>=".$month_time[$i][0]);
                    array_push($where_arr, "time<=".$month_time[$i][1]);//将需要的条件已经放进数组
                    //首先需要得到where套件，
                    $arr[2] = "where(".implode(",", $where_arr).")";
                    $array[$k] =  implode(".",$arr);
                }
            }
            $query =  implode(";",$array);
            $canl_array[$i] =  $this->t->query($query);
        }
        print_r($canl_array);
    }
}